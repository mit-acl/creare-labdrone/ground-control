import json
import os
import sys
import traceback

import numpy as np
from tornado import ioloop, web, websocket, gen
from traits.api import String, Undefined

from transformer.processor.patch import Patch
from transformer.settings import PARAMS_FILE

cl = []


class transformerWrapper:
    def __init__(self, socketHandler):
        self.socketHandler = socketHandler
        self.has_stl_file = False
        self.has_targets_file = False
        self.patch = Patch(PARAMS_FILE)

    def reload_patch_mesh(self):
        if self.has_stl_file and self.has_targets_file:
            self.patch.load_scan_data()
            self.patch.load_targets_data()
            self.patch.set_boundary_points()
            self.patch.fit_surface()
            self.patch.identify_outliers()
            scan_data_mesh = self.patch.scan_data  # mesh object

            points = scan_data_mesh.points
            mask = self._combine_masks()
            points = np.column_stack((points[:, 0], points[:, 1], points[:, 2], mask))
            points = np.row_stack(([4, 0, 0, 0], points))
            buffer = points.astype(np.float32).tobytes()
            self.socketHandler.write_message(buffer, binary=True)

            # Send the target and camera information before the facets.
            controls_target, camera_pos = self.patch.find_gui_view()
            scene_information = np.row_stack(([3, 8, 0], controls_target, camera_pos))
            buffer = scene_information.astype(np.float32).tobytes()
            self.socketHandler.write_message(buffer, binary=True)

            facets = scan_data_mesh.facets
            facets = np.column_stack((facets[:, 0], facets[:, 1], facets[:, 2]))
            facets = np.row_stack(([3, 1, 0], facets))
            buffer = facets.astype(np.float32).tobytes()
            self.socketHandler.write_message(buffer, binary=True)

            self._get_targets_mesh_points()
            self._get_targets_mesh_facets()

    def set_stl_file(self, stl_file):
        stl_file = os.path.join(os.getcwd(), r"www", r"data", stl_file)
        # Set the scan file
        self.patch.set_scan_file(stl_file)
        self.has_stl_file = stl_file is not String(Undefined)

    def set_targets_file(self, targets_file):
        targets_file = os.path.join(os.getcwd(), r"www", r"data", targets_file)
        # set the target file
        self.patch.set_targets_file(targets_file)
        self.has_targets_file = targets_file is not String(Undefined)
        self.reload_patch_mesh()

    def set_virtual_targets(self, virtual_targets):
        if len(virtual_targets) == 0:
            virtual_targets = np.zeros((0, 3))
        self.patch.virtual_targets = virtual_targets
        self.patch.set_boundary_points()
        self.patch.fit_surface()
        self.patch.identify_outliers()
        self.refresh_mask_colors()

    def set_find_holes(self, find_hole):
        self.patch.add_hole(find_hole)
        self._get_holes_mesh_points()
        self._get_holes_mesh_facets()

    def set_ignore_holes(self, ignore_hole):
        self.patch.ignore_hole(ignore_hole)
        self._get_holes_mesh_points()
        self._get_holes_mesh_facets()

    def _combine_masks(self):
        outlier_mask = self.patch.outlier_mask.astype(np.int)
        boundary_mask = 0.5 * self.patch.boundary_mask.astype(np.int)
        mask = outlier_mask + boundary_mask
        return mask

    def refresh_mask_colors(self):
        mask = self._combine_masks()
        colors = np.column_stack((np.zeros_like(mask), mask))
        colors = np.row_stack(([2, 6], colors))
        buffer = colors.astype(np.float32).tobytes()
        self._get_targets_mesh_points(virtual=True)
        self._get_targets_mesh_facets(virtual=True)
        self.socketHandler.write_message(buffer, binary=True)

    def do_main_processing(self):
        print("Doing main processing...")
        sys.stdout.flush()
        self.patch.main_processing()
        print("Done main processing.")
        sys.stdout.flush()
        self._get_holes_mesh_points()
        self._get_holes_mesh_facets()

    def _get_targets_mesh_points(self, virtual=False):
        mesh, point_index = self.patch.get_target_mesh(align=False, virtual=virtual)
        points = mesh.points
        points = np.column_stack(
            (points[:, 0], points[:, 1], points[:, 2], point_index[:])
        )
        message_id = 9 if virtual else 2
        points = np.row_stack(([4, message_id, 0, 0], points))
        buffer = points.astype(np.float32).tobytes()
        self.socketHandler.write_message(buffer, binary=True)

    def _get_targets_mesh_facets(self, virtual=False):
        facets = self.patch.get_target_mesh(align=False, virtual=virtual)[0].facets
        facets = np.column_stack((facets[:, 0], facets[:, 1], facets[:, 2]))
        message_id = 10 if virtual else 3
        facets = np.row_stack(([3, message_id, 0], facets))
        buffer = facets.astype(np.float32).tobytes()
        self.socketHandler.write_message(buffer, binary=True)

    def _get_holes_mesh_points(self):
        mesh, point_index = self.patch.get_hole_mesh(align=False)
        points = mesh.points
        holes = self.patch.get_holes(align=False)
        points = np.column_stack(
            (
                points[:, 0],
                points[:, 1],
                points[:, 2],
                holes[point_index, 4],
                point_index[:],
            )
        )
        points = np.row_stack(([5, 4, 0, 0, 0], points))
        buffer = points.astype(np.float32).tobytes()
        self.socketHandler.write_message(buffer, binary=True)

    def _get_holes_mesh_facets(self):
        facets = self.patch.get_hole_mesh(align=False)[0].facets
        facets = np.column_stack((facets[:, 0], facets[:, 1], facets[:, 2]))
        facets = np.row_stack(([3, 5, 0], facets))
        buffer = facets.astype(np.float32).tobytes()
        self.socketHandler.write_message(buffer, binary=True)


class IndexHandler(web.RequestHandler):
    def get(self):
        self.render("index.html")


class SocketHandler(websocket.WebSocketHandler):

    transformer_wrapper = None

    def check_origin(self, origin):
        return True

    def open(self):
        if self not in cl:
            cl.append(self)

    def on_close(self):
        if self in cl:
            cl.remove(self)

    def on_message(self, message):
        if self.transformer_wrapper is None:
            self.transformer_wrapper = transformerWrapper(self)
        print("web socket message: " + message)
        if message is not String(Undefined) and len(message) > 0:
            try:
                message_json = json.loads(message)
                print(message_json)
                save_file = message_json.get("save_file")
                if save_file is not None and save_file is True:
                    with open(
                        self.transformer_wrapper.patch.output_json_file,
                        "w",
                        encoding="utf-8",
                    ) as f:
                        first_target = self.transformer_wrapper.patch.targets[
                            self.transformer_wrapper.patch.alignment_targets[0], :
                        ]
                        second_target = self.transformer_wrapper.patch.targets[
                            self.transformer_wrapper.patch.alignment_targets[1], :
                        ]
                        message_json["alignmentTargets"] = {
                            "indices": message_json["alignmentTargets"],
                            "origin": [first_target[0], first_target[1]],
                            "x_direction": [second_target[0], second_target[1]],
                        }
                        json.dump(message_json, f, ensure_ascii=False, indent=4)
                else:
                    stl_file = message_json.get("stlFile")
                    if stl_file is not None:
                        self.transformer_wrapper.set_stl_file(stl_file)
                    targets_file = message_json.get("targetsFile")
                    if targets_file is not None:
                        self.transformer_wrapper.set_targets_file(targets_file)

                    virtual_targets = message_json.get("virtualTargets")
                    if virtual_targets is not None:
                        self.transformer_wrapper.set_virtual_targets(virtual_targets)

                    do_main_processing = message_json.get("do_main_processing")
                    if do_main_processing is not None and do_main_processing is True:
                        self.transformer_wrapper.do_main_processing()

                    find_holes = message_json.get("findHoles")
                    if find_holes is not None:
                        self.transformer_wrapper.set_find_holes(find_holes)

                    ignore_holes = message_json.get("ignoreHoles")
                    if ignore_holes is not None:
                        self.transformer_wrapper.set_ignore_holes(ignore_holes)

                    alignmentTargets = message_json.get("alignmentTargets")
                    if alignmentTargets is not None:
                        self.transformer_wrapper.patch.alignment_targets = (
                            alignmentTargets
                        )
                        boundary_polygon = message_json.get("boundaryPolygon")
                        if boundary_polygon is not None:
                            for index, item in enumerate(boundary_polygon):
                                boundary_polygon[index] = np.asarray(item)
                            boundary_polygon = np.asarray(
                                boundary_polygon, dtype="float32"
                            )
                            self.transformer_wrapper.patch.dimensions = boundary_polygon

                        # set alignment targets and export to machine code.
                        self.transformer_wrapper.patch.save_machine_code(
                            overwrite=True, igs_format=False
                        )

                        self.transformer_wrapper.patch.save_csv_holes(
                            convert_to_inch=True
                        )

                        if boundary_polygon is not None:
                            boundary_polygon = (
                                self.transformer_wrapper.patch.dimensions * 25.4
                            )
                            # Transform dimensions to scan coordinate system, send to GUI
                            boundary_polygon = self.transformer_wrapper.patch._invert_align(
                                boundary_polygon
                            )
                            boundary_polygon = np.row_stack(
                                ([3, 7, 0], boundary_polygon, boundary_polygon[0])
                            )
                            buffer = boundary_polygon.astype(np.float32).tobytes()
                            self.write_message(buffer, binary=True)

            except Exception as e:
                print("Error occurred when receiving message. Error = " + str(e))
                print("Message = " + message)
                traceback.print_exc()
                sys.stdout.flush()
            sys.stdout.flush()


class ApiHandler(web.RequestHandler):
    @gen.coroutine
    def get(self, *args):
        self.finish()
        id = self.get_argument("id")
        value = self.get_argument("value")
        data = {"id": id, "value": value}
        data = json.dumps(data)
        for c in cl:
            c.write_message(data)

    @gen.coroutine
    def post(self):
        pass


port = 8888
handlers = [
    (r"/ws", SocketHandler),
    (r"/", IndexHandler),
    (r"/api", ApiHandler),
    (
        r"/(.*)",
        web.StaticFileHandler,
        {"path": os.path.join(os.path.dirname(__file__), "../../www")},
    ),
    (r"/(favicon.ico)", web.StaticFileHandler, {"path": "../../"}),
]
# Change index if changing handler array!
app = web.Application(handlers, debug=True, template_path=handlers[3][2]["path"])

if __name__ == "__main__":
    app.listen(8888)
    ioloop.IOLoop.instance().start()
