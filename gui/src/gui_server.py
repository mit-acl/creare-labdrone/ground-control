import json
import os
import sys
import traceback
import time #debug
import threading
import random
from operator import add


import rospy
from std_srvs.srv import Empty,EmptyRequest

import numpy as np
from tornado import ioloop, web, websocket, gen
from traits.api import String, Undefined

import sys

import ground_control.database as db

cl = []

kill_proxy = rospy.ServiceProxy("/KillGC", Empty)
ground_proxy = rospy.ServiceProxy("/GroundGC", Empty)

testjson =json.dumps({"msgtype":"setLabObjects","data":[{"type":"Tube","pts":[[0,0,0],[0,0,5],[0,0,10],[0,1,15]]},{"type":"Cuboid","depth":10,"width":5,"height":5,"position":[0,3,5],"rot_z":30},{"type":"Cuboid","depth":0,"width":40,"height":35,"position":[0,0,0],"rot_z":0},{"type":"Cuboid","depth":20,"width":0,"height":35,"position":[20,0,10],"rot_z":0},{"type":"Cuboid","depth":20,"width":40,"height":0,"position":[0,17.5,10],"rot_z":0}]})
testjson2 = json.dumps({"msgtype":"setDroneInfo","data":[{"position":[2,2,2]},{"position":[5,3,8]}]})

#class MyStaticFileHandler(web.StaticFileHandler):
    #def set_extra_headers(self, path):
        ## Disable cache
        #self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0')
    #pass

class TornadoDaemon():
    __instance = None
    __running = False
    __io_thread = None
    __app = None
    __initialized = False
    @staticmethod
    def getInstance():
        """ Static access method. """
        if TornadoDaemon.__instance == None:
            TornadoDaemon()
        return TornadoDaemon.__instance
    @staticmethod
    def kill():
        TornadoDaemon.__instance == None
        #Probably need to add handling to dispose of previous instance if it exists. DEBUG for now.
    def __init__(self):
        """ Virtually private constructor. """
        if TornadoDaemon.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            TornadoDaemon.__instance = self
    def start(self,port=8888):
        if not TornadoDaemon.__running:
            if not TornadoDaemon.__initialized:
                handlers = [
                    (r"/ws", SocketHandler),
                    (r"/", IndexHandler),
                    (r"/api", ApiHandler),
                    (
                        r"/(.*)",
                        web.StaticFileHandler,
                        {"path": os.path.join(os.path.dirname(__file__), "../www")},
                    ),
                ]
                # Change index if changing handler array!
                TornadoDaemon.__app = web.Application(handlers, debug=True, template_path=handlers[3][2]["path"])    
                TornadoDaemon.__app.listen(port)
                TornadoDaemon.__initialized = True
            TornadoDaemon.__io_thread = threading.Thread(target=ioloop.IOLoop.instance().start,args = [])
            TornadoDaemon.__io_thread.start()
            TornadoDaemon.__running = True
    def stop(self):
        if TornadoDaemon.__running:
            ioloop.IOLoop.instance().stop()
            TornadoDaemon.__io_thread.join()
            TornadoDaemon.__running = False


class MainHandler:
    __instance = None
    
    @staticmethod
    def getInstance():
        """ Static access method. """
        if MainHandler.__instance == None:
            MainHandler()
        return MainHandler.__instance
    def __init__(self):
        """ Virtually private constructor. """
        if MainHandler.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            MainHandler.__instance = self
            self.msg_lock = threading.Lock()
            self.sockethandlers = []
            self.last_active_path_indices = []
            #I'm pretty sure there's not a use case where we want to instantiate MainHandler before the tornado app
            TornadoDaemon.getInstance().start()
    def _add_socket_handler(self,sockethandler):
        self.sockethandlers.append(sockethandler)
        print("adding websocket {}".format(sockethandler))
        
    #Externally facing methods for communicating information to GUI through websocket
    def set_drone_position(self,position_list):
        json_msg = json.dumps({"msgtype":"setDroneInfo","data":[{"position":position} for position in position_list]})
        self.write_to_all_json(json_msg)
        
    def set_fleet_status(self,status_list):
        # status_list is list of dict [{drone_id: dict(FleetManagerDroneStatus)}]
        # FleetManagerDroneStatus consists of:
        #   job_status
        #   payload_id
        #   battery_percentage
        #   last_contact
        json_msg = json.dumps({"msgtype":"setFleetStatus","data":[{"drone_id":status["drone_id"],\
                                                                    "job_status":status["job_status"],\
                                                                    "payload_id":status["payload_id"],\
                                                                    "battery_percentage":status["battery_percentage"],\
                                                                    "last_contact_ms":status["last_contact"],\
                                                                    "grounded":status["grounded"]\
                                                                    } for status in status_list]})
        self.write_to_all_json(json_msg)

    def set_log(self,log_dict):
        # with self.msg_lock:
        json_msg = json.dumps({"msgtype":"setLog","data":{"severity":log_dict["severity"],\
                                                          "source":log_dict["source"],\
                                                          "datetime":log_dict["datetime"],\
                                                          "msg":log_dict["msg"]\
                                                          }})
        self.write_to_all_json(json_msg)
        if log_dict["severity"] != "DEBUG":
            print(log_dict) #Obviously debug


    def set_candidate_exclusion(self,exclusion_json):
        if isinstance(exclusion_json,str):
            exclusion_json = json.loads(exclusion_json)
        msg = {"msgtype":"setCandidateExclusion", "data":exclusion_json}
        #print(msg)
        self.write_to_all_json(json.dumps(msg))

    def set_candidate_path(self,path_json):
        #TODO: Add processing to javascript frontend
        msg = {"msgtype":"setCandidatePath", "data":path_json}
        #print(msg)
        self.write_to_all_json(json.dumps(msg))

    def DEBUG_set_random_candidate_exclusion(self):
        candidate_cuboid_json = {"length":{"x": random.random()*5, "y": random.random()*5, "z": random.random()*5 },"centroid":{"x": (random.random()*20), "y": (random.random()*20), "z": (random.random()*20) },"theta_z":random.random()*360}
        self.set_candidate_exclusion(candidate_cuboid_json)        

    def set_lab_model(self,lab_model_json=None,lab_model_name=None):
        if lab_model_json is None:
            lab_model_json = self._get_lab_model_json(lab_model_name)

        data = json.loads(lab_model_json)
        msg = {"msgtype": "setLabObjects", "data": data}

        self.write_to_all_json(json.dumps(msg))

    def _get_lab_model_json(self,lab_model_name='CreareLabTest6'):
        lab_model_json = None
        with db.Database() as dbase:
            sess = dbase.session
            lab_model = sess.query(db.LabModelSQL).filter(db.LabModelSQL.name == lab_model_name).first()
            lab_model_json = lab_model.json
        return lab_model_json

    def set_active_path_indices(self,active_path_indices_list):
        json_msg = json.dumps({"msgtype":"setActivePaths","data":[{"index":path_index} for path_index in active_path_indices_list]})
        self.write_to_all_json(json_msg)
        self.last_active_path_indices = active_path_indices_list
        
    def set_cached_path_indices(self):
        self.set_active_path_indices(self.last_active_path_indices)


    def write_to_all_json(self,json_msg):
        """
        write json message to all socket handlers.
        Remove socket handlers that have been closed
        """
        #print(json_msg)
        for sh in  self.sockethandlers: 
            try:
                sh.write_message(json_msg)
            except websocket.WebSocketClosedError:
                print("removing websocket {}".format(sh))
                self.sockethandlers.remove(sh)
            except:
                print("Unexpected error:", sys.exc_info()[0])
                
class IndexHandler(web.RequestHandler):
    def get(self):
        self.render("index.html")


class SocketHandler(websocket.WebSocketHandler):

    main_handler = None

    def open(self):
        if self not in cl:
            cl.append(self)
            self.write_message("connected!")
            print(len(cl))
            if self.main_handler is None:
                self.main_handler = MainHandler.getInstance()
                self.main_handler._add_socket_handler(self)
                lab_model_json = self.main_handler._get_lab_model_json()
                data = json.loads(lab_model_json)
                msg = {"msgtype": "setLabObjects", "data": data}
                self.write_message(json.dumps(msg))

                #This will push to all GUIs, which is not optimal, but also not a specific problem
                self.main_handler.set_cached_path_indices()

    def on_close(self):
        if self in cl:
            cl.remove(self)

    def dummy_status(self):
        statusjson = json.dumps({"header":"dummy_status","data":random.random()})
        self.write_message(statusjson)

    def on_message(self, message):

        if message is not String(Undefined) and len(message) > 0:
            try:
                message_json = json.loads(message)
                print(message_json)
                #We probably want to be a little more rigorous here and define a message syntax
                if "candidateExclusion" in message_json :
                    kill_proxy(EmptyRequest())
                    pass
                    #self.main_handler.DEBUG_set_random_candidate_exclusion()
                elif "kill" in message_json :
                    kill_proxy(EmptyRequest())
                elif "ground" in message_json :
                    ground_proxy(EmptyRequest())

            except Exception as e:
                print("Error occurred when receiving message. Error = " + str(e))
                print("Message = " + message)
                traceback.print_exc()
                sys.stdout.flush()
            sys.stdout.flush()


class ApiHandler(web.RequestHandler):
    @gen.coroutine
    def get(self, *args):
        self.finish()
        id = self.get_argument("id")
        value = self.get_argument("value")
        data = {"id": id, "value": value}
        data = json.dumps(data)
        for c in cl:
            c.write_message(data)

    @gen.coroutine
    def post(self):
        pass

if __name__ == "__main__":
    start_daemon()
    mainHandler = MainHandler.getInstance()
    while (True):
        time.sleep(0.1)
        mainHandler.set_drone_info()
       

