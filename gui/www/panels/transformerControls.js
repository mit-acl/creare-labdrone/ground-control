function addTargetClicked(source) {
  mesh.virtualTargetSource = source;
  mesh.stateMachine.doTransition('addTarget');
}

function removeTargetClicked(source) {
  mesh.virtualTargetSource = source;
  mesh.stateMachine.doTransition('removeTarget');
}

function doneTargetsClicked(source) {
  var messageJSON = {
    do_main_processing: true
  };
  var message = JSON.stringify(messageJSON);
  transformerControls.setPanelData(source, message, 'tows');

  mesh.stateMachine.doTransition('holes');
  $('.targets-button').prop('disabled', true);
}

function findHoleClicked(source) {
  mesh.holesSource = source;
  mesh.stateMachine.doTransition('findHole');
}

function ignoreHoleClicked(source) {
  mesh.holesSource = source;
  mesh.stateMachine.doTransition('ignoreHole');
}

function doneHolesClicked(source) {
  mesh.stateMachine.doTransition('alignment');
  $('.holes-button').prop('disabled', true);
  $('.alignment-button').prop('disabled', false);
  $('#widthInput').prop('disabled', false);
  $('#heightInput').prop('disabled', false);
}

function selectTargetsClicked(source) {
  mesh.stateMachine.doTransition('selectAlignment');
}

function dimensionKeydown(source) {
  if(event.key === 'Enter') {
    // TODO: dimension input, do we have enough to cut?
    console.log("enter key pressed in dimension input");
  }
  return;
}

function undoClicked(source) {
  // TODO: Implement undo functionality: undo last target or cut
  console.log('Undo button clicked.');
}

function cutClicked(source) {
  // TODO: Implement cut to 2D functionality
  console.log('Cut button clicked.');
}

function exportClicked(source) {
  if (mesh.patch.alignmentTargets === undefined || mesh.patch.alignmentTargets.length !== 2) {
    alert("Must select two alignment targets from the mesh.");
    return;
  }
  mesh.patch.boundaryPolygon = [];
  mesh.patch.width = parseFloat($('#widthInput').val());
  mesh.patch.height = parseFloat($('#heightInput').val());
  mesh.exportButton = source;
  var v0 = [0, 0, 0];
  var v1 = [v0[0] + mesh.patch.width, v0[1], v0[2]];
  var v2 = [v0[0], v0[1] + mesh.patch.height, v0[2]];
  var v3 = [v1[0], v2[1], v0[2]];
  mesh.patch.boundaryPolygon.push(v0, v1, v3, v2);
  var messageObject = {
    alignmentTargets: mesh.patch.alignmentTargets,
    boundaryPolygon: mesh.patch.boundaryPolygon
  };
  var message = JSON.stringify(messageObject);
  transformerControls.setPanelData(source, message, 'tows');
}

transformerControls = {
  name: "Transfomer Controls",

  panelHTML: function(uniqueParentElementID) {
    $('#' + uniqueParentElementID + '-container').load('js/panels/transformerControls.html', function() {
      var stlElement = document.getElementById("stlFileChooser");
      stlElement.addEventListener("change", handleStlFile, false);

      function handleStlFile() {
        console.log('stl file handler called.');
        var fileNameElement = document.getElementById("stlFileName");
        if (this.files[0] && this.files[0].name && this.files[0].name.length > 0) {
          $('.import-svg>input').prop('disabled', true);
          $('#importSvg')[0].src = '/style/svgs/Import_Inactive.svg';
          var obj = {
            "stlFile": this.files[0].name,
            "targetsFile": this.files[0].name.split('.stl')[0]+'.txt'
          };
          mesh.patch.stlFile = obj.stlFile;
          mesh.patch.targetsFile = obj.targetsFile;
          var message = JSON.stringify(obj);
          transformerControls.setPanelData(this, message, 'tows');
          fileNameElement.innerText = mesh.patch.stlFile;
          return;
        }
        fileNameElement.innerText = "";
      }
    });
    return undefined;
  },

  linkDataKeys: [
    'tows', // To web socket as string
    'fromws', // From web socket as string
    'fromwsbinary' // From web socket as binary
  ],

  init: function(elID) {
    // Initialize Data Structure
    divIDE.panelJSData[elID] = {
      tows: '',
      fromws: '',
      fromwsbinary: ''
    };
  },

  getPanelData: function(parentElement, key) {
    var elID = parentElement.attr('id');
    console.log('TransformerControls.getPanelData(' + elID + ', ' + key + ') results in: ' + divIDE.panelJSData[elID][key]);
    if (key == 'tows') {
      return divIDE.panelJSData[elID][key];
    } else if (key == 'fromws') {
      return divIDE.panelJSData[elID][key];
    } else if (key == 'fromwsbinary') {
      return divIDE.panelJSData[elID][key];
    } else {
      return;
    }
  },

  ready: function() {
    $.ajax({
      url: 'GUILayout.json',
      success: function(result) {
        try {
          layout.importLayout(JSON.parse(result));
        } catch (e) {
          console.log(e);
          layout.importLayout(result);
        }
      }
    });
  },

  setPanelData: function(parentElement, data, key) {
    var elID = $(divIDE.getCtxTarget(parentElement)).attr('id');
    console.log('TransformerControls.setPanelData(' + elID + ', ' + data + ', ' + key + ')');
    console.log(data);
    if (key == 'tows') {
      divIDE.panelJSData[elID][key] = data;
      if (data) {
        divIDE.onLinkDataChange('#' + elID, 'tows');
      }
    } else if (key == 'fromws') {
      divIDE.panelJSData[elID][key] = data;
    } else if (key == 'fromwsbinary' && data) {
      divIDE.panelJSData[elID][key] = data;
      var pc = $('[panelType=Mesh]');
      var pcpe = divIDE.getCtxTarget(pc);
      mesh.setPanelData(pcpe, data, 'binaryGeometry');
    }

  },
};

divIDE.registerPanel(transformerControls);
