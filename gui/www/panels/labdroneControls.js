
function acceptButtonClick(source) {
  var messageObject = {
    candidateExclusion: "accept"
  };
  var message = JSON.stringify(messageObject);
  webSocket.sendMsg(message)
  try {
  virtualLab.acceptCandidateExclusion();
  }
  catch {}
  return;
}

function rejectButtonClick(source) {
  var messageObject = {
    candidateExclusion: "reject"
  };
  var message = JSON.stringify(messageObject);
  webSocket.sendMsg(message)
    try {
  virtualLab.rejectCandidateExclusion();
  }
  catch {}
  return;
}

//This clearly doesn't work
function createTable(tableData) {
  var table = document.createElement('table');
  var tableBody = document.createElement('tbody');

  tableData.forEach(function(rowData) {
    var row = document.createElement('tr');

    rowData.forEach(function(cellData) {
      var cell = document.createElement('td');
      cell.appendChild(document.createTextNode(cellData));
      row.appendChild(cell);
    });

    tableBody.appendChild(row);
  });

  table.appendChild(tableBody);
  var originalTable = document.getElementByid("statusTable")
  $("#statusTable").html("")
}

labdroneControls = {
  name: "LabdroneControls",

  panelHTML: function(uniqueParentElementID) {
    
  //  $('#' + uniqueParentElementID + '-container').load('panels/labdroneControls.html', function() {});
    $('#' + uniqueParentElementID + '-container').load('panels/labdroneControls.html', function() {});
    //Note: the function is a callback for when the load method is complete
      return undefined;
  },

  linkDataKeys: [
    'tows', // To web socket as string
    'fromws', // From web socket as string
    'fromwsjson' // From web socket as json
  ],

  init: function(elID) {
    // Initialize Data Structure
    divIDE.panelJSData[elID] = {
      tows: '',
      fromws: '',
      fromwsjson: ''
    };
  },

  getPanelData: function(parentElement, key) {
    var elID = parentElement.attr('id');
    //TODO update
    console.log('TransformerControls.getPanelData(' + elID + ', ' + key + ') results in: ' + divIDE.panelJSData[elID][key]);
    if (key == 'tows') {
      return divIDE.panelJSData[elID][key];
    } else if (key == 'fromws') {
      return divIDE.panelJSData[elID][key];
    } else if (key == 'fromwsjson') {
      return divIDE.panelJSData[elID][key];
    } else {
      return;
    }
  },

  ready: function() {
    $.ajax({
      url: 'GUILayout.json',
      success: function(result) {
        try {
          layout.importLayout(JSON.parse(result));
        } catch (e) {
          if (!(typeof result === 'object' && result !== null)) {console.log(e); }
          else {layout.importLayout(result);}
          
        }
      }
    });
  },

  setPanelData: function(parentElement, data, key) {
    var elID = $(divIDE.getCtxTarget(parentElement)).attr('id');

    if (key == 'setFleetStatus'){
        labdroneControls.setFleetStatus(elID,data);
    }
    else if (key == 'setLog'){
        labdroneControls.setLog(elID,data)
    }
    else {
        console.log('unknown message in labdroneControls: ')
        console.log(data)
    }

  },

  setFleetStatus: function(elID,json_msg) {
      //console.log(json_msg)
  },

  setLog: function(elID,json_msg) {
      //console.log(json_msg)
  },

};

divIDE.registerPanel(labdroneControls);
