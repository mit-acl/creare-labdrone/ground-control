webSocket = {
    name: "webSocketDataPanel", 
    curElID: undefined,
    panelHTML: function(uniqueParentElementID) {
        var html = '';
        return html;
    }, 

    autoUrl: function(){
        var loc = window.location;
        var url = '';
        if (loc.protocol === "https:"){
            url = 'wss://';
        } else if (loc.protocol === "file:"){
            url = 'ws://localhost:8888'
        } else {
            url = 'ws://';
        }
        url += loc.host + '/ws';
        return url;
    },

    panelAdminHTML: function(uniqueParentElementID) {
        var html = '';
        html += '\
            <input type="text" class="wsurl" id="';
        html += uniqueParentElementID;
        html += '-wsurl" value="' + webSocket.autoUrl + '"/>\
            <button class="button" type="button" onclick="webSocket.panelConnect(this)">\
                Connect\
            </button>\
        ';
        return html;
    }, 
    /*
    panelContextMenu: {
    },
    topMenuItems : {
    },
    */

    linkDataKeys: [
        //'wsurl',
        //'stringData',
        //'binaryData',
        //'jsonData',
        //'labGeometry'
    ],

    getPanelData: function(parentElement, key){
      if (key == 'wsurl'){
         var ta = $(parentElement).find('.wsurl');
         return ta.val(); 
      } else{ 
         var datakey = parentElement.find('.wsurl').attr('id')
         return divIDE.panelJSData[datakey][key];
      }
    },

    setPanelData: function(parentElement, data, key) {
      if (key == 'wsurl'){
         if (data === 'auto'){
             data = webSocket.autoUrl();
         }
         var ta = $(parentElement).find('.wsurl');
         ta.val(data);  
         webSocket.panelConnect(parentElement);
      } else { 
         var datakey = parentElement.find('.wsurl').attr('id');
         try {
            return divIDE.panelJSData[datakey].ws.send(data);
         } catch(e) { 
            return '';
         }
      }

    },
    
    sendMsg:  function(data) {
         var pc = $('[panelType=Mesh]');
        var pcpe = divIDE.getCtxTarget(pc);
        return divIDE.panelJSData[webSocket.curElID].ws.send(data);
    },

    panelConnect: function(elem){
        if (1){
            var parentElement = divIDE.getCtxTarget(elem);
            var elem = $(parentElement).find('.wsurl');
            var datakey = elem.attr('id');
            var url = elem.val();
    
            webSocket.curElID = datakey;
    
            //console.log("parent element on panel connect: ", parentElement);
            //console.log(url);
            divIDE.panelJSData[datakey] = {
                stringData: '',
                binaryData: '',
                ws: new WebSocket(url),
            }
            divIDE.panelJSData[datakey].ws.binaryType = 'arraybuffer';
            divIDE.panelJSData[datakey].ws.onmessage = function (e) {
                if (typeof e.data == 'string'){
                    try {
                        var msgjson = JSON.parse(e.data);
                        var msgtype = msgjson.msgtype;
                        
                        divIDE.panelJSData[datakey][msgtype] = msgjson;
                        divIDE.onLinkDataChange($(parentElement).parent('div'), msgtype);
    
                    } catch(e) {
                        divIDE.panelJSData[datakey].stringData = e.data;
                        divIDE.onLinkDataChange($(parentElement).parent('div'), 'stringData');
                    }
                } else {
                    divIDE.panelJSData[datakey].binaryData = e.data;
                    divIDE.onLinkDataChange($(parentElement).parent('div'), 'binaryData');
                }
            }
        }
    }
}

divIDE.registerPanel(webSocket);