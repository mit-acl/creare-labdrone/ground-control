//var idontknowhowtodothisbetter;

function selectMesh(source) {
  //console.log ("selected",source)
}


virtualLab = {
    name: "virtualLab", 
    curElID: undefined,
    droneModel: undefined,
    panelHTML: function(uniqueParentElementID) {
        var html = '';
        html += ""
        return html;
    }, 
    
    acceptCandidateExclusion:  function() {
	
	var elID = virtualLab.curElID; 
	var scene = divIDE.panelJSData[elID].scene;
	exclusion = divIDE.panelJSData[elID].candidateExclusions.pop();
	scene.remove(exclusion);
	exclusion.material.color.set( 0xFFFFFF);
	scene.add(exclusion);
	divIDE.panelJSData[elID].labObjects.push(exclusion);
	//alert('ACCEPTED');
	//TODO work out specifics of which information is communicated. Does the GUI server cache the latest candidate exclusion? Is it retransmitted in full here?
	var messageObject = {
	    candidateExclusion: "accepted"
	};
	var message = JSON.stringify(messageObject);
	webSocket.sendMsg(message)
	
    },
    
    rejectCandidateExclusion:  function() {
	
	var elID = virtualLab.curElID; 
	var scene = divIDE.panelJSData[elID].scene;
	exclusion = divIDE.panelJSData[elID].candidateExclusions.pop();
	scene.remove(exclusion);
	exclusion.geometry.dispose();
	//alert('DELETED!');
	//TODO work out specifics of which information is communicated. Does the GUI server cache the latest candidate exclusion? Is it retransmitted in full here?
	var messageObject = {
	    candidateExclusion: "rejected"
	};
	var message = JSON.stringify(messageObject);
	webSocket.sendMsg(message)
    },
    
    /*
    panelContextMenu: function(parentElement) {
        // TODO implement     
    },
    topMenuItems : {
        
    },*/

    linkDataKeys: [
        //'stringGeometry',
        //'binaryGeometry',
        //'status',
        //'binaryDrawType',
    ],
    
	
    ready: function() {
	$.ajax({
	  url: 'GUILayout.json',
	  success: function(result) {
	    try {
	      layout.importLayout(JSON.parse(result));
	} catch (e) {
	  if (!(typeof result === 'object' && result !== null)) {console.log(e); }
	  else {layout.importLayout(result);}
	    }
	  }
	});
      },


    getPanelData: function(parentElement, key){
      var container = $(divIDE.getCtxTarget(parentElement));
      var elID = container.attr('id'); 
      if (key == 'stringGeometry'){
         return;  
      } else if (key == 'binaryGeometry') { 
         return;
      } else if (key == 'status') {
          return divIDE.panelJSData[elID].status;
      } else if (key == 'binaryDrawType') {
          return divIDE.panelJSData[elID].binaryDrawType;
      } else {
          return ;
      }
    },

    setPanelData: function(parentElement, data, key)
    {
	//var elID = container.attr('id');
	if (parentElement == 'acceptButton')
	{
	    alert('ACCEPTED');
	    //for (i = 0; i < divIDE.panelJSData[elID].labObjects.length; i++)
	    //{ 
		//scene.remove(divIDE.panelJSData[elID].labObjects[i]);
		//divIDE.panelJSData[elID].labObjects[i].geometry.dispose();
	    //}
	    //divIDE.panelJSData[elID].labObjects = []
        }
	else if (parentElement == 'rejectButton')
	{
	    alert('REJECTED');
	}
	
	
      var elID = parentElement.attr('id'); 
      
      if (key == 'setLabObjects'){
	virtualLab.setLabObjects(elID, data);
      } else if (key == 'setDroneInfo') { 
	virtualLab.setDroneInfo(elID, data);
//      } else if (key == 'setFleetStatus') { 
//  virtualLab.setFleetStatus(elID, data);
      } else if (key == 'setActivePaths') { 
        virtualLab.setActivePaths(elID, data);
      } else if (key == 'status') { 
         divIDE.panelJSData[elID].status = data;
      } else if (key == 'setCandidateExclusion') { 
	 virtualLab.setCandidateExclusion(elID, data);
      } else {
	console.log(key);
          return;
      }
    },

    init: function (elID){
        var showStats = false;

        if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

        panelContainer = $('#' + elID);
	//console.log(panelContainer);

        var container;
        var camera, scene, renderer, material, i, h, color, sprite, size, raycaster;
        var controls;
        var mouseX = 0, mouseY = 0;
        var pcPtRange = 0;
        var pcMaxRange = 0;
        var pcNextI = 0;

        var windowHalfX = panelContainer.width() / 2;
        var windowHalfY = panelContainer.height() / 2;

        // Initialize panelContainer
        container = document.createElement( 'div' );
        panelContainer.append( container );

        camera = new THREE.PerspectiveCamera( 55, panelContainer.width() / panelContainer.height(), 0.05, 200 );
        camera.position.z = 0;
        camera.position.x = 1;
        camera.position.y = 3;
        camera.up.set(0, 0, 1);

        //

        scene = new THREE.Scene();
	scene.background = new THREE.Color( 0xffffff );
	var grid = new THREE.GridHelper( 200, 100, 0xbf819d, 0xbf819d );
	grid.rotation.x = - Math.PI / 2;
	grid.material.opacity = 0.2;
	grid.material.transparent = true;
	scene.add( grid );
	var ground = new THREE.Mesh( new THREE.PlaneBufferGeometry( 200, 200 ), new THREE.MeshPhongMaterial( { color: 0x2f1972, depthWrite: false } ) );
	//ground.rotation.x = - Math.PI / 2;
	scene.add( ground );
	
	var light = new THREE.PointLight( 0xffffff, 100, 100 );
	light.position.set( 0, 10, 0 );
	scene.add( light );


        material = new THREE.PointsMaterial( { 
            size: 1,
            vertexColors: THREE.VertexColors,
            sizeAttenuation: true
//             alphaTest: 0.5, 
//             transparent: true, 
//             opacity: 0.9 
//                 	color: 0x555555,
// 	                map: sprite
         } );
	var drone_geometry;
	var loader = new THREE.STLLoader();
	loader.load( './models/Drone_50pct.STL', function ( geometry ) {
	//loader.load( './models/pony.stl', function ( geometry ) {
	    virtualLab.droneModel = geometry;

	}, function(xhr){}, function (err) {console.error(err)} );

        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( panelContainer.width(), panelContainer.height() );
        container.appendChild( renderer.domElement );
	raycaster = new THREE.Raycaster();

        // 				

        controls = new THREE.OrbitControls( camera, renderer.domElement );
        controls.addEventListener( 'change', virtualLab.render );

        //
        if (showStats) {
            var stats;
            stats = new Stats();
            container.appendChild( stats.dom );
        }

        //
        // BET THIS WON'T WORK, Need the equivalent for a div... 
        // THIS STILL DOESN"T WORK
        //panelContainer[0].addEventListener( 'resize', virtualLab.onWindowResize, false );
        window.addEventListener( 'resize', virtualLab.onWindowResize, false );
        // NEED TO CHANGE CUREL
        panelContainer.click(function (event) {
           var container = $(divIDE.getCtxTarget(event.target));
           virtualLab.curElID = container.attr('id'); 
        });
        renderer.domElement.addEventListener("dblclick", virtualLab.ondblclick, false);
	renderer.domElement.addEventListener('mousedown', virtualLab.onDocumentMouseDown, false);
        divIDE.panelJSData[elID] = {
            container: container,
            camera: camera, 
            scene: scene, 
            renderer: renderer, 
            material: material, 
            i: i, 
            h: h, 
            color: color, 
            sprite: sprite, 
            size: size, 
            controls: controls,
            mouseX: mouseX,
            mouseY: mouseY,
            pcPtRange: pcPtRange,
            pcMaxRange: pcMaxRange,
            pcNextI: pcNextI,
            windowHalfX: windowHalfX,
            windowHalfY: windowHalfY,
            status: 'ready',
            binaryDrawType: 'match',
            rayCastThreshold: 1,
            labBoundsObject: null,
	          labPathObjects: [],
            labEndpointObjects: [],
            labObstructionObjects: [],
	    droneObjects: [],
	    raycaster: raycaster,
	    labname: "",
	    bounds: {},
	    exclusions: [],
	    paths: [],
	    endpoints: [],
	    candidateExclusions: []
	    
	    
        };

        if (showStats){
            divIDE.panelJSData[elID].stats = stats;
        };

        // Animate 
        virtualLab.curElID = elID;
        virtualLab.animate();
        virtualLab.onWindowResize();
    },

        
    incrementPcNextI: function (pcNextI, pcMaxRange){
        pcNextI += 1;
        if (pcNextI == pcMaxRange){
            pcNextI = 0;
        }
        return pcNextI;
    },

    onWindowResize: function () {
        var elID = virtualLab.curElID;
        var panelContainer = $('#' + elID);
                
        var windowHalfX = panelContainer.width() / 2;
        var windowHalfY = panelContainer.height() / 2;

        if (divIDE.panelJSData[elID].windowHalfX == windowHalfX &&
        divIDE.panelJSData[elID].windowHalfY == windowHalfY){
            return;
        }
        divIDE.panelJSData[elID].windowHalfX = windowHalfX;
        divIDE.panelJSData[elID].windowHalfY = windowHalfY;

        var camera = divIDE.panelJSData[elID].camera;
        var renderer = divIDE.panelJSData[elID].renderer;

        camera.aspect = panelContainer.width() / panelContainer.height();
        camera.updateProjectionMatrix();

        renderer.setSize( panelContainer.width(), panelContainer.height() );
    },


    onDocumentMouseDown: function ( event ) {
	var elID = virtualLab.curElID;

	event.preventDefault();
	
	mouse = new THREE.Vector2()
	renderer = divIDE.panelJSData[elID].renderer
	raycaster = divIDE.panelJSData[elID].raycaster
	camera = divIDE.panelJSData[elID].camera
	objects = divIDE.panelJSData[elID].labObjects
	
	mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;
	
	raycaster.setFromCamera( mouse, camera );
    
	//var intersects = raycaster.intersectObjects( objects ); 
    
	//if ( intersects.length > 0 ) {
	    //intersects[0].object.material.color.setHex( Math.random() * 0xffffff );
	    //intersects[0].object.callback();
	//}

    },






    // Functionality to change the focus point of the orbit controls
    ondblclick: function (event) {/*
        var elID = virtualLab.curElID;
        var panelContainer = $('#' + elID);
        var camera = divIDE.panelJSData[elID].camera;
        var controls = divIDE.panelJSData[elID].controls;
        var particles = divIDE.panelJSData[elID].binaryParticles;
        if (particles == undefined){
            particles = divIDE.panelJSData[elID].stringParticles;
        }
        if (particles == undefined){
            return;
        }

                
        x = ((event.clientX - panelContainer.offset().left) / panelContainer.innerWidth()) * 2 - 1;
        y = -((event.clientY - panelContainer.offset().top) / panelContainer.innerHeight()) * 2 + 1;
        dir = new THREE.Vector3(x, y, -1)
        dir.unproject(camera)

        ray = new THREE.Raycaster(camera.position, dir.sub(camera.position).normalize());
        ray.params.Points.threshold = divIDE.panelJSData[elID].rayCastThreshold;
        var intersects = ray.intersectObject(particles);
        if ( intersects.length > 0 )
        {
            var minI = 0;
            var minD = intersects[0].distanceToRay;
            for (var i=0; i < intersects.length; i++){
                if (minD > intersects[i].distanceToRay){
                    minI = i;
                    minD = intersects[i].distanceToRay;
                }
            }
            controls.target.set(intersects[minI].point.x, intersects[minI].point.y, intersects[minI].point.z);
        }
		*/
    },    

    animate: function() {
        var elID = virtualLab.curElID;
        var controls = divIDE.panelJSData[elID].controls;
        var stats = divIDE.panelJSData[elID].stats;
        requestAnimationFrame( virtualLab.animate );
        controls.update()

        virtualLab.render(elID);
        if (stats !== undefined){
            stats.update();    
        }
    },

    render: function () {
        var elID = virtualLab.curElID;
        var scene = divIDE.panelJSData[elID].scene;

	var camera = divIDE.panelJSData[elID].camera;

        divIDE.panelJSData[elID].renderer.render( scene, camera );

        virtualLab.onWindowResize();
    },

    setLabObjects: function (elID, json_obj_array){
        divIDE.panelJSData[elID].status = 'busy';
        divIDE.onLinkDataChange($('#' + elID), 'status');
	console.log("I exist!!");
	console.log(json_obj_array);
        var scene = divIDE.panelJSData[elID].scene;


	//Dispose of old lab objects.
  if (divIDE.panelJSData[elID].labBoundsObject !== null) {
    scene.remove(divIDE.panelJSData[elID].labBoundsObject);
    divIDE.panelJSData[elID].labBoundsObject.geometry.dispose();
  }
  divIDE.panelJSData[elID].labBoundsObject = null;

	for (i = 0; i < divIDE.panelJSData[elID].labPathObjects.length; i++) {
            scene.remove(divIDE.panelJSData[elID].labPathObjects[i]);
            divIDE.panelJSData[elID].labPathObjects[i].geometry.dispose();
        }
	divIDE.panelJSData[elID].labPathObjects = [];

	for (i = 0; i < divIDE.panelJSData[elID].labEndpointObjects.length; i++) {
            scene.remove(divIDE.panelJSData[elID].labEndpointObjects[i]);
            divIDE.panelJSData[elID].labEndpointObjects[i].geometry.dispose();
        }
	divIDE.panelJSData[elID].labEndpointObjects = [];

	for (i = 0; i < divIDE.panelJSData[elID].labObstructionObjects.length; i++) {
            scene.remove(divIDE.panelJSData[elID].labObstructionObjects[i]);
            divIDE.panelJSData[elID].labObstructionObjects[i].geometry.dispose();
        }
	divIDE.panelJSData[elID].labObstructionObjects = [];


        try {
	    var failed_parse = false;
	    var lab_model = json_obj_array.data;
        } catch (e){
	    var failed_parse = true;
          console.log("Failed lab model parse")
          var lab_model = {};
        }
		var entered_switch = false;

		//TODO Add these properties
		divIDE.panelJSData[elID].labName = lab_model.name;
		divIDE.panelJSData[elID].bounds = lab_model.bounds;
		divIDE.panelJSData[elID].exclusions = lab_model.exclusions;
		divIDE.panelJSData[elID].paths = lab_model.paths;
		divIDE.panelJSData[elID].endpoints = lab_model.endpoints;
		//#TODO Render bounds
		obj = lab_model.bounds;
		var geometry = new THREE.BoxGeometry( obj.length.x, obj.length.y, obj.length.z );
		var material = new THREE.MeshBasicMaterial( {wireframe: true,} );
		material.color.set( 0xCCCCCC) //color can't be passed in the constructor. However, this doesn't seem to work
		var cuboid = new THREE.Mesh( geometry, material );
		scene.add( cuboid );
		//console.log(obj.centroid_x, obj.centroid_z, obj.centroid_y);
		cuboid.position.set(obj.centroid.x, obj.centroid.y, obj.centroid.z);
		cuboid.rotateZ(obj.theta_z);
		cuboid.callback = function() { selectMesh( this.name ); }
		divIDE.panelJSData[elID].labBoundsObject = cuboid;

		//Render exclusions
		for (i = 0; i < lab_model.exclusions.length; i++) {
		    obj = lab_model.exclusions[i];
		    var geometry = new THREE.BoxGeometry( obj.length.x, obj.length.y, obj.length.z );
		    var material = new THREE.MeshBasicMaterial( {wireframe: false, transparent: true, opacity: 0.9} );
		    material.color.set( 0xFFFFFF) //color can't be passed in the constructor. However, this doesn't seem to work
		    var cuboid = new THREE.Mesh( geometry, material );
		    scene.add( cuboid );
		    cuboid.position.set(obj.centroid.x,obj.centroid.y,obj.centroid.z);
		    cuboid.rotateZ(obj.theta_z);
		    cuboid.callback = function() { selectMesh( this.name ); }
		    divIDE.panelJSData[elID].labObstructionObjects.push(cuboid);
		}
    		//Render paths
		for (i = 0; i < lab_model.paths.length; i++) {
		    console.log("rendering tubes?");
		    obj = lab_model.paths[i];
		    path = [];
		    for (j = 0; j < obj.points.length; j++) {
			path.push(new THREE.Vector3(obj.points[j].x,obj.points[j].y,obj.points[j].z));
		    }
		    var spline = new THREE.CatmullRomCurve3(path);
		    var geometry = new THREE.TubeGeometry(spline,20,0.5,8,false);
		    var material = new THREE.MeshBasicMaterial({color: 0xff0000, transparent: true, opacity: 0.5});
		    var tube = new THREE.Mesh( geometry, material );
		    tube.callback = function() { selectMesh( this.name ); }
		    scene.add( tube );
		    divIDE.panelJSData[elID].labPathObjects.push(tube);
		}


		//Render endpoints
		for (i = 0; i < lab_model.endpoints.length; i++) {
		    obj = lab_model.endpoints[i];
		    var geometry = new THREE.SphereGeometry( 0.5, 32, 32 ); //Radius, numWidthSegments, numHeightSegments
		    var material = new THREE.MeshBasicMaterial( {color: 0x662E6B, wireframe: false} );
		    material.color.set( 0x662E6B) //color can't be passed in the constructor. However, this doesn't seem to work
		    var sphere = new THREE.Mesh( geometry, material );
		    scene.add( sphere );
		    sphere.position.set(obj.pose.x,obj.pose.y,obj.pose.z);
		    sphere.callback = function() { selectMesh( this.name ); }
		    divIDE.panelJSData[elID].labEndpointObjects.push(sphere);
		}

        divIDE.panelJSData[elID].status = 'ready';
        divIDE.onLinkDataChange($('#' + elID), 'status');

    },
    
        setCandidateExclusion: function (elID, json_obj){
        divIDE.panelJSData[elID].status = 'busy';
        divIDE.onLinkDataChange($('#' + elID), 'status');
        var scene = divIDE.panelJSData[elID].scene;
	    
	//console.log(divIDE.panelJSData[elID].candidateExclusions);
	if (divIDE.panelJSData[elID].candidateExclusions === []) {
	    console.log("");
	}
	else {
	    for (i = 0; i < divIDE.panelJSData[elID].candidateExclusions.length; i++) { 
	    scene.remove(divIDE.panelJSData[elID].candidateExclusions[i]);
	    divIDE.panelJSData[elID].candidateExclusions[i].geometry.dispose();
	    divIDE.panelJSData[elID].candidateExclusions = [];
	    }
	}	
		
	console.log(json_obj);
        try {
	    var failed_parse = false;
	    var obj = json_obj.data;
	    
	    var geometry = new THREE.BoxGeometry( obj.length.x, obj.length.y, obj.length.z );
	    var material = new THREE.MeshBasicMaterial( {wireframe: false, transparent: true, opacity: 0.9} );
	    material.color.set( 0xFF0000) //color can't be passed in the constructor. However, this doesn't seem to work
	    var cuboid = new THREE.Mesh( geometry, material );
	    scene.add( cuboid );
	    cuboid.position.set(obj.centroid.x,obj.centroid.y,obj.centroid.z);
	    cuboid.rotateZ(obj.theta_z);
	    cuboid.callback = function() { selectMesh( this.name ); }
	    divIDE.panelJSData[elID].candidateExclusions.push(cuboid);

        } catch (e){
	    var failed_parse = true;
        }

        divIDE.panelJSData[elID].status = 'ready';
        divIDE.onLinkDataChange($('#' + elID), 'status');

    },
    
    setActivePaths: function (elID, json_obj){
    divIDE.panelJSData[elID].status = 'busy';
    divIDE.onLinkDataChange($('#' + elID), 'status');
    var scene = divIDE.panelJSData[elID].scene;
  
console.log(json_obj);
try {
  var failed_parse = false;
  var index_array = json_obj.data;
  for (i = 0; i < divIDE.panelJSData[elID].labPathObjects.length; i++)
  {
    divIDE.panelJSData[elID].labPathObjects[i].visible = false;
  }
  for (i = 0; i < index_array.length; i++)
  {
      cur_index = index_array[i].index;
      for (j = 0; j < divIDE.panelJSData[elID].paths.length; j++)
      {
        if (divIDE.panelJSData[elID].paths[j].id === cur_index)
        {
          divIDE.panelJSData[elID].labPathObjects[j].visible = true;
        }
      }
    }
  } catch (e){
  var failed_parse = true;
    }

    divIDE.panelJSData[elID].status = 'ready';
    divIDE.onLinkDataChange($('#' + elID), 'status');
},
    
//    setFleetStatus: function (elID, json_status_array) {
//      console.log ("beep boop")
//},
    
    setDroneInfo: function (elID, json_obj_array) {
        divIDE.panelJSData[elID].status = 'busy';
        divIDE.onLinkDataChange($('#' + elID), 'status');

        var scene = divIDE.panelJSData[elID].scene;

	try {
	    var obj_array = json_obj_array.data
	} catch (e){
	    var obj_array = [];
	}

	if (divIDE.panelJSData[elID].droneObjects.length > 0) {
		for (i = 0; i < divIDE.panelJSData[elID].droneObjects.length; i++) {
		    obj = obj_array[i];
		    divIDE.panelJSData[elID].droneObjects[i].position.set(obj.position[0],obj.position[1],obj.position[2]);
	    }
	}
	else {
	    //Dispose of old drone objects.
	    for (i = 0; i < divIDE.panelJSData[elID].droneObjects.length; i++) { 
	    
		scene.remove(divIDE.panelJSData[elID].droneObjects[i]);
		divIDE.panelJSData[elID].droneObjects[i].geometry.dispose();
	    }
	    divIDE.panelJSData[elID].droneObjects = []
		    
		    
		    
	    //try {
		//var obj_array = json_obj_array.data
	    //} catch (e){
		//var obj_array = [];
	    //}
	    for (i = 0; i < obj_array.length; i++) { 
		var obj = obj_array[i]
		var radius = 0.5
		var segments = 32
		var geometry = virtualLab.droneModel;//divIDE.panelJSData[elID].drone_geometry;
		//var geometry = new THREE.SphereGeometry( radius, segments, segments );
		//var material = new THREE.MeshBasicMaterial( {color: 0x00ffff, } );
		var material = new THREE.MeshPhongMaterial( { color: 0x006478,transparent: false, depthWrite: false, wireframe: false, opacity:1.0 } )
		material.color.set( 0x006478) //color can't be passed in the constructor. However, this doesn't seem to work
		var testDrone = new THREE.Mesh( geometry, material );
		testDrone.scale.set (0.001, 0.001, 0.001); //Drone scale
		//testDrone.scale.set (0.1, 0.1, 0.1); //Pony scale
		scene.add( testDrone );
		testDrone.position.set(obj.position[0],obj.position[1],obj.position[2]);
		testDrone.rotateX(Math.PI/2);
		testDrone.callback = function() { selectMesh( this.name ); }
		divIDE.panelJSData[elID].droneObjects.push(testDrone);
	    }
	}
		

        divIDE.panelJSData[elID].status = 'ready';
        divIDE.onLinkDataChange($('#' + elID), 'status');

    }
}

divIDE.registerPanel(virtualLab);
