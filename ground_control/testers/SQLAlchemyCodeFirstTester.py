import numpy as np
import sqlalchemy as sqla
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.mysql import DOUBLE




engine = sqla.create_engine('mysql+mysqlconnector://root:labdrone@localhost')
engine.execute("CREATE DATABASE IF NOT EXISTS labdrone_code_first")
engine.execute("USE labdrone_code_first")
engine.echo = True

meta = sqla.MetaData(bind=engine)

##Reference pre-existing metadata
Base = declarative_base(metadata=meta)

#### Creation testing

class Cuboid(Base):
    """
    Primitive descriptor for objects in lab model.
    """
    __tablename__ = 'cuboids'
    pk = sqla.Column(sqla.Integer, primary_key = True, autoincrement = True)
    
    centroid_x = sqla.Column(DOUBLE)
    centroid_y = sqla.Column(DOUBLE)
    centroid_z = sqla.Column(DOUBLE)
    theta_z = sqla.Column(DOUBLE)
    length_x = sqla.Column(DOUBLE)
    length_y = sqla.Column(DOUBLE)
    length_z = sqla.Column(DOUBLE)
    
    lab_model = sqla.orm.relationship("LabModel", back_populates="cuboid")
    exclusions = sqla.orm.relationship("Exclusion", uselist=False, back_populates="cuboids")
    

class LabModel(Base):
    """
    Highest logical level of organization. A lab model owns a set of exclusions, poses of interest, paths, etc... 
    """
    __tablename__ = 'lab_model'
    pk = sqla.Column(sqla.Integer, primary_key = True, autoincrement = True)
    fk_bounds = sqla.Column(sqla.Integer, sqla.ForeignKey('cuboids.pk'))
    
    name = sqla.Column(sqla.String(64))

    cuboid = sqla.orm.relationship("Cuboid", uselist=False, back_populates="lab_model")
    exclusions = sqla.orm.relationship("Exclusion")
    paths = sqla.orm.relationship("Path")
    special_objects = sqla.orm.relationship("SpecialObject")
    
    #Explicitly declaring this as python code
    @hybrid_property
    def bounds(self):
        return None
    
        #return np.array(self.bounds_x_min,\
                        #self.bounds_x_max,\
                        #self.bounds_y_min,\
                        #self.bounds_y_max,\
                        #self.bounds_z_min,\
                        #self.bounds_z_max)
    
class Exclusion(Base):
    __tablename__ = 'exclusions'
    pk = sqla.Column(sqla.Integer, primary_key = True, autoincrement = True)
    fk_lab_model = sqla.Column(sqla.Integer, sqla.ForeignKey('lab_model.pk'))
    fk_cuboids = sqla.Column(sqla.Integer, sqla.ForeignKey('cuboids.pk'))
    fk_exclusion_type = sqla.Column(sqla.Integer, sqla.ForeignKey('exclusion_type.pk'))
    
    
    #TODO look up 1-1 syntax.
    cuboids = sqla.orm.relationship("Cuboid", uselist=False, back_populates="exclusions")
    exclusion_type = sqla.orm.relationship("ExclusionType", uselist=False, back_populates="exclusions")
    

class ExclusionType(Base):
    """
    Enum for exclusions.
    Might be unneccessary.
    """
    __tablename__ = 'exclusion_type'
    pk = sqla.Column(sqla.Integer, primary_key = True, autoincrement = True)
    
    name = sqla.Column(sqla.String(64))
    
    exclusions = sqla.orm.relationship("Exclusion", back_populates="exclusion_type")

class Path(Base):
    """
    Defined paths.
    """
    __tablename__ = 'path'
    pk = sqla.Column(sqla.Integer, primary_key = True, autoincrement = True)
    fk_lab_model = sqla.Column(sqla.Integer, sqla.ForeignKey('lab_model.pk'))
    
    num_pts = sqla.Column(sqla.Integer)
    name = sqla.Column(sqla.String(64))
    
    pts = sqla.orm.relationship("PathPose")
    
    #@hybrid_property
    #path_array = None
    
    @hybrid_property
    #if self.path_array is None:
    def pose_array(self):
        sorted_pts = sorted(self.pts, key=lambda x: x.path_index, reverse=False)
        return np.array([pt.pose for pt in sorted_pts])
    

class PathPose(Base):
    """
    Components of path. Path index specifies order in path definition.
    """
    __tablename__ = 'path_pose'
    pk = sqla.Column(sqla.Integer, primary_key = True, autoincrement = True)
    fk_path = sqla.Column(sqla.Integer, sqla.ForeignKey('path.pk'))
    #Not sure what the value-add for storing null quaternion data is unless we plan to record full pose when defining paths.
    fk_pose = sqla.Column(sqla.Integer, sqla.ForeignKey('pose.pk'))
    
    path_index = sqla.Column(sqla.Integer)
    pose = sqla.orm.relationship("Pose", uselist=False, back_populates="path_pose")

    
class Pose(Base):
    __tablename__ = 'pose'
    pk = sqla.Column(sqla.Integer, primary_key = True, autoincrement = True)
    
    x = sqla.Column(DOUBLE)
    y = sqla.Column(DOUBLE)
    z = sqla.Column(DOUBLE)
    q1 = sqla.Column(DOUBLE)
    q2 = sqla.Column(DOUBLE)
    q3 = sqla.Column(DOUBLE)
    q4 = sqla.Column(DOUBLE)
    
    #Not every pose <--> path pose. Does this make sense?
    path_pose = sqla.orm.relationship("PathPose", back_populates="pose")
    special_object = sqla.orm.relationship("SpecialObject", back_populates="pose")
    
class SpecialObject(Base):
    """
    Pose of special objects, e.g. slots
    """
    __tablename__ = 'special_object'
    pk = sqla.Column(sqla.Integer, primary_key = True, autoincrement = True)
    fk_lab_model = sqla.Column(sqla.Integer, sqla.ForeignKey('lab_model.pk'))
    fk_pose = sqla.Column(sqla.Integer, sqla.ForeignKey('pose.pk'))
    fk_object_type = sqla.Column(sqla.Integer, sqla.ForeignKey('special_object_type.pk'))
    
    name = sqla.Column(sqla.String(64))
    pose = sqla.orm.relationship("Pose", uselist=False, back_populates="special_object")
    special_object_type = sqla.orm.relationship("SpecialObjectType", uselist=False, back_populates="special_object")
    
class SpecialObjectType(Base):
    """
    Enum of special objects
    """
    __tablename__ = 'special_object_type'
    pk = sqla.Column(sqla.Integer, primary_key = True, autoincrement = True)
    
    name = sqla.Column(sqla.String(64))
    
    special_object = sqla.orm.relationship("SpecialObject", back_populates="special_object_type")

    

#Drop all tables to reset database
# Base.metadata.drop_all(bind=engine)

#Create tables
Base.metadata.create_all(bind=engine)


#### Insert testing

exclusion_type = sqla.Table('exclusion_type', meta, autoload=True)
lab_model = sqla.Table('lab_model',meta,autoload=True)
exclusions = sqla.Table('exclusions',meta,autoload=True)

#This insert didn't work and didn't return an error.
#Session = sqla.orm.sessionmaker(bind=engine)
#session = Session()

#i = sqla.sql.insert(exclusion_type)
#i = i.values({"name":"pointy looking box" })
#session.commit()

#This did work
#con = engine.connect()
#i = sqla.sql.insert(exclusion_type).values(name="pointy looking box")
#con.execute(i)
#i = sqla.sql.insert(lab_model).values(name="dummy")
#con.execute(i)
#i = sqla.sql.insert(exclusions).values(pt1=0.0,pt2=2.2,pt3=4,fk_lab_model=1,fk_exclusion_type=10)
#con.execute(i)


#Works with manual FK specification
#i = sqla.sql.insert(Cuboid).values(centroid_x=10,centroid_y=10,centroid_z=10,theta_z=30,length_x=5,length_y=5,length_z=5)
#con.execute(i)
#i = sqla.sql.insert(LabModel).values(fk_bounds=1,name='test')
#con.execute(i)

#Trying with objects. Works!
Session = sessionmaker(bind=engine)
session = Session()

#Create labmodel
lab_bounds = Cuboid(centroid_x=10.,centroid_y=10.,centroid_z=10.,theta_z=0.0,length_x=20.,length_y=20.,length_z=20.)
lab_model = LabModel(name='lab_test',cuboid=lab_bounds)
# session.add(lab_model)
# session.commit()



#Modify existing entry
#obj = session.query(LabModel).filter_by(name='classtest').first()
#print(obj.cuboid.theta_z)
#print(obj.paths)
#obj.name='modified_name'
#session.commit()

#Add paths to existing labmodel
#lab_model = session.query(LabModel).filter_by(name='modified_name').first()
dummy_path = [np.array([3.,1.,4.]), np.array([1.,5.,9.]), np.array([9.,9.,9.])]

path_poses = []
for i,item in enumerate(dummy_path):
    #NOTE: the explicit float cast is necessary because mysql can't deal with float64 used by numpy. We probably want some sort of converter class somewhere.
    path_poses.append(PathPose(path_index=i,
                                   pose=Pose(x=item[0],
                                             y=item[1],
                                             z=item[2]
                                             ))) #,q1=0,q2=0,q3=0,q4=0)))
                               # pose=Pose(x=float(item[0]),\
                               #           y=float(item[1]),\
                               #           z=float(item[2])\
                               #           )))#,q1=0,q2=0,q3=0,q4=0)))
    
new_path = Path(name='new_path',num_pts=len(dummy_path),pts=path_poses)
lab_model.paths.append(new_path)
session.add(lab_model)
session.commit()

#Test hybrid property/querying 1-many fields
# obj = session.query(LabModel).filter_by(name='modified_name').first()
# print(obj.cuboid.theta_z)
# for path in obj.paths:
#     print(path.name)
#     print(path.pose_array)
#     for item in path.pose_array:
#         print(item.x,item.y,item.z)


#session.close()