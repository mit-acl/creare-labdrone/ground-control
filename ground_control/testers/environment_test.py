# -*- coding: utf-8 -*-

from environment import Environment
from geometry import Cuboid
from geometry import Path
from geometry import Pose

import database as db


labjson = None
labdict = None

with db.Database() as dbase:
    sess = dbase.session
    lab = sess.query(db.LabModelSQL).first()
    labjson = lab.to_json_pretty()
    labdict = lab.dict
    print(labjson)
    
print("Creating Laboratory Environment from JSON pulled from database.")
labenv = Environment.from_json(JSON=labjson)
p = Pose(JSON={'x':10, 'y':10, 'z':10})
p1 = Pose(JSON={'x':1, 'y':1, 'z':1})
p2 = Pose(JSON={'x':1, 'y':2, 'z':1})
p3 = Pose(JSON={'x':0, 'y':2, 'z':1})
p4 = Pose(JSON={'x':0.2, 'y':5, 'z':1})
p5 = Pose(JSON={'x':2, 'y':5, 'z':1})
p6 = Pose(JSON={'x':18.7, 'y':15, 'z':19})

path1 = Path(points=[p,p1,p2])
path2 = Path(points=[p3,p4,p5,p6])

print("Adding some paths.")
labenv.add_path(path1)
labenv.add_path(path2)

start_test = Pose(JSON={'x':1.1, 'y': 1.1, 'z':1.1})
end_test = Pose(JSON={'x':10.1, 'y': 9.9, 'z':10.1})
print("Finding path closest to (start: {}, end: {})".format(start_test.to_json(), end_test.to_json()))

bleh = labenv.get_paths_by_location(start_test, end_test)

for p in bleh.points:
    print(p)

print("\n Lab bounds: {}".format(labenv.bounds.to_json()))