import roslib
import rospy
# import rosservice

from acl_msgs.msg import HexSMData, HexSMState, HexData
from acl_msgs.srv import GetHexSMData
from geometry_msgs.msg import Vector3
# from std_srvs.srv import Trigger, TriggerResponse

# master = rospy.get_master()
# print(master)
# topics = rospy.get_published_topics()
# print(topics)
# services = rosservice.get_service_list()
# print(services)

def mysrv_handler(req):
    print("mysrv called. Assuming two args. They are %s and %s","foo","bar")
    return_value = HexSMData()
    return_value.state.mode = HexSMState.EMERGENCY_LANDING
    return(return_value)#state=HexSMState.START_UP,pathArray=[],pathIndex=0,payloadId="NA",controlData=HexData()))

rospy.init_node('my_node')
# mysrv = rospy.service('mysrv',acl_msgs.srv.GetHexSMData,mysrv_handler)
mysrv = rospy.Service('mysrv',GetHexSMData,mysrv_handler)


rospy.spin()
