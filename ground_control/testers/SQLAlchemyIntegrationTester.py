import numpy as np
# import logging
# print("Loading logging basic config.")
# logging.basicConfig()
# log = logging.getLogger(__name__)


#Convenience testing hack
import sys
import json
from collections import OrderedDict 

sys.path.append("C:\Projects\labdroneInternal\ground_control\ground_control")


print("importing the database module, which should create the labdrone database if it doesn't exist")
import database as db
print("Deleting tables")
db._delete_tables()
print("Trying to create the tables")
db.create_tables()

path = None
bounds = None

with db.Database() as dbase:

    sess = dbase.session

    lab_bounds = db.CuboidsSQL(centroid_x=10.,centroid_y=10.,centroid_z=10.,theta_z=0.0,length_x=20.,length_y=20.,length_z=20.)
    lab_model = db.LabModelSQL(name='lab_test',cuboid=lab_bounds)
    
    dummy_path = np.array([[3.,1.,4.], [1.,5.,9.], [9.,9.,9.]])
    
    path_poses = []
    for i,item in enumerate(dummy_path):
        #NOTE: the explicit float cast is necessary because mysql can't deal with float64 used by numpy. We probably want some sort of converter class somewhere.
        path_poses.append(db.PathPoseSQL(path_index=i,
                                       pose=db.PoseSQL(x=item[0],
                                                 y=item[1],
                                                 z=item[2]
                                                 ))) #,q1=0,q2=0,q3=0,q4=0)))
                                   # pose=Pose(x=float(item[0]),\
                                   #           y=float(item[1]),\
                                   #           z=float(item[2])\
                                   #           )))#,q1=0,q2=0,q3=0,q4=0)))
    
    new_path = db.PathSQL(name='new_path',num_pts=len(dummy_path),pts=path_poses)
    new_dock = db.EndpointSQL(endpoint_type=db.EndpointTypes.docking_station, name="dock1",pose=db.PoseSQL(x=3.3,y=2.2,z=1.1, q1=0.1, q2=0.2, q3=0.3, q4=1.0), wait_at = db.PoseSQL(x=0.5, y=0.5, z=0.5))
    new_exclusion_cuboid = db.CuboidsSQL(centroid_x=1.,centroid_y=5.,centroid_z=2.,theta_z=30.0,length_x=2.,length_y=1.,length_z=4.)
    new_exclusion = db.ExclusionSQL(cuboid=new_exclusion_cuboid)#,lab_model=lab_model)
    

    lab_model.paths.append(new_path)
    lab_model.endpoints.append(new_dock)
    lab_model.exclusions.append(new_exclusion)
    
    print("JKP: LAB MODEL JSON: ")
    print(json.dumps(json.loads(lab_model.json), indent=4))
    sess.add(lab_model)
    sess.commit()
    
    lab_bounds_dict = {"centroid":{
                        "x": 10, 
                        "y": 10, 
                        "z": 2},
                    "theta_z": 0.0,
                    "length":{
                        "x": 20, 
                        "y": 20, 
                        "z": 4}}
    lab_bounds_from_dict = db.CuboidsSQL.from_dict(lab_bounds_dict)
    cuboid_dict = { 
            "centroid":{
                "x": 1.1, 
                "y": 1.1, 
                "z": 1.1},
            "theta_z": 0.0,
            "length":{
                "x": 3.0, 
                "y": 3.0, 
                "z": 3.0}
            }
    cuboid_from_dict = db.CuboidsSQL.from_dict(cuboid_dict)
    
    lab_model_from_dicts = db.LabModelSQL(name='lab_dict_test',cuboid=lab_bounds_from_dict)
    
    dummy_path_2 = dummy_path - 1.0
    
    print("\n  >>>  JKP: LAB FROM DICT TEST:")
    print("\n")
    exclusion_from_dict_cuboid = db.ExclusionSQL(cuboid=cuboid_from_dict)

    lab_model_from_dicts.exclusions.append(exclusion_from_dict_cuboid)

    dict_path_poses = []
    i=0
    for pose in dummy_path_2:
        dict_path_poses.append({'x': pose[0], 'y': pose[1], 'z': pose[2]})
        i += 1
        
    path_dict = {'name': 'dict_path_test', 'points':dict_path_poses}
    dict_path = db.PathSQL.from_dict(path_dict)
    
    tray_dict = {"name": "Tray Endpoint From Dict",
                 "pose":{'x': 10, 'y':0.1, 'z':0.3}, 
                 "wait_at":{'x': 10, 'y':0.35, 'z':0.3, 'q1': 0.0, 'q2': 0.0, 'q3': 0.0, 'q4': 1.0}, 
                 "type": {'name': 'tray42', 'value': 1}}
    
    # (endpoint_type=db.EndpointTypes.docking_station, name="dock1",pose=db.PoseSQL(x=3.3,y=2.2,z=1.1), wait_at = db.PoseSQL(x=0.5, y=0.5, z=0.5))
    dict_tray = db.EndpointSQL.from_dict(tray_dict)

    lab_model_from_dicts.paths.append(dict_path)
    lab_model_from_dicts.endpoints.append(dict_tray)
    sess.add(lab_model_from_dicts)
    sess.commit()
    
    #let's try to get the path and its points:
    # path = sess.query(db.PathSQL).first()
    # points = path.pose_array
    # for p in points:
    #     print(p)
        
    #let's get the lab model bounds
    lab = sess.query(db.LabModelSQL).first()
    bounds = lab.bounds
    
    print("Trying out some JSONification.")
    print("Lab: ")
    print(lab.json)
    
print("Done with Database..")