import time
import sys
import random
#sys.path.append("/home/labdrone/Creare_Data/LabDrone/labdrone-internal/gui/python")
sys.path.append("C:\Projects\labdroneInternal\gui\python")

from gui_server import TornadoDaemon, MainHandler
TornadoDaemon.kill()
TornadoDaemon.getInstance().start()
mainHandler = MainHandler.getInstance()
t = 0
while (True):
    t += 1
    time.sleep(0.1)
    mainHandler.set_drone_info()
    TornadoDaemon.getInstance().start()
    if t%100 == 0:
        TornadoDaemon.getInstance().stop()
        time.sleep(1)
    #if t%20 == 0:
        #candidate_cuboid_json = {"length":{"x": random.random()*10, "y": random.random()*10, "z": random.random()*10 },"centroid":{"x": random.random()*20-10, "y": random.random()*20-10, "z": random.random()*20-10 },"theta_z":random.random()*360}
        #mainHandler.set_candidate_exclusion(candidate_cuboid_json)
