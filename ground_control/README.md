This is the Ground Controller application for the Advanced Plate Transport system.

This is a python application. To set up the envirionment needed to run the application, use the `conda` environment specificiation in `labdrone_env.yml`.

To run the unit tests, navigate to the `ground_control/ground_control` directory in a terminal, and enter:
```bash
python -m unittest discover unit_test
```