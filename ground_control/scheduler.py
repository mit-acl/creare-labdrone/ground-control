import Queue
#from enum import Enum
import threading
import logging
import time
import database as db
import rospy

from labdrone_msgs.msg import TransportJob
from labdrone_msgs.srv import TransportJobReq

from gui_log import glog, glog_debug, glog_info, glog_warn, glog_error

from defs import JobSpec

class Scheduler(object):
    def __init__(self, queue):
        #Init stuff
        self.queue = queue
        self.job_req_svc = rospy.Service("TransportRequest", TransportJobReq, self._ext_cmd_handler)
        glog_info(msg="Started scheduler",source="Scheduler")

    def ext_cmd_proxy(self, new_cmd):
        glog_info(msg="external command proxy called",source="Scheduler")
        new_queue_event = self._parse_ext_cmd(new_cmd)
        if new_queue_event is not None:
            #Enqueue one instance of the job
            if new_queue_event.is_periodic:
                #launch asynchronous timer for periodic events
                f_stop = threading.Event()
                self._schedule_periodic_put(new_queue_event, f_stop)
            else:
                self.queue.put(new_queue_event.job_spec)
        else:
            try:
                serialized_cmd = str(new_cmd)
                logging.log(logging.ERROR,"Invalid external command: %s",serialized_cmd)
            except:
                logging.log(logging.ERROR,"Invalid external command.")

    def _ext_cmd_handler(self,request):
        new_queue_event = self._parse_ext_cmd(request.job)
        if new_queue_event is not None:
            if new_queue_event.is_periodic:
                f_stop = threading.Event()
                glog_info(msg="Registered new periodic job: external request", source="Scheduler")
                self._schedule_periodic_put(new_queue_event, f_stop)
            else:
                glog_info(msg="Injected new job into queue: single external request",source="Scheduler")
                self.queue.put(new_queue_event.job_spec)
            return True
        else:
            try:
                serialized_cmd = str(request)
                log_msg = "Invalid external command: {}".format(serialized_cmd)
                logging.log(log_msg)
                glog_error(msg=log_msg,source="Scheduler")
            except:
                log_msg = "Invalid external command."
                logging.log(logging.ERROR,log_msg)
                glog_error(msg=log_msg,source="Scheduler")
            return False

    def _cmd_integrity_good(self, new_cmd):
        #TODO Validate content of new_cmd.
        #E.g look up if pickup/dropoff stations are defined
        return True

    def _parse_ext_cmd(self,new_cmd):
        if self._cmd_integrity_good(new_cmd):
            try:
                pickup =            new_cmd.pickup
                dropoff =           new_cmd.drop_off
                pickup_attempts =   new_cmd.pickup_attempts
                drop_off_attempts = new_cmd.drop_off_attempts
                payload_id =        new_cmd.payload_id

                count =             new_cmd.count
                period_s =          new_cmd.period_s
                return SchedulerEvent(JobSpec(pickup, dropoff, pickup_attempts, drop_off_attempts,payload_id),count,period_s)
            except:
                return None
        else:
            return None

    def _schedule_periodic_put(self,scheduler_event,f_stop):
        if not scheduler_event.count > 0:
            f_stop.set()
        else:
            glog_info(msg="Injected new job into queue: periodic request",source="Scheduler")
            self.queue.put(scheduler_event.job_spec)
            is_done = scheduler_event.decrement_count()
            if is_done:
                f_stop.set()
                glog_info(msg="Completed periodic job",source="Scheduler")
            else:
                if not f_stop.is_set():
                    threading.Timer(scheduler_event.period_s, self._schedule_periodic_put, [scheduler_event,f_stop]).start()


#Wrapper for jobs that adds functionality for periodic events
class SchedulerEvent(object):
    def __init__(self,job_spec,count=1,period_s=20):
        self.job_spec = job_spec
        self.count = count
        self.is_periodic = count > 1
        self.period_s = period_s
        self.time_queued = None

    def decrement_count(self):
        self.count -= 1
        is_done = (self.count <= 0)
        return is_done

if __name__ == '__main__':
    log = logging.getLogger(__name__)
    q = Queue()
    scheduler = Scheduler(q)
    scheduler.ext_cmd_proxy("foo")

    while(True):
        log.debug(q.qsize())
        time.sleep(1)
