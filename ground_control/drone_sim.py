import logging
import threading
import time
import math
import numpy as np
import random #Just for fun
from enum import Enum
from defs import DroneState #TODO: Only used for debug prints. Should probably remove when we share this

#Required imports for communication
import roslib
import rospy
from acl_msgs.msg import HexSMData, HexSMState, HexData, HexError, FloatStamped
from acl_msgs.srv import SetHexSMStateReq, GetBattery, GetHexSMData, HexSMStateNotification, ObstructionNotification
from geometry_msgs.msg import Vector3

#Convenience class for reformatting request info
class StateRequest(object):
    def __init__(self,request):
        self.state = request.state.state
        self.pathArray = []
        for item in request.pathArray:
            self.pathArray.append(np.array([item.x,item.y,item.z]))

#Main simulation class
class DroneSim(object):
    def __init__(self,drone_id):
        #High-level configuration
        self.drone_id = drone_id            #Drone id (e.g. "FlightPro1"). Will be cast to string

        #Low-level vars
        self.stop_flag = False              #High-level flag for periodic execution of state machine
        self.lock = threading.Lock()        #Lock for managing access to sm update method

        #Current status
        self.state = HexSMState.START_UP    #Current state
        self.last_state = HexSMState.START_UP #Last state
        self.position = np.array([0.,0.,0.])#Current position
        self.loaded = 0                     #Current loaded status
        self.battery_percent = 95           #Current battery percentage
        self.path_array = []                #Current path array data
        self.path_index = 0                 #Current path index

        #Simulation settings
        self.update_period_s = 0.1          #Periodic state machine update rate
        self.delta_distance = 0.5           #Distance step per time step when simulating travel
        self.position_error_thresh = 0.01   #Position error threshold when checking if arrived at waypoint
        self.battery_loss_rate = 0.05       #Battery percent loss per time step
        self.battery_charge_rate = 0.2      #Battery percent charge per time step when docked
        self.random_scale = 0.1             #Scale of random walk applied to moves

        #Service registration:
        #Hosted services
        rospy.init_node(drone_id)
        self.state_req_svc = rospy.Service(str(drone_id)+"StateReq",SetHexSMStateReq,self._SetHexSMStateReq_handler)
        self.get_battery_svc = rospy.Service(str(drone_id)+"GetBattery",GetBattery,self._GetBattery_handler)
        self.get_sm_data_svc = rospy.Service(str(drone_id)+"GetSMData",GetHexSMData,self._GetHexSMData_handler)
        #Called services
        self.state_change_notification_proxy = rospy.ServiceProxy(str(drone_id)+"StateNotification", HexSMStateNotification)
        self.obstruction_notification_proxy = rospy.ServiceProxy(str(drone_id)+"ObstructionNotification", ObstructionNotification)

    #Service methods
    def _SetHexSMStateReq_handler(self,request):
        reformatted_request = StateRequest(request)
        response = self._update_sm(reformatted_request)
        return HexError(error=response)

    def _GetBattery_handler(self,request):
        # print("handled getbattery request")
        return FloatStamped(data=self.battery_percent)

    def _GetHexSMData_handler(self,request):
        return self._construct_HexSMData()

    def _construct_HexSMData(self):
        hex_sm_data = HexSMData()
        hex_sm_data.state = HexSMState(state=self.state)
        hex_sm_data.loaded = self.loaded
        hex_sm_data.position = Vector3(x=self.position[0],y=self.position[1],z=self.position[2])
        hex_sm_data.pathArray = [] #TODO figure out appropriate formatting to store path/conversion
        hex_sm_data.pathIndex = self.path_index
        return hex_sm_data

    def _send_state_change_notification(self):
        self.state_change_notification_proxy(self._construct_HexSMData())

    def _send_obstruction_notification(self):
        self.obstruction_notification_proxy(self._construct_HexSMData())

    #High-level control
    def run(self):
        self.stop_flag = False
        f_stop = threading.Event()
        self._schedule_periodic_update(f_stop)


        self.cmd_queue_thread = threading.Thread(target=self._update_sm,args=[self])
        self.cmd_queue_thread.start()

    def stop(self):
        self.stop_flag = True

    #Main update methods
    def _schedule_periodic_update(self, f_stop):

        self._update_sm(None)
        if self.stop_flag:
            f_stop.set()
        else:
            threading.Timer(self.update_period_s, self._schedule_periodic_update, [f_stop]).start()

    def _update_sm(self,request=None):
        log = logging.getLogger(__name__)
        self.lock.acquire()

        #Special handling for emergency landing request
        if request is not None:
            log.debug("Update invoked from request")
            if request.state == HexSMState.EMERGENCY_LANDING:
                #Only time when we violate the convention for state transitions
                self.state = HexSMState.EMERGENCY_LANDING
                self._send_state_change_notification()

        response_error = HexError.NONE
        next_state = self.state

        if self.state == HexSMState.EMERGENCY_LANDING:
            self._emergency_landing()

        elif self.state == HexSMState.START_UP:
            if request is not None:
                response_error = HexError.TRANSITION
            log.debug("Waiting for drone manager...")
            rospy.wait_for_service(str(drone_id)+"StateNotification")
            log.debug("Found drone manager")
            next_state = HexSMState.DOCKED

        elif self.state == HexSMState.DOCKED:
            if request is not None:
                if request.state == HexSMState.TAKE_OFF:
                    response_error = self._evaluate_move_request(request)
                    log.debug("Response error is {}".format(response_error))
                    if response_error == HexError.NONE:
                        next_state = request.state
                else:
                    response_error = HexError.TRANSITION

        elif self.state == HexSMState.TAKE_OFF:
            if request is not None:
                response_error = HexError.TRANSITION
            arrived = self._advance()
            if arrived:
                next_state = HexSMState.WAIT_AT_DOCK

        elif self.state == HexSMState.WAIT_AT_DOCK:
            if request is not None:
                if request.state == HexSMState.GO_TO_PICKUP:
                    response_error = self._evaluate_move_request(request)
                    if response_error == HexError.NONE:
                        next_state = request.state
                elif request.state == HexSMState.DOCK_LANDING:
                    next_state = HexSMState.DOCK_LANDING
                    self._update_path(request)
                else:
                    response_error = HexError.TRANSITIONdummyStateChangeCallback

        elif self.state == HexSMState.GO_TO_PICKUP:
            if request is not None:
                response_error = HexError.TRANSITION
            if not self._check_for_obstruction():
                #No obstruction
                arrived = self._advance()
                if arrived:
                    next_state = HexSMState.WAIT_AT_PICKUP
            else:
                self._send_obstruction_notification()
                next_state = HexSMState.REROUTE_PENDING

        elif self.state == HexSMState.WAIT_AT_PICKUP:
            if request is not None:
                if request.state == HexSMState.PICKUP:
                    next_state = request.state
                    self._update_path(request)
                elif request.state == HexSMState.GO_TO_DROP_OFF:
                    response_error = self._evaluate_move_request(request)
                    if response_error == HexError.NONE:
                        next_state = request.state
                else:
                    response_error = HexError.TRANSITION

        elif self.state == HexSMState.PICKUP:
            self.loaded = True
            next_state = HexSMState.WAIT_AT_PICKUP

        elif self.state == HexSMState.GO_TO_DROP_OFF:
            if request is not None:
                response_error = HexError.TRANSITION
            arrived = self._advance()
            if arrived:
                next_state = HexSMState.WAIT_AT_DROP_OFF

        elif self.state == HexSMState.WAIT_AT_DROP_OFF:
            if request is not None:
                if request.state == HexSMState.DROP_OFF:
                    next_state = request.state
                    self._update_path(request)
                elif request.state == HexSMState.GO_TO_DOCK:
                    response_error = self._evaluate_move_request(request)
                    if response_error == HexError.NONE:
                        next_state = request.state
                else:
                    response_error = HexError.TRANSITION

        elif self.state == HexSMState.DROP_OFF:
            self.loaded = False
            next_state = HexSMState.WAIT_AT_DROP_OFF

        elif self.state == HexSMState.GO_TO_DOCK:
            if request is not None:
                response_error = HexError.TRANSITION
            arrived = self._advance()
            if arrived:
                next_state = HexSMState.WAIT_AT_DOCK

        elif self.state == HexSMState.DOCK_LANDING:
            next_state = HexSMState.DOCKED

        elif self.state == HexSMState.REROUTE_PENDING:
            if request is not None:
                if request.state == HexSMState.GO_TO_PICKUP:
                    next_state = request.state
                    self._update_path(request)
                elif request.state == HexSMState.GO_TO_DROP_OFF:
                    next_state = request.state
                    self._update_path(request)
                elif request.state == HexSMState.GO_TO_DOCK:
                    next_state = request.state
                    self._update_path(request)
                elif request.state == HexSMState.EMERGENCY_LANDING:
                    log.debug("This is an acceptable transition, but we should never enter this logical case")
                else:
                    response_error = HexError.TRANSITION

        else:
            #TODO handle
            pass

        #Update simulated battery level
        if self.state == HexSMState.DOCKED:
            self.battery_percent += self.battery_charge_rate
            self.battery_percent = min(100,self.battery_percent)
        else:
            self.battery_percent -= self.battery_loss_rate
            self.battery_percent = max(0,self.battery_percent)

        #State transition
        self.last_state = self.state
        self.state = next_state

        log.debug("state is {} and position is {} and battery is {} and loaded status is {}".format(DroneState(self.state), self.position, self.battery_percent, self.loaded))

        #Call state change notification service
        if self.last_state != self.state:
            self._send_state_change_notification()

        self.lock.release()
        #Note: response error ignored when update is not in response to a state change request
        return response_error

    #State-specific behavior
    def _emergency_landing(self):
        #It's ok if this blocks everything else
        pass

    #Generic travel methods
    def _evaluate_move_request(self,request):
        path_error = self._check_path_ok(request.pathArray)
        if path_error == HexError.NONE:
            self._update_path(request)
        return path_error


    def _check_path_ok(self,path):
        error = HexError.NONE
        if self._check_for_obstruction():
            error = HexError.OBSTRUCTION
        #largely placeholder. Long term we want to do some sort of check for path validity beyond "Does it exist?"
        elif len(path) == 0:
            error = HexError.ROUTE
        return error

    def _check_for_obstruction(self):
        #placeholder. Want to return some additional information about obstructed status
        return False

    def _dist(self,target):
        return math.sqrt((self.position[0]-target[0])**2 + (self.position[1]-target[1])**2 + (self.position[2]-target[2])**2)

    def _unit_vector(self,target):
        return (target-self.position)/self._dist(target)

    def _arrived(self,target):
        return self._dist(target) < self.position_error_thresh

    def _move_step(self,target):
        dist = self._dist(target)
        if dist < self.delta_distance:
            self.position = target
        else:
            unit_vector = self._unit_vector(target)
            self.position = self.position + unit_vector*self.delta_distance + (np.array([random.random(),random.random(),random.random()])-0.5)*self.random_scale

    def _advance(self):
        if self._arrived(self.path_array[self.path_index]):
            self.path_index += 1
        if self.path_index == len(self.path_array):
            arrived = True
        else:
            self._move_step(self.path_array[self.path_index])
            arrived = False
        return arrived

    def _update_path(self,request):
        self.path_array = request.pathArray
        self.path_index = 0

    #Debug methods
    def debugStateChangeCallback(self,msg):
        response = self._update_sm(msg)
        return response

if __name__ == '__main__':
    log = logging.getLogger(__name__)
    drone_id = "FlightProSim"
    sim = DroneSim(drone_id)
    sim.run()
    while(True):
        time.sleep(1)
    dummy_hb_proxy = rospy.ServiceProxy(str(drone_id)+"GetBattery", GetBattery)
    for i in range(10):
        result = dummy_hb_proxy()
        log.debug("Battery level:",result.batteryPercent.data)
        time.sleep(1)
    sim.stop()




    # time.sleep(0.1)
    # response = sim.debugStateChangeCallback(StateRequest(HexSMState.TAKE_OFF, [np.array([0,0,10])]))
    # log.debug("response to take off is ", response)
    # time.sleep(1)
    # response = sim.debugStateChangeCallback(StateRequest(HexSMState.GO_TO_DROP_OFF, [np.array([30,10,40])]))
    # log.debug("response to improper state transition request is ", response)
    # time.sleep(1)
    # sim.stop()
