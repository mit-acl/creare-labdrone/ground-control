import threading
import logging
import rospy
import numpy as np

from gui.src.gui_server import MainHandler

#TODO circular import issue
#from fleet_manager import choose_drone
from defs import Job
from geometry_msgs.msg import Pose
from snapstack_msgs.msg import State

import path_helpers
import environment
import database as db
from database import EndpointTypes

#Intended to be transitioned to rospy.get_param
from ros_param_proxy import get_param

from gui_log import glog, glog_debug, glog_info, glog_warn, glog_error

class AirTrafficController(object):
    __instance = None
    @staticmethod
    def getInstance():
        """ Static access method. """
        if AirTrafficController.__instance == None:
            AirTrafficController()
        return AirTrafficController.__instance
    def __init__(self):
        """ Virtually private constructor. """
        if AirTrafficController.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            AirTrafficController.__instance = self
            self.lock = threading.Lock()
            self.active_jobs = []


#            self.loc_comparison_error_thresh = get_param('/gc/atc/loc_comparison_error_thresh') This doesn't seem to be used anymore
            env_name = get_param('/gc/atc/env_name')
            self.min_path_dist = get_param('/gc/atc/min_path_dist')


            #DEBUG phasing out dummy logic
            self.use_env = True
            self.use_vicon = True


            lab_model_json = None
            with db.Database() as dbase:
                sess= dbase.session
                lab_model = sess.query(db.LabModelSQL).filter_by(name = env_name).order_by(db.LabModelSQL.id.desc()).first() #Most recently defined lab model with this name
                lab_model_json = lab_model.json
            if lab_model_json is not None:
                self.env = environment.Environment.from_json(lab_model_json)
                glog_info(msg="Loaded environment {}".format(env_name),source="ATC")
            else:
                glog_error(msg="Failed to load environment",source="ATC")
                raise Exception("failed to load environment")
            self.gui_handler = MainHandler.getInstance()

            glog_info(msg="Instantiated ATC",source="ATC")


    def get_drone_loc(self, drone_id):

        if self.use_vicon:
            topic_name = r"/" + drone_id + r"/state"
            topic_type = State

            timeout = get_param('/gc/atc/vicon_timeout')

            state = None
            try:
                state = rospy.wait_for_message(topic_name,topic_type,timeout)
                #print("state!:",drone_id,state)
            except:
                pass

            if state is None:
                return None
            else:
                return state.pos

        #DEBUG logic
        else:
            glog_info(msg="Using debug logic instead of Vicon to get drone location",source="ATC")
            if drone_id == "FlightPro1s":
                return np.array([-0.622,-0.652,0.180])
            elif drone_id == "FlightPro2s":
                return np.array([-1.042,1.242,0.180])
            else:
                raise Exception

    def loc_to_dock(self, location):

        if self.use_env:

            distance_tolerance = get_param('/gc/atc/loc_to_dock_distance_tolerance')

            for key in self.env.endpoints:
                endpoint = self.env.endpoints[key]
                type = endpoint.type
                pose = endpoint.pose
                np_location = np.array([location.x, location.y, location.z])
                # print(np_location)
                #TODO better linkage to "Dock" == 0
                if type == EndpointTypes.docking_station.value:
                    if endpoint.distance_to(np_location) < distance_tolerance:
                        return str(endpoint.name)
                    else:
                        print(endpoint.distance_to(np_location))
            return None

        #TODO DEBUG Phase out
        else:
            glog_info(msg="Using debug logic instead of Environment in loc_to_dock",source="ATC")

            #iterate over stored dock locations, return closest if error is within threshold.
            #Else return an identifier that indicates failure. e.g. "NoDock"
            if self.use_vicon:

                glog_info(msg="Using debug logic instead of Vicon in loc_to_dock",source="ATC")


                #This case is super niche and doesn't make sense long term
                if abs(location.x) < 0.1:
                    return "DockA"
                else:
                    return "DockB" 
            else:
                hacky_var = location - self.get_drone_loc("FlightPro1s")
                if hacky_var[0] == 0:
                    return "DockA"
                else:
                    return "DockB"

    # def dock_to_loc(self, dock):
    #     #TODO link to environment data
    #     glog_warn(msg="Using debug logic for dock_to_loc",source="ATC")
    #     if dock == "DockA":
    #         return np.array([0,0,0])
    #     elif dock == "DockB":
    #         return np.array([2,0,0])
    #     else:
    #         raise Exception("Dock undefined")

    def get_active_paths(self):
        #Format active jobs list into list of all contained paths
        active_path_list = []
        for job in self.active_jobs:
            active_path_list.append(job.go_to_pickup_path)
            active_path_list.append(job.go_to_drop_off_path)
            active_path_list.append(job.go_to_dock_path)
        return active_path_list

    def update_gui_paths(self):
        #Format active job info - pull out active path indices and communicate to gui_server
        active_path_indices = []
        for job in self.active_jobs:
            active_path_indices.append(job.go_to_pickup_path_db_index)
            active_path_indices.append(job.go_to_drop_off_path_db_index)
            active_path_indices.append(job.go_to_dock_path_db_index)
        self.gui_handler.set_active_path_indices(active_path_indices)


def request_job(job_spec,candidate_drone_ids):
    if len(candidate_drone_ids) <= 0 :
        glog_warn(msg="Request_job invoked with no candidate drone ids. This should not happen.",source="ATC")
        return None, 0
    atc = AirTrafficController.getInstance()
    with atc.lock:
        candidate_jobs = []

        for drone_id in candidate_drone_ids:
            loc = atc.get_drone_loc(drone_id)
            dock = atc.loc_to_dock(loc)
            if dock is not None:

                is_viable_job = True

                #Dock
                dock_endpoint = atc.env.get_endpoint_by_name(dock)
                dock_pose = dock_endpoint.pose
                dock_wait_at_pose = dock_endpoint.wait_at

                #Pickup
                pickup_endpoint = atc.env.get_endpoint_by_name(job_spec.pickup)
                pickup_pose = pickup_endpoint.pose
                pickup_wait_at_pose = pickup_endpoint.wait_at

                #Drop off
                drop_off_endpoint = atc.env.get_endpoint_by_name(job_spec.drop_off)
                drop_off_pose = drop_off_endpoint.pose
                drop_off_wait_at_pose = drop_off_endpoint.wait_at

                go_to_pickup_path_candidates, go_to_pickup_path_db_indices     = atc.env.get_paths_by_endpoint_names([dock,job_spec.pickup])
                go_to_drop_off_path_candidates, go_to_drop_off_path_db_indices = atc.env.get_paths_by_endpoint_names([job_spec.pickup,job_spec.drop_off])
                #For now, assume always return to same dock
                go_to_dock_path_candidates, go_to_dock_path_db_indices         = atc.env.get_paths_by_endpoint_names([job_spec.drop_off,dock])


                #Not very DRY
                active_path_list = atc.get_active_paths()
                #Validate paths
                if len(go_to_pickup_path_candidates) > 0:
                    if len(active_path_list) > 0:
                        dists = []
                        for path_candidate in go_to_pickup_path_candidates:
                            dist = path_helpers.min_distance_between_path_and_path_list(path_candidate,active_path_list)
                            dists.append(dist)
                        if max(dists) > atc.min_path_dist:
                            max_separation_index = dists.index(max(dists))
                            go_to_pickup_path = go_to_pickup_path_candidates[max_separation_index]
                            go_to_pickup_path_db_index = go_to_pickup_path_db_indices[max_separation_index]
                        else:
                            log_msg = "Job failed: go to pickup path too close to registered paths for job with payload_id = {} and drone_id = {}".format(job_spec.payload_id, drone_id)
                            glog_debug(msg=log_msg,source="ATC")
                            is_viable_job = False
                    else:
                        #Pick shortest option
                        path_lengths = []
                        for path_candidate in go_to_pickup_path_candidates:
                            path_lengths.append(path_helpers.path_length(path_candidate))
                        selected_index = path_lengths.index(min(path_lengths))

                        go_to_pickup_path = go_to_pickup_path_candidates[selected_index]
                        go_to_pickup_path_db_index = go_to_pickup_path_db_indices[selected_index]
                else:
                    log_msg = "Job failed: no go to pickup path candidates for job with payload_id = {} and drone_id = {}".format(job_spec.payload_id, drone_id)
                    glog_debug(msg=log_msg,source="ATC")
                    is_viable_job = False

                if len(go_to_drop_off_path_candidates) > 0:
                    if len(active_path_list) > 0:
                        dists = []
                        for path_candidate in go_to_drop_off_path_candidates:
                            dist = path_helpers.min_distance_between_path_and_path_list(path_candidate,active_path_list)
                            dists.append(dist)
                        if max(dists) > atc.min_path_dist:
                            max_separation_index = dists.index(max(dists))
                            go_to_drop_off_path = go_to_drop_off_path_candidates[max_separation_index]
                            go_to_drop_off_path_db_index = go_to_drop_off_path_db_indices[max_separation_index]
                        else:
                            log_msg = "Job failed: go to drop off path too close to registered paths for job with payload_id = {} and drone_id = {}".format(job_spec.payload_id, drone_id)
                            glog_debug(msg=log_msg,source="ATC")
                            is_viable_job = False
                    else:
                        #Pick shortest option
                        path_lengths = []
                        for path_candidate in go_to_drop_off_path_candidates:
                            path_lengths.append(path_helpers.path_length(path_candidate))
                        selected_index = path_lengths.index(min(path_lengths))

                        go_to_drop_off_path = go_to_drop_off_path_candidates[selected_index]
                        go_to_drop_off_path_db_index = go_to_drop_off_path_db_indices[selected_index]
                else:
                    log_msg = "Job failed: no go to drop off path candidates for job with payload_id = {} and drone_id = {}".format(job_spec.payload_id, drone_id)
                    glog_debug(msg=log_msg,source="ATC")
                    is_viable_job = False

                if len(go_to_dock_path_candidates) > 0:
                    if len(active_path_list) > 0:
                        dists = []
                        for path_candidate in go_to_dock_path_candidates:
                            dist = path_helpers.min_distance_between_path_and_path_list(path_candidate,active_path_list)
                            dists.append(dist)
                        if max(dists) > atc.min_path_dist:
                            max_separation_index = dists.index(max(dists))
                            go_to_dock_path = go_to_dock_path_candidates[max_separation_index]
                            go_to_dock_path_db_index = go_to_dock_path_db_indices[max_separation_index]
                        else:
                            log_msg = "Job failed: go to dock path too close to registered paths for job with payload_id = {} and drone_id = {}".format(job_spec.payload_id, drone_id)
                            glog_debug(msg=log_msg,source="ATC")
                            is_viable_job = False
                    else:
                        #Pick shortest option
                        path_lengths = []
                        for path_candidate in go_to_dock_path_candidates:
                            path_lengths.append(path_helpers.path_length(path_candidate))
                        selected_index = path_lengths.index(min(path_lengths))

                        go_to_dock_path = go_to_dock_path_candidates[selected_index]
                        go_to_dock_path_db_index = go_to_dock_path_db_indices[selected_index]
                else:
                    log_msg = "Job failed: no go to dock path candidates for job with payload_id = {} and drone_id = {}".format(job_spec.payload_id, drone_id)
                    glog_debug(msg=log_msg,source="ATC")
                    is_viable_job = False


                if is_viable_job:

                    job = Job(job_spec,\
                              drone_id,\
                              take_off_path                = [],\
                              take_off_pose                = dock_wait_at_pose,\
                              go_to_pickup_path            = go_to_pickup_path,\
                              go_to_pickup_path_db_index   = go_to_pickup_path_db_index,\
                              go_to_pickup_pose            = pickup_wait_at_pose,\
                              pick_up_path                 = [],\
                              pick_up_pose                 = pickup_pose,\
                              go_to_drop_off_path          = go_to_drop_off_path,\
                              go_to_drop_off_path_db_index = go_to_drop_off_path_db_index,\
                              go_to_drop_off_pose          = drop_off_wait_at_pose,\
                              drop_off_path                = [],\
                              drop_off_pose                = drop_off_pose,\
                              go_to_dock_path              = go_to_dock_path,\
                              go_to_dock_path_db_index     = go_to_dock_path_db_index,\
                              go_to_dock_pose              = dock_wait_at_pose,\
                              dock_landing_path            = [],\
                              dock_landing_pose            = dock_pose)
                    candidate_jobs.append(job)

        if len(candidate_jobs) >= 1:
            #Pick job with min path length
            path_lengths = []
            for job in candidate_jobs:
                gtp_length = path_helpers.path_length(job.go_to_pickup_path)
                gtdo_length = path_helpers.path_length(job.go_to_drop_off_path)
                gtd_length = path_helpers.path_length(job.go_to_drop_off_path)
                path_lengths.append(gtp_length + gtdo_length + gtd_length)
            selected_index = path_lengths.index(min(path_lengths))

            log_msg = "Assigned job with payload {} to drone {}. Path length is {}".format(job_spec.payload_id,\
                                                                                           candidate_drone_ids[selected_index],\
                                                                                           path_lengths[selected_index])
            glog_info(msg=log_msg,source="ATC")

            job = candidate_jobs[selected_index]
        else:
            log_msg = "Failed to assign job with payload {} to drone ids {}. No path exists.".format(job_spec.payload_id,\
                                                                                                     candidate_drone_ids)
            glog_warn(msg=log_msg,source="ATC")

            job = None

        #We should be able to signal that there is no viable set of paths at the moment, e.g. by returning None. Fleet manager needs to be able to deal with that.
        if job is not None:
            atc.active_jobs.append(job)
            atc.update_gui_paths()
        return job, len(atc.active_jobs)


def job_done(job):
    """
    Used for a drone_manager to communicate that it is done executing a job. Might consider having fleet manager do this, but for now drone manager seems more appropriate
    """
    glog_info(msg="Job completed with payload_id: {}".format(job.payload_id),source="ATC")
    atc = AirTrafficController.getInstance()
    with atc.lock:
        try:
            atc.active_jobs.remove(job)
            atc.update_gui_paths()
        except ValueError:
            try:
                glog_error(msg="Failed to deregister job with payload_id: {}. This shouldn't happen.".format(job.job_spec.payload_id),source="ATC")
            except:
                glog_error(msg="Failed to deregister job AND failed to parse job. This really shouldn't happen",source="ATC")
            log.error("Element not found in list")
            #Think about badness of this

#Obstruction info should include what phase of the job the drone is currently on. There is no need to reroute the entire path
def reroute_request(job,obstruction_info):
    log = logging.getLogger(__name__)
    atc = AirTrafficController.getInstance()
    with atc.lock:
        glog_info(msg="Reroute requested for drone id: {}".format(job.drone_id),source="ATC")
        #Would call job_done, except that then there is a small chance of a potentially bad outcome if you deregister an active drone, then a new job request grabs the lock, then the reroute request is processed,
        try:
            atc.active_jobs.remove(job)
        except ValueError:
            log.error("Element not found in list")
            #We should really never get here

        #Do reroute stuff. I'm not even sure what format obstruction_info is going to come in
        atc.active_jobs.append(job)
        atc.update_gui_paths()
        #For now just return the same job
        glog_info(msg="Reroute completed for drone id: {}".format(job.drone_id),source="ATC")
    return job

def get_num_active_jobs():
    atc = AirTrafficController.getInstance()
    with atc.lock:
        return len(atc.active_jobs)



if __name__ == '__main__':
    atc = AirTrafficController.getInstance()

#    print(atc.env.paths_np)
#    print(atc.env.path_indices)

    path_segment_endpoint_names = [["Nest2","Nest5"],["Nest5","Nest3"],["Nest6","Nest7"],["Nest7","Nest8"],["Nest8","Nest4"]]
    for endpoint_names in path_segment_endpoint_names:
    	output_path = atc.env.get_paths_by_endpoint_names(endpoint_names)
#        print("endpoints: {} -> paths: {}".format(endpoint_names,output_path))
