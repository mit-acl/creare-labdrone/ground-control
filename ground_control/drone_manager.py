import logging
import threading
import Queue
import datetime
import numpy as np
import time
from copy import copy

import rospy

#Intended to be transitioned to rospy.get_param
from ros_param_proxy import get_param

from labdrone_msgs.msg import HexSMData, HexSMState, HexError
from labdrone_msgs.srv import GetHexSMData, ObstructionNotification, HexSMStateNotification, GetBattery, SetHexSMStateReq, HexSMStateNotificationResponse, ObstructionNotificationResponse
from geometry_msgs.msg import Vector3, Pose

from defs import Job
# from defs import DroneState #May still want to use when communicating status to higher levels of application
from defs import BatteryStatus
from defs import DroneStatus
from defs import DroneManagerCmdType     as CmdType
from defs import DroneMsgType            as MsgType
from defs import CmdQueueElement         as CmdQueueElement
from defs import DroneMgrMsgCmd          as MsgCmd
from defs import DroneMgrJobCmd          as JobCmd
from defs import JobStatus
from defs import StateChangeRequest
from defs import FleetManagerDroneStatus #Communicated back to fleet manager for job assignment decisions

import air_traffic_controller as atc
import fleet_manager

from gui_log import glog, glog_debug, glog_info, glog_warn, glog_error
from job_log import job_assigned, job_completed, pickup_completed, drop_off_completed, job_incomplete

log = logging.getLogger(__name__)

class DroneManager(object):
    #""""
    #Manager for single drone.
    #""""
    def __init__(self, drone_id):
        log = logging.getLogger(__name__)
        glog_info(msg="Initializing drone {}".format(drone_id),source="DM")
        self.id = drone_id

        self.stop_flag = False #Execution control of high-level drone manager process.

        #Drone Heartbeat settings
        self.hb_stop_flag = False
        self.hb_rate_s = get_param('/gc/drone_manager/hb_rate_s')
        self.hb_active = False

        #Monitor communication status
        self.last_comm = None

        #external control interface for DroneManager. Incoming messages will be parsed and an associated element will be enqueued in the command queue.
        #The main control method will have a loop that performs blocking reads of queue elements and executes commands as they become available.
        self.cmd_queue = Queue.Queue()
        #thread that handles blocking dequeues & processing of cmd_queue elements
        self.cmd_queue_thread = None

        self.battery_status = BatteryStatus()
        self.battery_abort_threshold =                get_param('/gc/drone_manager/battery_abort_threshold')
        self.battery_pickup_threshold =               get_param('/gc/drone_manager/battery_pickup_threshold')
        self.battery_drop_off_threshold =             get_param('/gc/drone_manager/battery_drop_off_threshold')
        self.battery_job_ready_threshold =            get_param('/gc/drone_manager/battery_job_ready_threshold')
        self.battery_post_charge_drop_off_threshold = get_param('/gc/drone_manager/battery_post_charge_drop_off_threshold')
        self.battery_post_charge_pickup_threshold =   get_param('/gc/drone_manager/battery_post_charge_pickup_threshold')



        #Switch between attempting to return to charging dock or emergency landing on transaction failure
        self.attempt_recharge = get_param('/gc/drone_manager/attempt_recharge')
        self.recharging = True #Flag for state machine to indicate that the drone is en_route to dock to charge, as result of 'attempt_recharge' logic mid-job

        # Max time before which drone manager raises timeout to fleet manager.
        self.max_comm_timeout_s = get_param('/gc/drone_manager/max_comm_timeout_s')
        self.timeout_latched = False #Latch this high so as to not create a firehose of messages from one timeout event

        #Omit first point on state transition request messages to drone
        self.omit_first_path_point = get_param('/gc/drone_manager/omit_first_path_point')

        #Obstruction info placeholder for now. Might want to move into drone status
        self.obstructed = False
        self.obstructed_info = None #This is HexSMData
        self.obstructed_last_drone_state = None

        #Obstruction handling params
        #Possible future development to extend reroute logic would add options here

        #Enable rerouting if obstacle encountered.
        self.on_obstacle_reroute = get_param('/gc/drone_manager/on_obstacle_reroute')


        #Enable reversing path when an obstacle is encountered
        self.on_obstacle_reverse_path = get_param('/gc/drone_manager/on_obstacle_reverse_path')

        #Enable emergency landing if obstacle is encountered when en route to dock
        self.on_obstacle_land_enroute_to_dock = get_param('/gc/drone_manager/on_obstacle_land_enroute_to_dock')

        #Status flag to indicate that 1) A flying drone is returning to dock, regardless of job state and 2) It is not eligable to receive new jobs, regardless of other state
        self.grounded_status = False


        node_name =                         str(drone_id)+"Manager"
        #Drone-specific service names
        obstruction_notification_service =  str(drone_id)+"/ObstructionNotification"
        state_notification_service =        str(drone_id)+"/StateNotification"
        state_request_service =             str(drone_id)+"/StateReq"
        get_battery_service =               str(drone_id)+"/GetBattery"
        get_state_machine_data_service =    str(drone_id)+"/GetSMData"

#        rospy.init_node(node_name)
        #Advertise these services for the drone to call:
        self.obstruction_notification_svc = rospy.Service(obstruction_notification_service,ObstructionNotification,self._ObstructionNotification_handler)
        self.state_notification_svc = rospy.Service(state_notification_service,HexSMStateNotification,self._StateNotification_handler)
        #Proxies to call drone services: (though we don't need to use proxies because we created a node)
        self.state_req_proxy = rospy.ServiceProxy(state_request_service, SetHexSMStateReq)
        self.get_battery_proxy = rospy.ServiceProxy(get_battery_service, GetBattery)
        self.get_sm_data_proxy = rospy.ServiceProxy(get_state_machine_data_service, GetHexSMData)

        #Instantiate status object: Internal model of drone status
        self.drone_status = DroneStatus()
        self.last_drone_status = DroneStatus()

        #Currently used only in reroute logic. Stores previous distinct drone state, only updated on state transitions
        self.last_drone_state = None

        self.external_drone_status = FleetManagerDroneStatus()

        log.info("Waiting for service in drone manager init")
        glog_info(msg="Waiting for service: {}".format(self.id),source="DM")
        rospy.wait_for_service(get_state_machine_data_service)
        rospy.wait_for_service(get_battery_service)
        rospy.wait_for_service(state_request_service)
        log.info("Done waiting!")
        glog_info(msg="Done waiting: {}".format(self.id),source="DM")

        self.job = None #current job definition
        self.job_status = JobStatus.NOT_READY

        self.pickup_attempts = 0 # Counter for attempted pickups of current job
        self.drop_off_attempts = 0 # Counter for attempted drop-offs of current job

        self._send_get_data_msg()

#These getter methods are intended as the drone manager -> fleet manager communication interface, so as to expose all necessary information for fleet manager decisions
    def get_drone_status(self):
        #Return DroneManager's conception of drone status. Intended to be called by FleetManager
        return self.drone_status

    def get_job_status(self):
        return self.job_status

    def get_job(self):
        return self.job

#These public methods are intended as the control interface for the fleet manager -> drone manager
    def start(self):
        self._start_cmd_queue()
        self._schedule_hb()

#    def pause(self):
#        new_queue_element = CmdQueueElement(CmdType.PAUSE)
#        self.cmd_queue.put(new_queue_element)

#    def resume(self):
#        new_queue_element = CmdQueueElement(CmdType.RESUME)
#        self.cmd_queue.put(new_queue_element)

    def assign_job(self,job):
        #Might be an unnecessary check, though would be valuable if the job status could go from ready->not ready without any control input.
        #not sure specifically what that use case is - failing battery? seems unlikely. At any rate, this shouldn't hurt anything if it doesn't add value.
        if self.job_status == JobStatus.READY:
            #Assignment pending is used to ensure that there is no race condition that could assign multiple jobs to a drone manager before it handles executing logic for the first request.
            #The status will prevent the fleet manager from considering this drone for any immediately available jobs.
            self.job_status = JobStatus.ASSIGNMENT_PENDING
            self._update_external_drone_status()
            #Conditionally enqueue job message on the above check? Return successful job assignment status?
        new_queue_element = JobCmd(job)
        self.cmd_queue.put(new_queue_element)

    def kill(self):
        log_msg = "Received kill command in DM: {}".format(self.id)
        glog_warn(msg=log_msg,source="DM")
        new_queue_element = CmdQueueElement(CmdType.KILL)
        self.cmd_queue.put(new_queue_element)

    def ground(self):
        log_msg = "Received ground command in DM: {}".format(self.id)
        glog_warn(msg=log_msg,source="DM")
        new_queue_element = CmdQueueElement(CmdType.ABORT)
        self.cmd_queue.put(new_queue_element)

    def abort(self):
        new_queue_element = CmdQueueElement(CmdType.ABORT)
        self.cmd_queue.put(new_queue_element)

    def _update_external_drone_status(self):
        """
        Call Update fleet manager with current drone status
        This should be called whenever there is an update to one of the status
        components, though we might not want to send every battery update
        """
        #format message
        job_status = self.job_status
        if self.drone_status.loaded:
            if self.job is not None:
                payload_id = self.job.payload_id
            else:
                payload_id = "NA"
        else:
            payload_id = "NA"
        battery_percentage = self.battery_status.percent
        last_contact = self.last_comm
        grounded = self.grounded_status

        self.external_drone_status = FleetManagerDroneStatus(job_status,payload_id,battery_percentage,last_contact,grounded)

        #invoke callback
        fleet_manager.update_drone_status(self.id,self.external_drone_status)

    def _StateNotification_handler(self,request):
        #CB method for new status info from drone, e.g. asynchronous state transition
        msg_data = request
        self.last_comm = datetime.datetime.now()
        new_queue_element = MsgCmd(MsgType.STATE_NOTIF,msg_data,None,True)
        self.cmd_queue.put(new_queue_element)
        return HexSMStateNotificationResponse()

    def _ObstructionNotification_handler(self,request):
        #CB method for obstruction info from drone. Use to make routing decisions.
        msg_data = request
        self.last_comm = datetime.datetime.now()
        new_queue_element = MsgCmd(MsgType.OBSTRUCTION,msg_data,None,True)
        self.cmd_queue.put(new_queue_element)
        return ObstructionNotificationResponse()

    def _send_hb_msg(self):
        #Call hb service on drone
        response = None
        try:
            response = self.get_battery_proxy()
            self.last_comm = datetime.datetime.now()
            self.battery_status.percent = response.batteryPercent.data
            self.battery_status.timestamp = datetime.datetime.now() #response.battery_percent.header.stamp
            #TODO this needs to be parsed from two ints into a sensical datetime.
            #Could be a better way to do this. The logic is as follows: When the drone is docked, we need some method of setting jobStatus = READY when the battery is full enough
            #We could schedule a separate command to indirectly call _eval_job, but that seems overkill. This piggybacks on the (otherwise unused) notification of a successful
            #hb message to call _eval_job within the main queue handler.
            if self.drone_status.state == HexSMState.DOCKED:
                new_queue_element = MsgCmd(MsgType.HB,None,None,True)
                self.cmd_queue.put(new_queue_element)

            #We could also just use our datetime stamp and ignore transit time. It's probably good enough for this.
        except:
            new_queue_element = MsgCmd(MsgType.HB,None,None,False)
            self.cmd_queue.put(new_queue_element)

        self._update_external_drone_status()

        return response

    def _send_go_to_state_msg(self,req_state,req_path,req_pose):
        log = logging.getLogger(__name__)
        log_msg = "call go to state service on drone {}".format(self.id)
        log.debug(log_msg)
        glog_debug(msg=log_msg,source="DM")

        req_state_formatted = HexSMState(state=req_state)
        req_path_formatted = []

        #Would be better to have more elegant handling here. TODO revisit if time
        req_pose_formatted = Pose()
        if req_pose is not None and not isinstance(req_pose,Pose):
            req_pose_formatted.position.x = req_pose.location[0]
            req_pose_formatted.position.y = req_pose.location[1]
            req_pose_formatted.position.z = req_pose.location[2]
            req_pose_formatted.orientation.x = req_pose.orientation[0]
            req_pose_formatted.orientation.y = req_pose.orientation[1]
            req_pose_formatted.orientation.z = req_pose.orientation[2]
            req_pose_formatted.orientation.w = req_pose.orientation[3]

        request = StateChangeRequest(req_state,req_path,req_pose)
        for item in req_path:
            req_path_formatted.append(Vector3(x=item[0],y=item[1],z=item[2]))

        if self.omit_first_path_point:
            req_path_formatted = req_path_formatted[1:]

        try:
            log_msg = "attempting to call state req proxy,drone id = {}".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")

            response = self.state_req_proxy(req_state_formatted,req_path_formatted,req_pose_formatted)

            log.debug("successfully called state req proxy. Return error is {}".format(response))
            glog_debug(msg="Successfully called state req proxy, drone id = {}".format(self.id),source="DM")

            self.last_comm = datetime.datetime.now()
            succeeded = True
            new_queue_element = MsgCmd(MsgType.SET_SM_STATE,request,response,succeeded)
        except:
            log_msg = "state transition request failed for drone id {}".format(self.id)
            glog_warn(msg=log_msg,source="DM")
            new_queue_element = MsgCmd(MsgType.SET_SM_STATE,request,None,False)
        self.cmd_queue.put(new_queue_element)
        #TODO Ensure the differentiation between communication failure and a rejected request are handled appropriately when processing this queue item.

    def _send_get_data_msg(self):
        log = logging.getLogger(__name__)
        log_msg = "call get sm data message, drone_id = {}".format(self.id)
        log.debug(log_msg)
        glog_debug(msg=log_msg,source="DM")

        try:
            response = self.get_sm_data_proxy()
            self.last_comm = datetime.datetime.now()
            succeeded = True
            new_queue_element = MsgCmd(MsgType.GET_SM_DATA,None,response,succeeded)
        except:
            log_msg = "get sm data failed, drone_id = {}".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")

            succeeded = False
            new_queue_element = MsgCmd(MsgType.GET_SM_DATA,None,None,succeeded)
        self.cmd_queue.put(new_queue_element)
        return succeeded

    def _handle_state_change(self):
        self._eval_job()
        #Use last status and current status
        #Switch on new state, drone loaded status, etc..
        #Possibly query path logic to obtain new path
        #Possibly indicate job complete externally
        #This will be a decent sized chunk of logic

    def _update_drone_state(self,hex_SM_data):
        #It's (unlikely but) possible that a manual request for state data could happen, and that this callback does not imply change of state. Ensure knowledge of actual last state.
        if hex_SM_data.hexData.state.state == self.drone_status.state:
            #New state is same as 'last state'
            pass
        else:
            #Actual state transition
            self.last_drone_state = self.drone_status.state

        #Store last state before updating new state
        self.last_drone_status = copy(self.drone_status)
        #Populate state info from drone msg:
        self.drone_status.state = hex_SM_data.hexData.state.state
        self.drone_status.path_waypts = hex_SM_data.hexData.pathArray #TODO Conversion?
        self.drone_status.path_index = hex_SM_data.hexData.pathIndex
        self.drone_status.loaded = hex_SM_data.hexData.loaded
        self.drone_status.position = hex_SM_data.hexData.position #TODO Conversion
        self.drone_status.last_update_timestamp = datetime.datetime.now()
        self._update_external_drone_status() #could conditionally do this, e.g. on change of loaded status

    def _update_battery_status(self,hb_response):
        self.battery_status.percent = hb_response.batteryPercent.data
        self.battery_status.timestamp = datetime.datetime.now() #TODO pull this from header of msg.
        if (self.battery_status.percent < self.battery_abort_threshold) and not (self.drone_status.state == HexSMState.DOCKED):
            self.abort()

    def _schedule_hb(self):
        f_stop = threading.Event()
        self._send_msg_hb(f_stop)
        self.hb_active = True
        self.hb_stop_flag = False

    def _send_msg_hb(self,f_stop):
        log = logging.getLogger(__name__)

        response = self._send_hb_msg()
        if response is not None:
            self._update_battery_status(response)
        else:
            log_msg = "HB failed for drone {}".format(self.id)
            log.warn(log_msg)
            glog_warn(msg=log_msg,source="DM")

        #Schedule next hb message
        if self.hb_stop_flag:
            log.debug("killing hb")
            glog_debug(msg="killing HB requests for drone {}".format(self.id),source="DM")
            f_stop.set()
            self.hb_stop_flag = False
            self.hb_active = False
        if not f_stop.is_set():
            threading.Timer(self.hb_rate_s,self._send_msg_hb, [f_stop]).start()

    def _start_cmd_queue(self):
        self.stop_flag = False
        self.cmd_queue_thread = threading.Thread(target=self._read_cmd_queue,args=[])
        self.cmd_queue_thread.start()

    def _read_cmd_queue(self):
        log = logging.getLogger(__name__)
        while not self.stop_flag:
            new_cmd = self.cmd_queue.get() #blocking read
            log_msg = "executing new command of type: {}, drone id = {}".format(new_cmd.cmd_type,self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")
            self._execute_cmd(new_cmd)

    def _execute_cmd(self,cmd):
        log = logging.getLogger(__name__)

        #Pause request from fleet manager - e.g. if unexpected obstruction enters the space.
        if cmd.cmd_type == CmdType.PAUSE:
            #Set some vars
            self._eval_job()
            log.debug("TODO think through the use case")

        #Start request from fleet manager.
        elif cmd.cmd_type == CmdType.RESUME:
            self._eval_job()
            log.debug("TODO")
            #Use to resume after a pause

        #Shut down request from fleet manager
        elif cmd.cmd_type == CmdType.ABORT:
            #TODO resolve unfinished job stuff here
            self._incomplete_job_action() #Deal with any incomplete job information
            #For now, issue command to execute emergency landing:
            self._send_go_to_state_msg(HexSMState.EMERGENCY_LANDING, [], None)
            #High-level shut-down request from fleet manager
            #Might want to think about responsibility drone manager has to get drone safely landed
            #Could check docked status and initiate landing procedure if not docked.

        elif cmd.cmd_type == CmdType.KILL:
            #TODO resolve unfinished job stuff here
            self._incomplete_job_action() #Deal with any incomplete job information
            #For now, issue command to execute emergency landing:
            self._send_go_to_state_msg(HexSMState.EMERGENCY_LANDING, [], None)

        #Handle job request. The fleet manager should only issue a job request if it thinks the drone manager is available
        elif cmd.cmd_type == CmdType.JOB:
            if self.job_status == JobStatus.ASSIGNMENT_PENDING:
                self.job = cmd.job
                self.job_status = JobStatus.PICKUP_PENDING
                self.pickup_attempts = 0
                self.drop_off_attempts = 0
                self._eval_job()
                self._update_external_drone_status()

                #Communicate to JobLog
                job_assigned(self.job)
            else:
                log.error("job requested, but job status is not ready. Something went wrong to be here")
                #TODO handle. Msg failure back to fleet manager. Carefully think about how jobstatus is updated

        #All handling dealing with receipt of an asynchronous message OR a message response OR a failed message attempt
        elif cmd.cmd_type == CmdType.DRONE_MSG:
            log_msg = "processing drone msg: {} for drone {}".format(cmd.msg_type,self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")
            #Response to drone manager request for state info
            if cmd.msg_type == MsgType.GET_SM_DATA:

                if cmd.succeeded:

                    self.timeout_latched = False

                    self._update_drone_state(cmd.res_data)
                    if self.last_drone_status.state != self.drone_status.state:
                        #State change. This is an unusual path. The drone should notify of any state changes. Might end up here on start up.
                        log_msg = "Unusual state change path for drone {}".format(self.id)
                        log.warn(log_msg)
                        glog_warn(msg=log_msg,source="DM")

                        self._handle_state_change()
                else:
                    #Retry? log failure? run some logic that checks for timeout
                    glog_warn(msg="Get SM data failed for drone {]".format(self.id),source="DM")
                    self._eval_timeout()

            #Response to drone manager request to change state
            elif cmd.msg_type == MsgType.SET_SM_STATE:
                if cmd.succeeded:

                    self.timeout_latched = False

                    log.debug(cmd.res_data)
                    if cmd.res_data.error.error == HexError.NONE:
                        pass
                    elif cmd.res_data.error.error == HexError.TRANSITION:
                        #No point repeatedly trying to request a transition that is being actively rejected
                        glog_warn(msg="State change request rejected for drone {} to state {}".format(self.id,cmd.req_data.state),source="DM")
                    else:
                        #We shouldn't (at present) get here
                        glog_warn(msg="State change request response for drone {} with error code {}".format(self.id,cmd.res_data.error.error),source="DM")
                        #self._send_go_to_state_msg(cmd.req_data.state,cmd.req_data.path)
                        pass
                else:
                    self._eval_timeout()
                    #retry?
                    log.debug("command req data: {}".format(cmd.req_data))
                    if not self.timeout_latched:
                        glog_info(msg="Resending state change request for drone {}".format(self.id),source="DM")
                        self._send_go_to_state_msg(cmd.req_data.state,cmd.req_data.path,cmd.req_data.pose)

            #Response to HB. Should only get here in case of comm failure or if drone is docked
            elif cmd.msg_type == MsgType.HB:
                if not cmd.succeeded:
                    self._eval_timeout()
                else:
                    self.timeout_latched = False
                    #We should only be here if the drone is docked and we want to regularly check that it's ready for another job.
                    self._eval_job()

            #Notification to drone manager indicating state change
            elif cmd.msg_type == MsgType.STATE_NOTIF:
                self._update_drone_state(cmd.req_data)
                log.debug("status post update: {}".format(self.last_drone_status.state))
                log.debug("loaded? {}".format(self.drone_status.loaded))
                glog_debug(msg="State change notification for drone {} to state {}".format(self.id,self.drone_status.state),source="DM")
                if self.last_drone_status.state != self.drone_status.state:
                    self._handle_state_change()
                else:
                    log_msg = "state notification received, but state hasn't changed for drone {}".format(self.id)
                    log.warn(log_msg)
                    log.warn("state = {}".format(self.drone_status.state))
                    glog_warn(msg=log_msg,source="DM")
                    #This is unusual and may indicate a dropped message
                    #Consider re-evaluating life choices

            #Notification to drone manager indicating obstruction
            elif cmd.msg_type == MsgType.OBSTRUCTION:
                #Possible race condition if obstruction notification is sent after MM transitions to reroute pending
                self.obstructed = True
                self.obstructed_info = cmd.req_data
                self.obstructed_last_drone_state = self.last_drone_state
                #Wait for the notification of the reroute pending state.
            else:
                log_msg = "Unknown message type"
                log.error(log_msg)
                glog_error(msg=log_msg,source="DM")
        else:
            log.error("Unknown command type")
            glog_error(msg="Unknown command type in cmd_queue for drone {}".format(self.id),source="DM")
            raise NotImplementedError

    def _eval_timeout(self):
        if (datetime.datetime.now() - self.last_comm).total_seconds() > self.max_comm_timeout_s:
            if not self.timeout_latched:
                self.timeout_latched = True
                fleet_manager.notify_drone_timeout(self.id)
        else:
            self.timeout_latched = False


    def _eval_job(self):
        log = logging.getLogger(__name__)
        log_msg = "eval job called. current state is {} for drone {}".format(self.job_status,self.id)
        log.debug(log_msg)
        glog_debug(msg=log_msg,source="DM")
        #Inject commands into cmd queue to execute job. Evaluate when decisions might need to be made, e.g. drone state change
        drone_state = self.drone_status.state
        if self.job_status == JobStatus.PICKUP_PENDING:

            if drone_state == HexSMState.DOCKED:
                self._pickup_pending__docked()
            elif drone_state == HexSMState.WAIT_AT_DOCK:
                self._pickup_pending__wait_at_dock()
            elif drone_state == HexSMState.WAIT_AT_PICKUP:
                self._pickup_pending__wait_at_pickup()
            elif drone_state == HexSMState.GO_TO_DOCK:
                self._pickup_pending__go_to_dock()
            elif drone_state == HexSMState.GO_TO_PICKUP:
                self._pickup_pending__go_to_pickup()
            elif drone_state == HexSMState.REROUTE_PENDING:
                self._pickup_pending__reroute_pending()
            elif drone_state == HexSMState.TAKE_OFF:
                pass
            elif drone_state == HexSMState.PICKUP:
                pass
            elif drone_state == HexSMState.DOCK_LANDING:
                #Only expected if aborted pickup transaction
                pass
            else:
                self._pickup_pending__default()

        if self.job_status == JobStatus.DELIVERY_PENDING:

            if drone_state == HexSMState.WAIT_AT_PICKUP:
                self._delivery_pending__wait_at_pickup()
            elif drone_state == HexSMState.GO_TO_PICKUP:
                self._delivery_pending__go_to_pickup()
            elif drone_state == HexSMState.WAIT_AT_DROP_OFF:
                self._delivery_pending__wait_at_drop_off()
            elif drone_state == HexSMState.GO_TO_DROP_OFF:
                self._delivery_pending__go_to_drop_off()
            elif drone_state == HexSMState.REROUTE_PENDING:
                self._delivery_pending__reroute_pending()
            elif drone_state == HexSMState.GO_TO_DOCK:
                self._delivery_pending__go_to_dock()
            elif drone_state == HexSMState.WAIT_AT_DOCK:
                self._delivery_pending__wait_at_dock()
            elif drone_state == HexSMState.DOCKED:
                self._delivery_pending__docked()
            elif drone_state == HexSMState.DROP_OFF:
                pass
            elif drone_state == HexSMState.DOCK_LANDING:
                #Only expected if aborted drop off transaction
                pass
            else:
                self._delivery_pending__default()

        if self.job_status == JobStatus.RETURN_PENDING:

            if drone_state == HexSMState.WAIT_AT_DROP_OFF:
                self._return_pending__wait_at_drop_off()
            elif drone_state == HexSMState.WAIT_AT_DOCK:
                self._return_pending__wait_at_dock()
            elif drone_state == HexSMState.GO_TO_DOCK:
                self._return_pending__go_to_dock()
            elif drone_state == HexSMState.DOCKED:
                self._return_pending__docked()
            elif drone_state == HexSMState.REROUTE_PENDING:
                self._return_pending__reroute_pending()
            elif drone_state == HexSMState.DOCK_LANDING:
                pass
            else:
                self._return_pending__default()


        if self.job_status == JobStatus.NOT_READY:

            if drone_state == HexSMState.DOCKED:

                if self.battery_status.percent > self.battery_job_ready_threshold:
                    self.job_status = JobStatus.READY
                    self._update_external_drone_status()

        if self.job_status == JobStatus.READY:
            pass

#Job execution lowest level state implementations
    def _pickup_pending__docked(self):
        if (self.battery_status.percent >= self.battery_job_ready_threshold) and not self.grounded_status:

            self.recharging = False
            self.pickup_attempts = 0
            self.drop_off_attempts = 0

            req_path = self.job.take_off_path
            req_state = HexSMState.TAKE_OFF
            req_pose = self.job.take_off_pose
            self._send_go_to_state_msg(req_state, req_path, req_pose)

    def _pickup_pending__wait_at_dock(self):
        if self.recharging or self.grounded_status:

            if self.recharging:
                log_msg = "Entered Wait at Dock for mid-job recharge as expected for drone {} during pickup pending".format(self.id)
            if self.grounded_status:
                log_msg = "Entered Wait at Dock due to obstruction as expected for drone {} during pickup pending".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")

            req_path = self.job.dock_landing_path
            req_state = HexSMState.DOCK_LANDING
            req_pose = self.job.dock_landing_pose
            self._send_go_to_state_msg(req_state, req_path, req_pose)

        else:

            #Double check battery level
            if self.battery_status.percent >= self.battery_post_charge_pickup_threshold:
                req_path = self.job.go_to_pickup_path
                req_state = HexSMState.GO_TO_PICKUP
                req_pose = self.job.go_to_pickup_pose
                self._send_go_to_state_msg(req_state, req_path,req_pose)

            else:
                #This is a weird case, shouldn't get here unless battery_post_charge_pickup_threshold is poorly tuned
                #For now, land again. This could lead to a drone getting stuck in a loop of taking off and landing on the charging station, but that seems like the least bad way to handle it for now.
                self.recharging = True
                req_path = self.job.dock_landing_path
                req_state = HexSMState.DOCK_LANDING
                req_pose = self.job.dock_landing_pose
                self._send_go_to_state_msg(req_state, req_path, req_pose)

    def _pickup_pending__wait_at_pickup(self):

        log_msg = "drone loaded status is: {} for drone {}".format(self.drone_status.loaded,self.id)
        log.debug(log_msg)
        glog_debug(msg=log_msg,source="DM")

        if self.drone_status.loaded:

            log_msg = "Job status changed to Delivery Pending for drone {}".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")

            self.job_status = JobStatus.DELIVERY_PENDING
            self._update_external_drone_status()
            #Communicate to JobLog
            pickup_completed(self.job)

        else:

            if self.battery_status.percent > self.battery_pickup_threshold:

                log_msg = "Battery ok to pickup for drone {}: {} > {}".format(self.id, self.battery_status.percent, self.battery_pickup_threshold)
                log.debug(log_msg)
                glog_debug(msg=log_msg,source="DM")

                if self.pickup_attempts < self.job.pickup_attempts:

                    log_msg = "Attempting pickup for drone {}".format(self.id)
                    log.debug(log_msg)
                    glog_debug(msg=log_msg,source="DM")

                    self.pickup_attempts = self.pickup_attempts + 1
                    req_path = self.job.pick_up_path
                    req_state = HexSMState.PICKUP
                    req_pose = self.job.pick_up_pose
                    self._send_go_to_state_msg(req_state, req_path, req_pose)

                else:
                    log_msg = "Too many pickup attempts for drone {}.".format(self.id)
                    log.warn(log_msg)
                    glog_warn(msg=log_msg,source="DM")

                    if self.attempt_recharge:

                        log_msg = "Attempting to return to dock for drone {}".format(self.id)
                        log.warn(log_msg)
                        glog_warn(msg=log_msg,source="DM")

                        req_path = self.job.go_to_pickup_path[:]
                        req_path.reverse()
                        req_state = HexSMState.GO_TO_DOCK
                        req_pose = self.job.go_to_dock_pose
                        self.recharging = True
                        self._send_go_to_state_msg(req_state, req_path, req_pose)

                    else:

                        log_msg = "Aborting job for drone {}".format(self.id)
                        log.warn(log_msg)
                        glog_warn(msg=log_msg,source="DM")

                        self.abort()

            else:
                log_msg = "Not enough battery left to retry pickup for drone {}. Aborting".format(self.id)
                log.warn(log_msg)
                glog_warn(msg=log_msg,source="DM")

                if self.attempt_recharge:

                    log_msg = "Attempting to return to dock for drone {}".format(self.id)
                    log.warn(log_msg)
                    glog_warn(msg=log_msg,source="DM")

                    req_path = self.job.go_to_pickup_path[:]
                    req_path.reverse()
                    req_state = HexSMState.GO_TO_DOCK
                    req_pose = self.job.go_to_dock_pose
                    self.recharging = True
                    self._send_go_to_state_msg(req_state, req_path, req_pose)

                else:

                    log_msg = "Aborting job for drone {}".format(self.id)
                    log.warn(log_msg)
                    glog_warn(msg=log_msg,source="DM")

                    self.abort()

    def _pickup_pending__go_to_dock(self):
        if self.recharging:
            log_msg = "Entered Go To Dock for mid-job recharge as expected for drone {} during pickup pending".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")
        elif self.grounded_status:
            log_msg = "Entered Go To Dock due to obstruction as expected for drone {} during pickup pending".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")
        else:
            log_msg = "Entered Go To Dock for mid-job recharge unexpectedly for drone {} during pickup pending".format(self.id)
            log.warn(log_msg)
            glog_warn(msg=log_msg,source="DM")

    def _pickup_pending__go_to_pickup(self):
        self._eval_job_expected_transition_log("Go To Pickup")

    def _pickup_pending__reroute_pending(self):

        #TODOFUTURE model reroute_feasible
        reroute_feasible = False #Will definitely be a function of battery

        #First priority to reroute if enabled
        if self.on_obstacle_reroute:
            #Need to look at remaining battery, maybe loaded status (should be unloaded), obstruction info, issue reroute request to air traffic controller if sufficient battery remaining, otherwise abort

            if reroute_feasible:
                #TODOFUTURE Extend to other cases. Future dev as we shouldn't get into this case now.
                #NOTE: This assumes one can only get into REROUTE_PENDING during GO_TO_PICKUP if in JobStatus PICKUP_PENDING.
                #      This is true for now, but would need to be changed if the drone logic allows for other avenues to get into REROUTE PENDING.
                #      Same for the other JobStatus cases, analogously GO_TO_DROP_OFF and GO_TO_DOCK
                new_job = atc.reroute_request(self.job,None)
                self.job = new_job
                req_path = self.job.go_to_pickup_path
                req_state = HexSMState.GO_TO_PICKUP
                req_pose = self.job.go_to_pickup_pose

                self._send_go_to_state_msg(req_state, req_path, req_pose)
                return

        if self.grounded_status:
            #This is the second obstacle encountered, which means escape is impossible
            req_path = []
            req_state = HexSMState.EMERGENCY_LANDING
            req_pose = None
            self._send_go_to_state_msg(req_state, req_path, req_pose)
            return

        self.grounded_status = True

        if self.on_obstacle_reverse_path:
            drone_state = self.drone_status.state

            #Case for satisfying 'emergency land if obstacle is encountered en route to dock'
            if not ((drone_state == HexSMState.GO_TO_DOCK) and (self.on_obstacle_land_enroute_to_dock)):

                #Currently the only state for which this makes sense
                if drone_state == HexSMState.GO_TO_PICKUP:
                    #We need to worry about if coming from dock or pickup.

                    if self.obstructed_last_drone_state == HexSMState.WAIT_AT_DOCK:
                        #Can just reverse course to dock
                        req_path = self._calc_reverse_path( self.job.go_to_pickup_path, self.obstructed_info.pathIndex)
                        req_state = HexSMState.GO_TO_DOCK
                        req_pose = self.job.go_to_dock_pose
                        self._send_go_to_state_msg(req_state, req_path, req_pose)
                        return

                    elif self.obstructed_last_drone_state == HexSMState.WAIT_AT_PICKUP:
                        if drone_state == HexSMState.GO_TO_DOCK:
                            #We are here if we either aborted a pickup because of battery reasons or if we are actively returning to dock because of another obstruction.
                            #It seems reasonable to ELand, even if self.on_obstacle_land_enroute_to_dock is NOT set.
                            pass
                        else:
                            pass

                    else:
                        pass
                        #Shouldn't be here

        #If all else fails, or if this is the only enabled option
        req_path = []
        req_state = HexSMState.EMERGENCY_LANDING
        req_pose = None
        self._send_go_to_state_msg(req_state, req_path, req_pose)
        return


    def _pickup_pending__default(self):
        self._eval_job_default("Pickup Pending")

    def _delivery_pending__wait_at_pickup(self):
        if not self.grounded_status:
            #Normal case
            req_path = self.job.go_to_drop_off_path
            req_state = HexSMState.GO_TO_DROP_OFF
            req_pose = self.job.go_to_drop_off_pose
            self._send_go_to_state_msg(req_state, req_path,req_pose)
        else:
            #Go To Dock via reversed path b/c obstruction
            req_path = self.job.go_to_pickup_path[:]
            req_path.reverse()
            req_state = HexSMState.GO_TO_DOCK
            req_pose = self.job.go_to_dock_pose
            self._send_go_to_state_msg(req_state, req_path, req_pose)


    def _delivery_pending__go_to_pickup(self):
        if self.grounded_status:

            log_msg = "Entered Go To Pickup due to obstruction as expected for drone {} during delivery pending".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")
        else:
            #This is very wrong
            log_msg = "Entered Go To Pickup unexpectedly for drone {} during delivery pending".format(self.id)
            log.debug(log_msg)
            glog_error(msg=log_msg,source="DM")

            req_path = []
            req_state = HexSMState.EMERGENCY_LANDING
            req_pose = None
            self._send_go_to_state_msg(req_state, req_path, req_pose)


    def _delivery_pending__wait_at_drop_off(self):
        if not self.drone_status.loaded:

            self.job_status = JobStatus.RETURN_PENDING
            self._update_external_drone_status()
            #Communicate to JobLog
            drop_off_completed(self.job)

        else:

            if self.battery_status.percent > self.battery_drop_off_threshold:

                if self.drop_off_attempts < self.job.drop_off_attempts:
                    self.drop_off_attempts += 1
                    req_path = self.job.drop_off_path
                    req_state = HexSMState.DROP_OFF
                    req_pose = self.job.drop_off_pose
                    self._send_go_to_state_msg(req_state, req_path, req_pose)

                else:
                    log_msg = "Too many drop-off attempts for drone {}".format(self.id)
                    log.warn(log_msg)
                    glog_warn(msg=log_msg,source="DM")

                    if self.attempt_recharge:

                        log_msg = "Attempting to return to dock for drone {}".format(self.id)
                        log.warn(log_msg)
                        glog_warn(msg=log_msg,source="DM")

                        req_path = self.job.go_to_dock_path
                        req_state = HexSMState.GO_TO_DOCK
                        req_pose = self.job.go_to_dock_pose
                        self.recharging = True
                        self._send_go_to_state_msg(req_state, req_path, req_pose)

                    else:

                        log_msg = "Aborting job for drone {}".format(self.id)
                        log.warn(log_msg)
                        glog_warn(msg=log_msg,source="DM")

                        self.abort()
            else:

                log_msg = "Not enough battery left to retry drop-off for drone {}".format(self.id)
                log.warn(log_msg)
                glog_warn(msg=log_msg,source="DM")

                if self.attempt_recharge:

                    log_msg = "Attempting to return to dock for drone {}".format(self.id)
                    log.warn(log_msg)
                    glog_warn(msg=log_msg,source="DM")

                    req_path = self.job.go_to_dock_path
                    req_state = HexSMState.GO_TO_DOCK
                    req_pose = self.job.go_to_dock_pose
                    self.recharging = True
                    self._send_go_to_state_msg(req_state, req_path, req_pose)

                else:

                    log_msg = "Aborting job for drone {}".format(self.id)
                    log.warn(log_msg)
                    glog_warn(msg=log_msg,source="DM")

                    self.abort()

    def _delivery_pending__go_to_drop_off(self):
        self._eval_job_expected_transition_log("Go To Drop Off")

    def _delivery_pending__reroute_pending(self):

        #TODOFUTURE model reroute_feasible
        reroute_feasible = False #Will definitely be a function of battery

        #First priority to reroute if enabled
        if self.on_obstacle_reroute:
            #Need to look at remaining battery, maybe loaded status (should be unloaded), obstruction info, issue reroute request to air traffic controller if sufficient battery remaining, otherwise abort

            if reroute_feasible:
                #TODOFUTURE Extend to other cases. Future dev as we shouldn't get into this case now.
                new_job = atc.reroute_request(self.job,None)
                self.job = new_job
                req_path = self.job.go_to_drop_off_path
                req_state = HexSMState.GO_TO_DROP_OFF
                req_pose = self.job.go_to_drop_off_pose

                self._send_go_to_state_msg(req_state, req_path, req_pose)
                return

        if self.grounded_status:
            #This is the second obstacle encountered, which means escape is impossible
            req_path = []
            req_state = HexSMState.EMERGENCY_LANDING
            req_pose = None
            self._send_go_to_state_msg(req_state, req_path, req_pose)
            return

        #This applies to all situations not resolved by dynamic reroute
        self.grounded_status = True

        if self.on_obstacle_reverse_path:
            drone_state = self.drone_status.state

            #Case for satisfying 'emergency land if obstacle is encountered en route to dock'
            if not ((drone_state == HexSMState.GO_TO_DOCK) and (self.on_obstacle_land_enroute_to_dock)):

                #Currently the only state for which this makes sense
                if drone_state == HexSMState.GO_TO_DROP_OFF:
                    #We need to worry about if coming from drop off or pickup.

                    if self.obstructed_last_drone_state == HexSMState.WAIT_AT_DOCK:
                        #Can just reverse course to dock
                        prior_path = self.job.go_to_dock_path
                        prior_path.reverse()

                        req_path = self._calc_reverse_path( prior_path, self.obstructed_info.pathIndex)
                        req_state = HexSMState.GO_TO_DOCK
                        req_pose = self.job.go_to_dock_pose
                        self._send_go_to_state_msg(req_state, req_path, req_pose)
                        return

                    elif self.obstructed_last_drone_state == HexSMState.WAIT_AT_PICKUP:
                        #Need to reverse course to wait at pickup, then continue to use 'grounded' flag to route back to dock
                        req_path = self._calc_reverse_path( self.job.go_to_pickup_path, self.obstructed_info.pathIndex)
                        req_state = HexSMState.GO_TO_PICKUP
                        req_pose = self.job.go_to_pickup_pose
                        self._send_go_to_state_msg(req_state, req_path, req_pose)
                        return

                    else:
                        pass
                        #Shouldn't be here

                elif drone_state == HexSMState.GO_TO_DOCK:
                    #TODOFUTURE
                    #In a strict sense, this should reroute the drone through its entire path, but it seems wrong to do that
                    #ELand instead
                    pass


        #If all else fails, or if this is the only enabled option
        req_path = []
        req_state = HexSMState.EMERGENCY_LANDING
        req_pose = None
        self._send_go_to_state_msg(req_state, req_path, req_pose)
        return


    def _delivery_pending__go_to_dock(self):
        if self.recharging:

            log_msg = "Entered Go To Dock for mid-job recharge as expected for drone {} during drop-off pending".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")
        if self.grounded_status:

            log_msg = "Entered Go To Dock due to obstruction as expected for drone {} during drop-off pending".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")

    def _delivery_pending__wait_at_dock(self):
        if self.recharging or self.grounded_status:

            if self.recharging:
                log_msg = "Entered Wait at Dock for mid-job recharge as expected for drone {} during drop-off pending".format(self.id)
            if self.grounded_status:
                log_msg = "Entered Wait at Dock due to obstruction as expected for drone {} during drop-off pending".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")

            req_path = self.job.dock_landing_path
            req_state = HexSMState.DOCK_LANDING
            req_pose = self.job.dock_landing_pose
            self._send_go_to_state_msg(req_state, req_path, req_pose)

        else:

            log_msg = "Entered Wait at Dock for mid-job recharge during drop-off pending".format(self.id)
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="DM")

            #Double check battery level
            if self.battery_status.percent >= self.battery_post_charge_drop_off_threshold:

                req_path = self.job.go_to_dock_path[:]
                req_path.reverse()
                req_state = HexSMState.GO_TO_DROP_OFF
                req_pose = self.job.go_to_drop_off_pose
                self._send_go_to_state_msg(req_state, req_path,req_pose)

            else:

                #This is a weird case, shouldn't get here unless battery_post_charge_drop_off_threshold is poorly tuned
                #For now, land again. This could lead to a drone getting stuck in a loop of taking off and landing on the charging station, but that seems like the least bad way to handle it for now.
                self.recharging = True
                req_path = self.job.dock_landing_path
                req_state = HexSMState.DOCK_LANDING
                req_pose = self.job.dock_landing_pose
                self._send_go_to_state_msg(req_state, req_path, req_pose)

    def _delivery_pending__docked(self):
        if not (self.recharging or self.grounded_status):

            #This shouldn't happen. Don't actually doing anything about it for now except log a msg.
            log_msg = "Unexpected drone state is docked for job state drop-off pending for drone {}, recharging flag not set".format(self.id)
            log.warn(log_msg)
            glog_warn(msg=log_msg,source="DM")

        if (self.battery_status.percent >= self.battery_job_ready_threshold) and not self.grounded_status:

            self.recharging = False
            self.pickup_attempts = 0
            self.drop_off_attempts = 0

            req_path = self.job.take_off_path
            req_state = HexSMState.TAKE_OFF
            req_pose = self.job.take_off_pose
            self._send_go_to_state_msg(req_state, req_path, req_pose)

    def _delivery_pending__default(self):
        self._eval_job_default("Delivery Pending")

    def _return_pending__wait_at_drop_off(self):
        req_path = self.job.go_to_dock_path
        req_state = HexSMState.GO_TO_DOCK
        req_pose = self.job.go_to_dock_pose
        self._send_go_to_state_msg(req_state, req_path, req_pose)

    def _return_pending__wait_at_dock(self):
        req_path = self.job.dock_landing_path
        req_state = HexSMState.DOCK_LANDING
        req_pose = self.job.dock_landing_pose
        self._send_go_to_state_msg(req_state, req_path, req_pose)

    def _return_pending__go_to_dock(self):
        self._eval_job_expected_transition_log("Go To Dock")

    def _return_pending__docked(self):
        self.job_status = JobStatus.NOT_READY
        atc.job_done(self.job)
        #Communicate to JobLog
        job_completed(self.job)
        self.job = None
        self._update_external_drone_status()

    def _return_pending__reroute_pending(self):

        #TODOFUTURE model reroute_feasible
        reroute_feasible = False #Will definitely be a function of battery

        #First priority to reroute if enabled
        if self.on_obstacle_reroute:
            #Need to look at remaining battery, maybe loaded status (should be unloaded), obstruction info, issue reroute request to air traffic controller if sufficient battery remaining, otherwise abort

            if reroute_feasible:
                new_job = atc.reroute_request(self.job,None)
                self.job = new_job
                req_path = self.job.go_to_dock_path
                req_state = HexSMState.GO_TO_DOCK
                req_pose = self.job.go_to_dock_pose

                self._send_go_to_state_msg(req_state, req_path, req_pose)
                return

        if self.grounded_status:
            #This is the second obstacle encountered, which means escape is impossible
            req_path = []
            req_state = HexSMState.EMERGENCY_LANDING
            req_pose = None
            self._send_go_to_state_msg(req_state, req_path, req_pose)
            return

        #This applies to all situations not resolved by dynamic reroute
        self.grounded_status = True

        if self.on_obstacle_reverse_path:
            drone_state = self.drone_status.state

            #Case for satisfying 'emergency land if obstacle is encountered en route to dock'
            if not ((drone_state == HexSMState.GO_TO_DOCK) and (self.on_obstacle_land_enroute_to_dock)):
                #I don't know how we would get here in job state 'Return pending'

                #DUMMY logic to ELand for now because we should never get here
                req_path = []
                req_state = HexSMState.EMERGENCY_LANDING
                req_pose = None
                self._send_go_to_state_msg(req_state, req_path, req_pose)
                return


        #If all else fails, or if this is the only enabled option
        req_path = []
        req_state = HexSMState.EMERGENCY_LANDING
        req_pose = None
        self._send_go_to_state_msg(req_state, req_path, req_pose)
        return



    def _return_pending__default(self):
        self._eval_job_default("Return Pending")

#Generic methods for the above
    def _eval_job_default(self,job_state=""):
        log_msg = "Unexpected drone state {} for job state {} for drone {}".format(self.drone_status.state, job_state, self.id)
        log.warn(log_msg)
        glog_warn(msg=log_msg,source="DM")

    def _eval_job_expected_transition_log(self,new_state=""):
        log_msg = "Entered {} as expected for drone {}".format(new_state,self.id)
        log.info(log_msg)
        glog_info(msg=log_msg,source="DM")


    def _calc_reverse_path(self,path,last_index):
        return_path = path[0:last_index + 1]
        return_path.reverse()
        return return_path



    def _incomplete_job_action(self):
        if self.job is not None:
            job_incomplete(self.job)
            glog_warn(msg="Incomplete job action placeholder called for drone {}".format(self.id),source="DM")
            #Placeholder. need to do something if job cannot be performed fully


if __name__ == '__main__':
    dummy_fleet_manager_queue = Queue()
    dummy_fleet_manager_callback_queue = Queue()
    drone_id = "FlightProSim"
    drone_manager = DroneManager(drone_id,None)
    drone_manager.start()
    pickup = drone_manager.debug_pickup_loc
    dropoff = drone_manager.debug_dropoff_loc
    pickup_attempts = 1
    drop_off_attempts = 1
    payload_id = "ABCD1234"
    job = Job(pickup,dropoff,pickup_attempts,drop_off_attempts,payload_id)
    drone_manager.assign_job(job)
    while(True):
        time.sleep(1)
