#TODO fix import paths
import sys
sys.path.append("/home/labdrone/Creare_Data/LabDrone/labdrone-internal/gui/python") 
import rospy
from geometry_msgs.msg import PoseStamped, Pose
from acl_msgs.msg import ViconState
from gui_server import MainHandler

class ViconListener():
    def __init__(self):
	print("initializing")
	self.initial_position = None
	self.GUI = MainHandler.getInstance()
        self.pose = Pose()
        rospy.init_node('listener', anonymous=True)
        rospy.Subscriber("/FlightPro1/world", PoseStamped, self.callback)
	print("made it through init")
        
    def callback(self,data):
        pose = data.pose
	if self.initial_position is None:
		self.initial_position = pose.position
#	print(pose)
        #TODO connect to gui server
        #How does this distinguish between drones? is that handled at the ros rebroadcast level? Did I miss some configuration in joy.py?
        self.GUI.set_drone_position([100*(pose.position.x-self.initial_position.x), 100*(pose.position.y-self.initial_position.y), 100*(pose.position.z-self.initial_position.z)])
        
if __name__ == '__main__':
    #edited MainHandler to do this.
    #Normally this would happen elsewhere. It needs to be run 
    #from gui_server import TornadoDaemon
    #TornadoDaemon.getInstance().start()
    
    listener = ViconListener()
    rospy.spin()
