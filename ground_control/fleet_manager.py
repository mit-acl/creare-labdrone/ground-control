import logging
import threading
import Queue
import time
import datetime

#Intended to be transitioned to rospy.get_param
from ros_param_proxy import get_param

import rospy
from snapstack_msgs.msg import State
from std_srvs.srv import Empty, EmptyResponse, SetBool

from air_traffic_controller import request_job, get_num_active_jobs
#from defs import DroneState
from defs import DroneStatus
from defs import FleetManagerCmdType as CmdType
from defs import FleetManagerDroneStatus as DroneStatus #Note DroneManager uses a different class as DroneStatus
from defs import FleetMgrKillCmd
from defs import FleetMgrGroundCmd
from defs import FleetMgrTimeoutCmd
from defs import JobStatus

import drone_manager as dm

from gui.src.gui_server import MainHandler

from gui_log import glog, glog_debug, glog_info, glog_warn, glog_error

class FleetManager(object):
    #""""
    #Top-level manager for all drones. Delegates jobs and handles external interrupt conditions.
    #""""
    __instance = None
    @staticmethod
    def getInstance():
        """ Static access method. """
        if FleetManager.__instance == None:
            #This works better when the constructor doesn't take any arguments. That said, it would be awfully strange if something was trying to get the singleton instance of FleetManager
            #Before it was constructed by ground control.
            raise Exception("Fleet Manager isn't instantiated yet.")
        return FleetManager.__instance
    def __init__(self,job_spec_queue):
        """ Virtually private constructor. """
        if FleetManager.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            FleetManager.__instance = self

            glog_info(msg="Initializing Fleet Manager",source="FM")
            #Job queue communicates jobs between scheduler and fleet manager
            self.job_spec_queue = job_spec_queue
            self.pending_job_spec = None

            #Used to mediate access to the fleetstatus dictionary
            self.drone_update_lock = threading.Lock()

            #Cmd queue is the main processing queue for fleet manager that integrates status messages from individual drone managers into a
            #holistic picture of the fleet, as well as delegating jobs to specific drone managers and emergency instructions.
            self.cmd_queue = Queue.Queue()

            #High-level stop signal for shutting down fleet manager processes
            self.stop_flag = True

            #Initialize fleet status variables
            self.fleet_status = {}
            self.fleet = {}
            self.drone_is_ready = False # convenience variable for "At least one drone is ready"

            self.ground_all_on_timeout = get_param('/gc/fleet_manager/ground_all_on_timeout')


            #Instantiate drone manager objects
            self.drone_ids = get_param('/gc/fleet_manager/drone_ids')
            self.drone_arm_svcs = {}
            for drone_id in self.drone_ids:
                self.fleet[drone_id]=dm.DroneManager(drone_id)
                self.fleet_status[drone_id]=DroneStatus()
                
                arm_svc_name = str(drone_id)+"/snap/arm"
                self.drone_arm_svcs[drone_id] = rospy.ServiceProxy(arm_svc_name, SetBool)

            #Period for updating GUI with drone positions
            self.position_update_period_s = get_param('/gc/fleet_manager/position_update_period_s')

            self.gui_server_main_handler = MainHandler.getInstance()

            self.ext_cmd_kill_svc = rospy.Service("KillGC", Empty, self._ext_cmd_kill_handler)
            self.ext_cmd_ground_svc = rospy.Service("GroundGC", Empty, self._ext_cmd_ground_handler)
            
            


#public high-level command for starting fleet manager process
    def start(self):
        glog_info(msg="Starting Fleet Manager",source="FM")
        self.stop_flag = False
        self.cmd_queue_thread = threading.Thread(target=self._read_cmd_queue,args=[])
        self.cmd_queue_thread.start()
        self.job_queue_thread = threading.Thread(target=self._read_job_queue,args = [])
        self.job_queue_thread.start()
        for drone_id in self.fleet:
            self.fleet[drone_id].start()
        self._schedule_periodic_position_broadcast(threading.Event())

    def _read_cmd_queue(self):
        log = logging.getLogger(__name__)
        while(self.stop_flag == False):
            glog_debug(msg="Attempting to read command queue",source="FM")
            log.debug("Fleet Manager: Attempting to read command queue.")
            new_cmd = self.cmd_queue.get() #blocking read
            #TODO Differentiate between KILL and GROUND

            #TODO remove.
            #if new_cmd.cmd_type == CmdType.PAUSE:
            #    pass
            if new_cmd.cmd_type == CmdType.KILL:
                glog_warn(msg="External kill command invoked",source="FM")
                if new_cmd.drone_ids is None:
                    #If no ids specified, disarm all drones. Expected use case
                    for drone_id in self.drone_ids:
                        self.drone_arm_svcs[drone_id](False)
                else:
                    #Remove dups. Should probably do other type checking
                    kill_drone_ids = list(dict.fromkeys(new_cmd.drone_ids))
                    for kill_drone_id in kill_drone_ids:
                        for drone_id in self.drone_ids:
                            if kill_drone_id == drone_id:
                                self.drone_arm_svcs[drone_id](False)
            elif new_cmd.cmd_type == CmdType.GROUND:
                glog_warn(msg="External ground command invoked",source="FM")
                if new_cmd.drone_ids is None:
                    #If no ids specified, issue KILL to all drones. Expected use case
                    for drone_id in self.drone_ids:
                        self.fleet[drone_id].ground()
                else:
                    #Remove dups. Should probably do other type checking
                    ground_drone_ids = list(dict.fromkeys(new_cmd.drone_ids))
                    for ground_drone_id in kill_drone_ids:
                        for drone_id in self.drone_ids:
                            if ground_drone_id == drone_id:
                                self.fleet[drone_id].ground()
            elif new_cmd.cmd_type == CmdType.TIMEOUT_EXCEEDED:

                timed_out_drone_id = new_cmd.drone_id
                if self.ground_all_on_timeout:
                    ground_cmd = FleetMgrGroundCmd()
                    self.cmd_queue.put(ground_cmd)


    def _read_job_queue(self):
        log = logging.getLogger(__name__)
        log_msg = "Fleet Manager: _read_job_queue called for the first time."
        log.debug(log_msg)
        glog_debug(msg=log_msg,source="FM")
        while(self.stop_flag == False):
            log_msg = "Fleet Manager: Attempting to read command queue."
            log.debug(log_msg)
            glog_debug(msg=log_msg,source="FM")
            if self.pending_job_spec is None:
                self.pending_job_spec = self.job_spec_queue.get() #blocking read
            else:
                if self.drone_is_ready:
                    log_msg = "Fleet Manager: Drone is ready and job available. Attempting to assign"
                    log.debug(log_msg)
                    glog_debug(msg=log_msg,source="FM")
                    #Figure out which drone to assign
                    #update job ready status of drone so that it can't be double assigned
                    #enqueue cmd to assign job/directly assign job

                    candidate_drone_ids = []

                    for drone_id in self.fleet_status:
                        if self.fleet_status[drone_id].job_status == JobStatus.READY:
                            candidate_drone_ids.append(drone_id)
                    #Doesn't really hurt anything, but this is now triply redundant in checking that we are requesting for a case where drones are available.
                    if len(candidate_drone_ids) > 0:

                        job, num_registered_jobs = request_job(self.pending_job_spec,candidate_drone_ids)
                        if job is None:
                            #This should happen if the atc can't find a viable flight plan. Wait a bit. May need to refine if there are other ways request_job could return None
                            #Ask ATC if there are any registered flight plans. If not then, this job probably can never be completed
                            if num_registered_jobs == 0:
                                glog_warn(msg="Impossible to assign job. Discarding.",source="FM")
                                self.pending_job_spec = None
                            else:
                                glog_warn(msg="Cannot assign job, but may be possible in the future. Waiting.",source="FM")
                                time.sleep(10)
                        else:
                            #Success!
                            glog_info(msg="Job successfully assigned",source="FM")
                            drone_id = job.drone_id
                            self.fleet[drone_id].assign_job(job)
                            self.pending_job_spec = None #Reset
                    else:
                        log_msg = "We really shouldn't get here. That would probably indicate the drone_is_ready flag isn't being calculated accurately"
                        log.warn(log_msg)
                        glog_warn(msg=log_msg,source="FM")
                        time.sleep(1)

                else:
                    log_msg = "Fleet Manager: Job is ready but no drones are ready."
                    log.debug(log_msg)
                    glog_debug(msg=log_msg,source="FM")
                    #Can't think of a more elegant way to deal with case of ready jobs but waiting on drones for now. It shouldn't be a big deal if there is
                    #an artificial delay on the order of 1s introduced by this method between the earliest moment a drone could be assigned a job and when it actually is
                    #Can also tune the delay.
                    time.sleep(1)

    def check_drone_ready(self):
        drone_ready = False
        for id in self.fleet_status:
            drone_ready = drone_ready or (self.fleet_status[id].job_status == JobStatus.READY)
        self.drone_is_ready = drone_ready

    def choose_drone(self,candidate_jobs):
        #Basic idea here is:
        #1) Fleet manager requests Job info for multiple candidate drones and the same JobSpec from AirTrafficController
        #2) AirTrafficController finds candidate paths for at least two drones but doesn't have full information to make the best decision. So - it invokes choose_drone
        #3) choose_drone takes a look at all of the information, including battery levels, metrics from the flight plans, etc... and returns an index associated with the preferred option to AirTrafficController
        #4) Air traffic controller finally returns the requested information to the original call (1).
        #It's also totally possible that we don't want to bother involving Fleet Manager in this way, but we could using this scheme.
        return 0

    def _schedule_periodic_position_broadcast(self,f_stop):
        if self.stop_flag:
            f_stop.set()
        else:
            self._broadcast_drone_position()
            threading.Timer(self.position_update_period_s, self._schedule_periodic_position_broadcast, [f_stop]).start()

    def _broadcast_drone_position(self):
        output = []
        for drone_id in self.drone_ids:
            topic_name = r"/" + drone_id + r"/state"
            topic_type = State
            timeout = 1
            state = None
            try:
                state = rospy.wait_for_message(topic_name,topic_type,timeout)
            except:
                pass
            if state is not None:
                output.append([state.pos.x,state.pos.y,state.pos.z])
            else:
                #We could do something more elegant here, but if this happens,
                #either it's a basic config issue, or something is really wrong.
                output.append([0,0,0])
        self.gui_server_main_handler.set_drone_position(output)

    def broadcast_fleet_status(self):
        output = []
        for key in self.fleet_status:
            #Make a flat dictionary
            drone_status_dict = {"drone_id":key}
            drone_status_dict.update(self.fleet_status[key].to_dict())

            job_status = drone_status_dict["job_status"]
            if job_status is None:
                job_status_string = 'None'
            else:
                job_status_string = job_status.name #Enum name
            drone_status_dict.update({"job_status":job_status_string})

            #Convert the datetime difference to ms so json can encode it
            last_contact = drone_status_dict["last_contact"]
            if last_contact is None:
                last_contact_duration_ms = -1
            else:
                last_contact_duration = datetime.datetime.now() - last_contact
                last_contact_duration_ms = (last_contact_duration.seconds * 1000) + (last_contact_duration.microseconds / 1000)
            drone_status_dict.update({"last_contact":last_contact_duration_ms})

            output.append(drone_status_dict)
        self.gui_server_main_handler.set_fleet_status(output)

    def _ext_cmd_kill_handler(self,request):
#        print("FM:kill service invoked")
        kill_cmd = FleetMgrKillCmd()
        self.cmd_queue.put(kill_cmd)
        return EmptyResponse()

    def _ext_cmd_ground_handler(self,request):
        ground_cmd = FleetMgrGroundCmd()
        self.cmd_queue.put(ground_cmd)
        return EmptyResponse()


def update_drone_status(drone_id, drone_status):
    #Need to protect against simultaneous access from different drone managers
    fleet_manager = FleetManager.getInstance()
    with fleet_manager.drone_update_lock:

        if drone_id in fleet_manager.fleet_status:
            fleet_manager.fleet_status.update({drone_id: drone_status})
            fleet_manager.check_drone_ready()
            fleet_manager.broadcast_fleet_status()
        else:
            glog_warning(msg="Update drone status failed, drone not in status dictionary. This shouldn't happen.",source="FM")
            #Raise error? update with new key? At least log a warning
            pass


def notify_drone_timeout(drone_id):

    fleet_manager = FleetManager.getInstance()

    timeout_msg = FleetMgrTimeoutCmd(drone_id)
    fleet_manager.cmd_queue.put(timeout_msg)


def choose_drone(candidate_jobs):
    fleet_manager = FleetManager.getInstance()
    index = fleet_manager.choose_drone(candidate_jobs)
    return index

    # def _read_comm_queue(self):
    #     while(self.stop_flag == False):
    #         msg = self.comm_queue.get()
    #         #switch on msg.
    #         #construct command queue element.

