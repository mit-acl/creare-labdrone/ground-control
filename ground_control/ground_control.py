import time
import Queue

import rospy

from scheduler import Scheduler
from fleet_manager import FleetManager
from dummy_lab_manager import DummyLabManager

from labdrone_msgs.msg import TransportJob #DEBUG

from gui_log import glog, glog_debug, glog_info, glog_warn, glog_error


class GroundControl(object):
    def __init__(self):
        glog_info(msg="Starting ground controller",source="GC")
        #Consider making global or singleton class. Depends on how much this needs to be passed around
        q = Queue.Queue()

        rospy.init_node("GroundController")
        self.scheduler = Scheduler(q)
        self.fleet_manager = FleetManager(q)
        self.stop_signal = False


if __name__ == '__main__':
    gc = GroundControl()

    #cmd_proxy = TransportJob()
    #cmd_proxy.pickup = 'Nest6'
    #cmd_proxy.drop_off = 'Nest7'
    #cmd_proxy.pickup_attempts = 2
    #cmd_proxy.drop_off_attempts = 5
    #cmd_proxy.payload_id = "6->7"
    #cmd_proxy.count = 10
    #cmd_proxy.period_s = 15
    #gc.scheduler.ext_cmd_proxy(cmd_proxy)

    gc.fleet_manager.start()


    time.sleep(5)
    dlm = DummyLabManager()
#    cmd_proxy = TransportJob()
#    cmd_proxy.pickup = 'Nest2'
#    cmd_proxy.drop_off = 'Nest5'
#    cmd_proxy.pickup_attempts = 2
#    cmd_proxy.drop_off_attempts = 5
#    cmd_proxy.payload_id = "2->5"
#    cmd_proxy.count = 10
#    cmd_proxy.period_s = 15
#    gc.scheduler.ext_cmd_proxy(cmd_proxy)

    
    while(True):
        time.sleep(1)

