import yaml
import io

initialized = False
params_file = "params.yaml"
base_name = '/gc/'

params = None


def _init():
    # Read YAML file
    with open(params_file, 'r') as stream:
        global params
        params = yaml.safe_load(stream)
#    print(params)
    initialized = True


def get_param(param_name):
    #This is a temporary implementation, so not trying to make it terribly robust.
    if not initialized:
        _init()
#        print('initialized')

    try:
        trimmed_name = param_name[len(base_name):]
#        print(trimmed_name)
#        print(params)
        value = params[trimmed_name]
        return value
    except:
        print("param load failed")
        return None
