
import rospy

from std_srvs.srv import Empty, EmptyResponse
from labdrone_msgs.msg import TransportJob, TransactionInfo
from labdrone_msgs.srv import TransportJobReq, TransactionNotification

class TrayStatus():
    def __init__(self,location_id,location_type):
        #self.id = id
        self.location_id = location_id
        self.location_type = location_type

    def pickup(self,drone_id):
        if self.location_type == "NEST":
            self.location_id=drone_id
            self.location_type = "DRONE"
            success = True
        else:
            #Shouldn't be here
            success = False
        return success

    def drop_off(self,nest_id):
        if self.location_type == "DRONE":
            self.location_id=nest_id
            self.location_type = "NEST"
            success = True
        else:
            #Shouldn't be here
            success = False
        return success

class DummyLabManager():

    def __init__(self):

        self.sched_proxy = rospy.ServiceProxy("TransportRequest",TransportJobReq)

        self.transaction_notification_svc = rospy.Service("TransactionNotification", TransactionNotification, self._transaction_notif_handler)

        self.inventory = {}
        self.inventory["Tray1"] = TrayStatus("Nest2","NEST")

        #Total dummy logic. Swap back and forth between nest 2 and 5 forever
        self.transport_rules = {}
        self.transport_rules["Nest2"] = "Nest5"
        self.transport_rules["Nest5"] = "Nest2"

        #Init sequence of transports
        self._check_transport_logic("Tray1")


    def _update_inventory(self,transaction_type,drone_id,nest_id,tray_id):
        if self.inventory.has_key(tray_id):
            success = False
            if transaction_type == "PICKUP":
                success = self.inventory[tray_id].pickup(drone_id)
            elif transaction_type == "DROP_OFF":
                success = self.inventory[tray_id].drop_off(nest_id)

            if success:
                self._check_transport_logic(tray_id)

    def _check_transport_logic(self,tray_id):
        if self.inventory[tray_id].location_type=="NEST":
            cur_nest = self.inventory[tray_id].location_id
            next_nest = self.transport_rules[cur_nest]
            
            new_job = TransportJob(cur_nest,next_nest,3,3,tray_id,1,0)
            response = self.sched_proxy(new_job)


    def _transaction_notif_handler(self,request):

        transaction_type = request.info.transaction_type
        drone_id = request.info.drone_id
        nest_id = request.info.nest_id
        tray_id = request.info.tray_id

        self._update_inventory(transaction_type,drone_id,nest_id,tray_id)

        return True
