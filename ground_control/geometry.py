# -*- coding: utf-8 -*-

"""
Created on Tue, 24 Dec, 2019

@author: @jkp

Geometry module for advanced plate transfer ground controller.
"""

import logging
import json
import copy
from collections import Iterable

import numpy as np
from scipy.spatial import ConvexHull
from scipy.ndimage.interpolation import rotate
from scipy.spatial.transform import Rotation as R

import database as db

NUMERICAL_PRECISION = 0.000001
HUMAN_PRECISION = 0.1

class Cuboid(object):
    def __init__(self, cuboid=None, points=None, JSON=None):
        """Chose one input for constructing a Cuboid.
            Choices:
            cuboid: another Cuboid or a CuboidsSQL object.
            points: an array of numpy 3-vectors with points along the outside
                boundary of the cuboid.
            JSON: the JSON description of a Cuboid as used by the GUI.
        """
        log = logging.getLogger(__name__)

        self.bounds = None
        if isinstance(cuboid, db.CuboidsSQL):
            log.debug("Creating Cuboid from CuboidSQL object.")
            self.from_json(cuboid.json)
        elif isinstance(cuboid, Cuboid):
            log.debug("Creating Cuboid from another Cuboid object.")
            self.bounds = cuboid.bounds
        elif isinstance(points, list):
            if len(points) > 0:
                if isinstance(points[0], Pose):
                    log.debug("Creating Cuboid from list of Pose objects.")
                    pts = np.array([p.location for p in points])
                else:
                    log.debug("Creating Cuboid from list of points.")
                    pts = np.array(points)
                self.construct_from_bounding_points(pts)
            else:
                raise Exception("Missing points for creating Cuboid.")
        elif isinstance(points, np.ndarray):
            log.debug("Creating Cuboid from ndarray of points.")
            self.construct_from_bounding_points(points)
        elif isinstance(JSON, str):
            log.debug("Creating Cuboid from JSON.")
            self.from_json(JSON)
        elif isinstance(JSON, dict):
            log.debug("Creating Cuboid from dict.")
            self.from_dict(JSON)
                    
    def construct_from_bounding_points(self, points):
        """
        Takes a numpy array of 3-vectors. Requires at least 4 points.
        
        Points are checked for approximate coplanarity in z-plane. If points 
        are found to be approximately all in z-plane, object is assumed to 
        extend from that z-position down to z = 0.
            
        Corners of objects are labeled as follows:
                  2------------3
                 / |         / |
                0-----------1  |
                |  |        |  |
                |  |        |  |
                |  6--------|--7
                | /         | /
                4-----------5
                
        Faces are (face #: corner #s):
        {
            0: 0, 1, 2, 3;
            1: 0, 1, 4, 5;
            2: 0, 2, 4, 6;
            3: 1, 3, 5, 7;
            4: 4, 5, 6, 7;
            5: 2, 3, 6, 7
        }
        """
        assert(points is not None)
        log = logging.getLogger(__name__)

        if not isinstance(points, np.ndarray):
            #This isn't the best check for iterability in python, but really,
            # I don't want this to accept ALL iterable objects; lots of stuff
            #is iterable that isn't the correct type of data
            assert(isinstance(points, Iterable))
            try:
                points = np.array(points)
            except TypeError as e:
                log.error("Invalid input type: {}".format(e))
                raise e
        assert(len(points) > 3)
        #check if points fall in approximately the same z plane
        j = 0
        planar = True
        while j < len(points):
            if np.any(np.abs(points[j,2] - points[:,2]) > HUMAN_PRECISION):
                planar = False
                break
            j = j + 1
                
        if planar:
            log.debug("Cuboid construction from approximately planar points.")
            zmin = 0
            zmax = np.max(points[:,2])
        else:
            zmin = np.min(points[:,2])
            zmax = np.max(points[:,2])
            
        #get x-y bounding box:
        rectangle = self.minimum_bounding_rectangle(points[:,0:2])
        
        #store the 8 bounding corners
        self.bounds = np.zeros((8,3))
        self.bounds[:4, 0:2] = rectangle
        self.bounds[4:, 0:2] = rectangle
        self.bounds[:4,2] = zmax
        self.bounds[4:, 2] = zmin
        
    def from_dict(self, json_dict):
        """Import dict representation of cuboid to internal bounding
            corner representation."""
        assert isinstance(json_dict, dict)
        try:
            cuboid = json_dict
            assert('centroid' in cuboid)
            assert('length' in cuboid)
            
            position = np.array([cuboid['centroid']['x'], cuboid['centroid']['y'], cuboid['centroid']['z']])
            size = np.array([cuboid['length']['x'], cuboid['length']['y'], cuboid['length']['z']])
            rotation = cuboid['theta_z'] if 'theta_z' in cuboid else 0.0

            xmin = -size[0]/2.
            xmax = size[0]/2.
            ymin = -size[1]/2.
            ymax = size[1]/2.
            zmin = -size[2]/2.
            zmax = size[2]/2.
            
            #construct unrotated cuboid boundsaround (0,0,0)
            bounds = np.array([[xmin, ymin, zmax],
                              [xmax, ymin, zmax],
                              [xmin, ymax, zmax],
                              [xmax, ymax, zmax],
                              [xmin, ymin, zmin],
                              [xmax, ymin, zmin],
                              [xmin, ymax, zmin],
                              [xmax, ymax, zmin]])
                              
            #rotate
            r = R.from_euler('z', rotation, degrees=True)
            r.apply(bounds)            
            
            #translate
            bounds = bounds + position
            self.bounds = bounds
            if self.bounds is None:
                raise Exception("Error constructing Cuboid from dict. Bounds None.")
        except ValueError as ve:
            log.error("Error processing Cuboid from dict: {}".format(json_dict))
            log.error(ve)
            raise ve
            
    def from_json(self, JSON):
        """Import JSON string representation of cuboid to internal bounding
            corner representation."""
        assert isinstance(JSON, str)
        try:
            cuboid = json.loads(JSON)
            self.from_dict(cuboid)
        except ValueError as ve:
            log.error("Error processing Cuboid from JSON: {}".format(JSON))
            log.error(ve)
            raise ve
        
    def to_dict(self):
        """Export dict representation of cuboid as precursor to JSON."""
        #get centroid and shift cuboid by centroid
        centroid = self.get_position()
        bounds = copy.copy(self.bounds) - centroid
        
        #calculate rotation:
        vec0to1 = bounds[1]-bounds[0]
        rotation = np.arctan(vec0to1[1] - vec0to1[0])*np.pi/180
        
        r = R.from_euler('z', -rotation, degrees=True)
        bounds = r.apply(bounds)
        mins = np.min(bounds, axis=0)
        maxes = np.max(bounds, axis=0)
        
        cuboid_dict = {   
                        "centroid": {'x': centroid[0],
                                     'y': centroid[1],
                                     'z': centroid[2]},
                        "length": {'x': maxes[0]-mins[0],
                                   'y': maxes[1]-mins[1],
                                   'z': maxes[2]-mins[2]},
                        "theta_z": rotation
                      }
        
        return cuboid_dict
        
    def to_json(self, pretty=False):
        """Export JSON string representation of cuboid."""
        if pretty:
            json_out = json.dumps(self.to_dict(), indent=2)
        else:
            json_out = json.dumps(self.to_dict())
        return json_out
        
        
    def move(self, position):
        self.offset = position

    def get_position(self):
        return np.mean(self.bounds, axis=0)

    def get_walls(self, buffer):
        """
        Returns Plane objects for all walls of Cuboid.
        
          2------------3
         / |         / |
        0-----------1  |
        |  |        |  |
        |  |        |  |
        |  6--------|--7
        | /         | /
        4-----------5
        
        Faces are (face #: corner #s):
        {
            0: 0, 1, 2, 3;
            1: 0, 1, 4, 5;
            2: 0, 2, 4, 6;
            3: 1, 3, 5, 7;
            4: 4, 5, 6, 7;
            5: 2, 3, 6, 7
        }
        
        """
        p0 = self.bounds[0]
        p1 = self.bounds[1]
        p2 = self.bounds[2]
        p3 = self.bounds[3]
        p4 = self.bounds[4]
        p5 = self.bounds[5]
        p7 = self.bounds[7]
                
        walls = []
        walls.append(Plane(p0, p1, p2)) #0
        walls.append(Plane(p0, p1, p4)) #1
        walls.append(Plane(p0, p2, p4)) #2
        walls.append(Plane(p1, p3, p5)) #3
        walls.append(Plane(p4, p5, p7)) #4
        walls.append(Plane(p2, p3, p7)) #5
        
        #Offset walls by buffer
        for wall in walls:
            wall.add_offset(wall.get_unit_normal()*buffer)
            wall.expand(buffer)
                        
        return walls

    def check_if_intersects(self, path, buffer = 1.0):
        """
        Check if path intersects any of the faces of this Cuboid.
        Params:
            path: Path object to check. 
            buffer: drone buffer to be added to the size of the object to check
                that path clears Cuboid safely. Added linearly to all
                dimensions of cuebeoid. Specified in meters. Defaults to 1.0.
        Returns: boolean. True if line segment between point1 and point2
            intersects within buffer distance of this Cuboid.
        """
        for plane in self.get_walls(buffer):
            if plane.check_if_intersects(path):
                return True
        return False

    def check_if_contains_point(self, location, buffer = 1.0):
        """
        Check if location is within this Cuboid plus the buffer distance.
        Params:
            location: Pose object to check for containment within this
                Cuboid; only position portion is used for this check.
            buffer: buffer to be added to the size of the object to check
                that location clears Cuboid with buffer margin. Specified in
                meters. Defaults to 1.0.
        Returns: boolean. True if location intersects within buffer distance
            of this Cuboid.
            
        Notes: 
          2------------3
         / |         / |
        0-----------1  |
        |  |        |  |
        |  |        |  |
        |  6--------|--7
        | /         | /
        4-----------5
        
        Faces are (face #: corner #s):
        {
            0: 0, 1, 2, 3;
            1: 0, 1, 4, 5;
            2: 0, 2, 4, 6;
            3: 1, 3, 5, 7;
            4: 4, 5, 6, 7;
            5: 2, 3, 6, 7
        }
        
        The box normals used are u for face 0, v for face 3, and w for face 1. 
        We want to make sure location projections along these directions fall 
        within the box, so location.u within [p0,p2].u, location.v in [p0,p1].v, 
        and location.w in [p0,p4].w.
        """
        assert(isinstance(location, Pose))
        loc = location.location
        assert(not np.any(np.isnan(loc)))
        log = logging.getLogger(__name__)

        #TODO: use buffer
        #get wall normals
        p0 = self.bounds[0]
        p1 = self.bounds[1]
        p2 = self.bounds[2]
        p3 = self.bounds[3]
        p4 = self.bounds[4]
        p5 = self.bounds[5]
        p7 = self.bounds[7]

        #TODO: check signs carefully...
        u0 = p1-p0
        u1 = p2-p0
        u = np.cross(u0, u1) #up out of top of box
        # v0 = p3-p0
        # v1 = p1-p0
        v0 = p7-p5
        v1 = p1-p5
        v = np.cross(v0, v1) #right out of side of box
        # w0 = p3-p0
        # w1 = p2-p0
        w0 = p0-p4
        w1 = p5-p4
        w = np.cross(w0, w1) #straight into screen away from viewer

        #TODO: check signs carefully
        # check location*u is between u*p0 and u*p4
        checkdot = np.dot(loc, u)
        if checkdot < np.dot(u, p4): # bottom
            log.debug("Point containment check failed on plane 4 (bottom).")
            return False
        if checkdot > np.dot(u, p0): # top
            log.debug("Point containment check failed on plane 0 (top).")
            return False

        # check location*v is between v*p0 and v*p1
        checkdot = np.dot(loc, v)
        if checkdot > np.dot(v, p1): # right
            log.debug("Point containment check failed on plane 3 (right).")
            return False
        if checkdot < np.dot(v, p0): # left
            log.debug("Point containment check failed on plane 2 (left).")
            return False

        # check location*w is between w*p0 and w*p2:
        checkdot = np.dot(loc, w)
        if checkdot < np.dot(w, p0): # front
            log.debug("Point containment check failed on plane 1 (front).")
            return False
        if checkdot > np.dot(w, p2): # back
            log.debug("Point containment check failed on plane 5 (back).")
            return False

        return True

        #TODO: implement: https://math.stackexchange.com/questions/1472049/check-if-a-point-is-inside-a-rectangular-shaped-area-3d

    def __str__(self):
        if self.bounds is None:
            return "None"
        return self.bounds.__str__()
        # return "[xmin: {}, xmax: {}, ymin: {}, ymax: {}, zmin: {}, zmax: {}]".format(xmin, xmax, ymin, ymax, zmin, zmax)

    @staticmethod
    def minimum_bounding_rectangle(points):
        """
        Find the smallest bounding rectangle for a set of points.
        Returns a set of points representing the corners of the bounding box.
        
        From https://gis.stackexchange.com/a/169633

        Parameters:
            points: an nx2 matrix of coordinates
        Returns:
            rval: an nx2 matrix of coordinates
        """
        pi2 = np.pi/2.

        # get the convex hull for the points
        hull_points = points[ConvexHull(points).vertices]

        # calculate edge angles
        edges = np.zeros((len(hull_points)-1, 2))
        edges = hull_points[1:] - hull_points[:-1]

        angles = np.zeros((len(edges)))
        angles = np.arctan2(edges[:, 1], edges[:, 0])

        angles = np.abs(np.mod(angles, pi2))
        angles = np.unique(angles)

        # find rotation matrices
        rotations = np.vstack([
            np.cos(angles),
            np.cos(angles-pi2),
            np.cos(angles+pi2),
            np.cos(angles)]).T

        rotations = rotations.reshape((-1, 2, 2))

        # apply rotations to the hull
        rot_points = np.dot(rotations, hull_points.T)

        # find the bounding points
        x_min = np.nanmin(rot_points[:, 0], axis=1)
        x_max = np.nanmax(rot_points[:, 0], axis=1)
        y_min = np.nanmin(rot_points[:, 1], axis=1)
        y_max = np.nanmax(rot_points[:, 1], axis=1)

        # find the box with the best area
        areas = (x_max - x_min) * (y_max - y_min)
        lowest_area_index = np.argmin(areas)

        # return the best box
        x1 = x_max[lowest_area_index]
        x2 = x_min[lowest_area_index]
        y1 = y_max[lowest_area_index]
        y2 = y_min[lowest_area_index]
        r = rotations[lowest_area_index]

        rectangle = np.zeros((4, 2))
        rectangle[0] = np.dot([x1, y2], r)
        rectangle[1] = np.dot([x2, y2], r)
        rectangle[2] = np.dot([x2, y1], r)
        rectangle[3] = np.dot([x1, y1], r)

        return rectangle

    @staticmethod
    def _cross(pt1, pt2):
        return np.cross(pt1, pt2)

    @staticmethod
    def _check_points_distinct(pt1, pt2):
        assert(pt1 is not None and pt2 is not None)
        assert(np.all(pt1 != pt2))
        assert(np.linalg.norm(pt2 - pt1) > NUMERICAL_PRECISION)

class Plane(object):
    def __init__(self, pt0, pt1, pt2):
        """
        Plane object takes three points assumed to be at the corners of a 
        finite plane:
        
           1
          /
         0 ---- 2
        """
        assert(isinstance(pt0, Pose) or (isinstance(pt0, np.ndarray) and len(pt0) == 3))
        assert(isinstance(pt1, Pose) or (isinstance(pt1, np.ndarray) and len(pt1) == 3))
        assert(isinstance(pt2, Pose) or (isinstance(pt2, np.ndarray) and len(pt2) == 3))
        
        if isinstance(pt0, Pose):
            p0 = pt0.location
        else:
            p0 = pt0
        if isinstance(pt1, Pose):
            p1 = pt1.location
        else:
            p1 = pt1
        if isinstance(pt2, Pose):
            p2 = pt2.location
        else:
            p2 = pt2

        self.points = np.array([p0, p1, p2])

    def __getitem__(self, key):
        return self.points[key]

    def __getattr__(self, key):
        if key == 'pt0' or key == '0':
            return self.points[0]
        if key == 'pt1' or key == '1':
            return self.points[1]
        if key == 'pt2' or key == '2':
            return self.points[2]
        else:
            return None
            
    def add_offset(self, offset):
        """
        Displace plane by offset 3-vector or Pose object.
        """
        log = logging.getLogger(__name__)

        if isinstance(offset, Pose):
            self.points + offset.location
        elif isinstance(offset, np.ndarray):
            self.points + offset
        else:
            log.error("Invalid input to add_offset function. Type: {}".format(type(offset)))
            raise TypeError("Invalid input type to add_offset function.")

    def get_unit_normal(self):
        """Returns a unit vector normal to this plane."""
        normal = np.cross(self.points[1]-self.points[0], self.points[2] - self.points[0])
        return (normal.T/np.linalg.norm(normal)).T
        
    def expand(self, buffer):
        """Extends the internal points by buffer along both principal axes."""
        axes = np.array([self.points[1]-self.points[0], self.points[2] - self.points[0]])
        axes = (axes.T/np.linalg.norm(axes, axis=1)).T
        
        self.points[0] = self.points[0] - axes[0]*buffer - axes[1]*buffer
        self.points[1:] = self.points[1] + axes[0]*buffer + axes[1]*buffer
        

    def check_if_intersects(self, path):
        """
        Check if line_segment intersects this plane.
        Params:
            path: Path object to check for intersection with
                this Plane
                
        Note: assumes that internal points are at the corners of a finite plane.
        
        Returns: boolean. True if any line segment in path intersects with this Plane.
        """
        # from: https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
        log = logging.getLogger(__name__)

        for segment in path.get_line_segments():
            det = self._line_segment_determinant(segment)
            if np.abs(det) < NUMERICAL_PRECISION:
                #determinant essentially zero; no unique solution
                #treat line segment contained in wall same as parallel line; allowing point-containment check to determine whether line segment is allowed
                log.debug("Effectively zero determinant: {}".format(det))
                return False
            else:
                p0 = self.points[0]
                p1 = self.points[1]
                p2 = self.points[2]
                cp12 = np.cross(p1 - p0, p2 - p0)
                denominator = np.dot(segment[0] - segment[1], cp12)
                t = np.dot(cp12, segment[0] - p0)/denominator
                if t < 0 or t > 1:
                    log.debug("Intersection not on line segment.")
                    return False
                u = np.dot(np.cross(p2 - p0, segment[0] - segment[1]), segment[0] - p0)/denominator
                if u < 0 or u > 1:
                    log.debug("Intersection not within paralellogram.")
                    return False
                v = np.dot(np.cross(segment[0] - segment[1], p1 - p0), segment[0] - p0)/denominator
                if v < 0 or v > 1:
                    log.debug("Intersection not within paralellogram.")
                    return False
        return True

    def _line_segment_determinant(self, lineseg):
        if(isinstance(lineseg[0], Pose)):
            return np.dot(lineseg[0].location - lineseg[1].location, np.cross(self.points[1]-self.points[0], self.points[2]-self.points[0]))
        else:
            return np.dot(lineseg[0] - lineseg[1], np.cross(self.points[1]-self.points[0], self.points[2]-self.points[0]))

class Path(object):
    def __init__(self, path=None, points=None, JSON=None):
        self.points = []
        if isinstance(path, db.PathSQL):
            self.name = path.name
            self.points = [pose for pose in path.pts.pose_ndarray]
        elif isinstance(path, Path):
            self.name = path.name
            self.points = path.points
        elif points is not None:
            assert(not isinstance(points, str) )
            for p in points:
                assert(isinstance(p, Pose))
            assert(len(points) >= 2)
            
            self.points = [p for p in points]
        elif isinstance(JSON, str):
            self.from_json(JSON)
        elif isinstance(JSON, dict):
            self.from_dict(JSON)
        else:
            raise TypeError("Invalid input for constructing Path.")
                
                
    def from_dict(self, json_dict):
        log = logging.getLogger(__name__)

        try:
            log.debug("Constructing Path from json dict: {}".format(json_dict))
            self.name = json_dict['name']
            for pt in json_dict["points"]:
                log.debug("Adding point to Path from dict: {}".format(pt))
                self.points.append(Pose(JSON = pt))

        except ValueError as e:
            log.error("Error loading Pose from dict")
            log.error(e)
            raise e
        
    def from_json(self, JSON):
        log = logging.getLogger(__name__)

        try:
            log.debug("Constructing Path from JSON: {}".format(JSON))
            path = json.loads(JSON)
            self.from_dict(path)

        except Exception as e:
            log.error("Error loading Pose from JSON object: {}".format(JSON))
            log.error(e)
            raise e
            
    def to_dict(self):
        """Export dict representation of path."""
        path_dict = {"name": self.name,
                     "points": [p.to_dict() for p in self.points]}
        return path_dict
        
    def to_json(self, pretty=False):
        """Export JSON string representation of path."""
        if pretty:
            json_out = json.dumps(self.to_dict(), indent=2)
        else:
            json_out = json.dumps(self.to_dict())
        return json_out
        
    def add_waypoint(self, end):
        """Add a point to the end"""
        assert(isinstance(end, Pose))
        
        self.points.append(end)

    def get_line_segments(self):
        if len(self.points) < 2:
            yield self.points[-1]
        else:
            i = 1
            while i < len(self.points):
                yield (self.points[i-1], self.points[i])
                i += 1
                
    def distance_to_ends(self, location):
        """ Get distance between ends of path and location as an numpy ndarray. """
        dist = None
        if isinstance(location, Pose):
            return self._get_distance_to_end(self, location.location)
            # dist = np.array([np.linalg.norm(location.ndarray[:3]-self.start.ndarray[:3]), 
            #                  np.linalg.norm(location.ndarray[:3]-self.end.ndarray[:3])])
        elif isinstance(location, np.ndarray):
            return self._get_distance_to_end((self, location))
            # dist = np.array([np.linalg.norm(location[:3]-self.start.ndarray[:3]), 
            #                  np.linalg.norm(location[:3]-self.end.ndarray[:3])])
        # return dist
    def does_intersect(path, safety_radius):
        """Checks for path intersection within safety_radius."""
        #TODO: Fix this. Right now this is a stub which mostly returns False
        #TODO: Here's the solution: https://math.stackexchange.com/a/2416582
        # Basically: find the distance of closest approach, if it's lesss than
        # 2*safety_radius, find the point on each line segment where closest
        # aproach occurs, and see if it is within the bounds of the line segment.
        for segment in path.get_line_segments():
            for seg in self.get_line_segments():
                if segment[0] == seg[0] or segment[0] == seg[1] or segment[1] == seg[0] or segment[1] == seg[1]:
                    return True
        
        return False
            
    def __getattr__(self, key):
        if key == 'start':
            return self.points[0]
        if key == 'end':
            return self.points[-1]
        else:
            return None
        
    @classmethod
    def _get_distance_to_end(cls, path, location):
        """ Get distance from location to start and end points of path. """
        return np.array([np.linalg.norm(location[:3]-path.start.ndarray[:3]), 
                             np.linalg.norm(location[:3]-path.end.ndarray[:3])])
    # TODO: vectorize
    # _vec_get_distance_to_end = np.vectorize(_get_distance_to_end, excluded='location')
    
    @classmethod
    def get_distance_to_end(cls, paths, location):
        #TODO: ensure paths is np.ndarray. Make sure location is np.ndarray
        # return cls._vec_get_distance_to_end(paths, location)
        return np.array([p._get_distance_to_end(p, location) for p in  paths])

class Pose(object):
    """ Pose contains position and and orientation (as a quaternion). """

    def __init__(self, pose=None, array=None, JSON=None):
        self.pose = np.full(7,np.nan)
        if isinstance(pose, db.PoseSQL) or isinstance(pose, Pose):
            self.pose = pose.ndarray
        elif isinstance(array, np.ndarray):
            self.from_ndarray(array)
        elif isinstance(JSON, str):
            self.from_json(JSON)
        elif isinstance(JSON, dict):
            self.from_dict(JSON)
        
    def from_ndarray(self, array):
        if not isinstance(array, np.ndarray):
            raise ValueError("Expected numpy.ndarray, got {}".format(type(array)))
        if len(array) != 7:
            raise ValueError("Expected array of length 7, got length {}".format(len(array)))
            
        self.pose = array
        
    def from_dict(self, json_dict):
        log = logging.getLogger(__name__)
        try:
            log.debug("Creating Pose from json dict: {}".format(json_dict))
            #Must have at least a full position or a full orientation
            assert(('x' in json_dict and 'y' in json_dict and 'z' in json_dict) or 
                    ('q1' in json_dict and 'q2' in json_dict and 'q3' in json_dict and 'q4' in json_dict))
            
            if 'x' in json_dict: #position
                self.pose[0] = json_dict['x']
                self.pose[1] = json_dict['y']
                self.pose[2] = json_dict['z']
            if 'q1' in json_dict: #orientation
                self.pose[3] = json_dict['q1']
                self.pose[4] = json_dict['q2']
                self.pose[5] = json_dict['q3']
                self.pose[6] = json_dict['q4']
            
        except Exception as e:
            log.error("Error loading Pose from JSON dict: {}".format(json_dict))
            log.error(e)
            raise e    
            
    def from_json(self, JSON):
        log = logging.getLogger(__name__)
        try:
            pose = json.loads(JSON)
            log.debug("Creating Pose from JSON: {}".format(JSON))
            self.from_dict(pose)
            
        except Exception as e:
            log.error("Error loading Pose from JSON object: {}".format(JSON))
            log.error(e)
            raise e

    def to_dict(self):        
        """Export dict representation of Pose."""
        pose_dict = {}
        if not np.isnan(self.pose[0]):
            pose_dict['x'] = self.pose[0]
        if not np.isnan(self.pose[1]):
            pose_dict['y'] = self.pose[1]
        if not np.isnan(self.pose[2]):
            pose_dict['z'] = self.pose[2]
        if not np.isnan(self.pose[3]):
            pose_dict['q1'] = self.pose[3]
        if not np.isnan(self.pose[4]):
            pose_dict['q2'] = self.pose[4]
        if not np.isnan(self.pose[5]):
            pose_dict['q3'] = self.pose[5]
        if not np.isnan(self.pose[6]):
            pose_dict['q4'] = self.pose[6]
            
        return pose_dict

    def to_json(self, pretty=False):
        """Export JSON string representation of path."""
        if pretty:
            json_out = json.dumps(self.to_dict(), indent=2)
        else:
            json_out = json.dumps(self.to_dict())
        return json_out
            
    def to_ndarray(self):
        return self.pose
        
    def get_location(self):
        return self.pose[:3]
    
    def get_orientation(self):
        return self.pose[3:]

    def __str__(self):
        return "[({self.x}, {self.y}, {self.z}), ({self.q1}, {self.q2}, {self.q3}, {self.q4})]".format(self=self)

    def __getattr__(self, key):
        if key == 'x' or key == '0':
            return self.pose[0]
        if key == 'y' or key == '1':
            return self.pose[1]
        if key == 'z' or key == '2':
            return self.pose[2]
        if key == 'q1' or key == 'qx':
            return self.pose[3]
        if key == 'q2' or key == 'qx':
            return self.pose[4]
        if key == 'q3' or key == 'qy':
            return self.pose[5]
        if key == 'q4' or key == 'w':
            return self.pose[6]
        if key == 'ndarray':
            return self.to_ndarray()
        if key == 'location':
            return self.get_location()
        if key == 'orientation':
            return self.get_orientation()
        if key == 'json':
            return self.to_json()
        else:
            return None

# 
#     def __eq__(self, other):
#         return isinstance(other, Location) and self.x == other.x and self.y == other.y and self.x == other.z
#     def __lt__(self, other):
#         if not isinstance(other, Location):
#             raise TypeError("Comparing with non-Location type.")
#         return self.x < other.x and self.y < other.y and self.z < other.z
# 
#     def __gt__(self, other):
#         if not isinstance(other, Location):
#             raise TypeError("Comparing with non-Location type.")
#         return self.x > other.x and self.y > other.y and self.z > other.z
# 
#     def __le__(self, other):
#         return self < other or self == other
# 
#     def __ge__(self, other):
#         return self > other or self == other


