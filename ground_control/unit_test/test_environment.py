# -*- coding: utf-8 -*-

"""
Created on Fri, 06 Dec, 2019

@author: @jkp

Unit tests for environment module.
"""

import unittest

import numpy as np
from numpy.testing import assert_array_equal

from environment import Environment

class TestEnvironment(unittest.TestCase):
    def make_room_boundaries(self):
        #make a test room
        return Environment(-2.0, 2.0, -3.0, 3.0, 0.0, 2.0)

    def setup_simple_room(self):
        #make room with some objects
        room = self.make_room_boundaries()
        room.drone_clearance(1.0) #1.0 m radius around drone
        obs_mins = np.array([-2.0,-1.5, 0.0])
        obs_maxes = np.array([-1.5, -1.0, 1.5])
        room.add_obstacle([obs_mins, obs_maxes])
        obs_mins = np.array([0.2,0.89, 0.5])
        obs_maxes = np.array([1.3, 1.33, 1.7])
        room.add_obstacle([obs_mins, obs_maxes])

        return room

    def test_create_room(self):
        room = self.setup_simple_room()
        self.assertIsNotNone(room.boundary)
