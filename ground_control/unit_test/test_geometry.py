
"""
Created on Tue, 24 Dec, 2019

@author: @jkp

Unit tests for geometry module.
"""
import unittest

import logging
import json

import numpy as np
from numpy.testing import assert_array_equal

from geometry import Pose
from geometry import Path
from geometry import Plane
from geometry import Cuboid


class TestPose(unittest.TestCase):
    def create_poses(self):
        self.L1 = Pose(JSON=json.dumps({"x": 0.0, "y": 1.0, "z": 2.0}))
        self.L2 = Pose(JSON=json.dumps({"x": -5.3, "y": -0.2, "z": 13.75}))
        self.L3 = Pose(JSON=json.dumps({"x": 1.4, "y": -7.2, "z": -1.0}))
    
    #test default pose creation
    def test_default_pose_init(self):
        pose = Pose()
        self.assertIsNotNone(pose)
        assert_array_equal(pose.pose, np.full(7, np.nan))
        
        
    #test create pose from numpy pose_array
    def test_create_pose_from_ndarray(self):
        ndarr = np.arange(7)*1.0
        pose = Pose(array=ndarr)
        
        self.assertIsNotNone(pose)
        
        assert_array_equal(ndarr, pose.pose)
        assert_array_equal(ndarr[0:3], pose.get_location())
        assert_array_equal(ndarr[3:], pose.get_orientation())
        
    def test_create_pose_from_json(self):
        #only  position
        pose = Pose(JSON=json.dumps({"x": 0.0, "y": 1.0, "z": 2.0}))
        self.assertIsNotNone(pose)
        ndarr = np.zeros(7)
        ndarr[1] = 1.0
        ndarr[2] = 2.0
        # assert_array_equal(ndarr, pose.pose)
        assert_array_equal(np.arange(3), pose.get_location())
        
        pose2 = Pose(JSON=json.dumps({"x": 0.0, "y": 1.0, "z": 2.0, "q1": 0.1, 
                                    "q2": 0.2, "q3": 0.3, "q4": 0.4}))
        self.assertIsNotNone(pose)
        a = np.arange(3)
        b = np.array([0.1,0.2,0.3,0.4])
        a = np.append(a,b)
        assert_array_equal(a, pose2.pose)

        self.create_poses()
        
        self.assertIsNotNone(self.L1)
        self.assertIsNotNone(self.L2)
        self.assertIsNotNone(self.L3)
        
        assert_array_equal(self.L1.get_location(), np.array([0.0, 1.0, 2.0]))
        assert_array_equal(self.L2.get_location(), np.array([-5.3, -0.2, 13.75]))
        assert_array_equal(self.L3.get_location(), np.array([1.4, -7.2, -1.0]))
        
    def test_create_pose_from_invalid_json(self):
        with self.assertRaises(ValueError):
            pose = Pose(JSON="{x:y, 'blah'}")
        

    #test invalid creation inputs
    #test accessors
    #test non-extant accessors
    #test comparators 
    #test rigid rotation
    
class TestPath(unittest.TestCase):
    
    def create_path_from_JSON(self):
        j = {"lab_id": 1, "points": [{"y": 1.0, "x": 2.0, "z": 2.0, "id:": 1}, {"y": 5.0, "x": 1.0, "z": 9.0, "id:": 2}, {"y": 9.0, "x": 9.0, "z": 9.0, "id:": 3}], "id": 1, "name": "new_path"}
        return Path(JSON=json.dumps(j))
    

    def test_create_path_with_invalid_points_inputs(self):
        with self.assertRaises(AssertionError):
            Path(points="None")
    
    def test_create_path_with_invalid_json(self):
        with self.assertRaises(ValueError):
            p = Path(JSON='string')
        
    
    def test_create_path_with_json(self):
        path = self.create_path_from_JSON()
        
        self.assertIsNotNone(path)
        p1 = path.start
        p2 = path.end
        
        self.assertEqual(p1.x, 2.0)
        self.assertEqual(p1.y, 1.0)
        self.assertEqual(p1.z, 2.0)
        assert_array_equal(p2.get_location(), np.ones(3)*9.0)
            
    def test_create_path_with_invalid_points_inputs(self):
        p1 = Pose()
        with self.assertRaises(AssertionError):
            path = Path(points=[p1])

    def test_create_path_two_poses(self):
        p1 = Pose()
        p2 = Pose(array=np.ones(7))
        
        self.assertIsNotNone(p1)
        self.assertIsNotNone(p2)
        
        path = Path(points=[p1, p2])
        
        self.assertIsNotNone(path)
        self.assertIsNotNone(path.points)
        self.assertTrue(isinstance(path.points, list))
        self.assertEqual(2, len(path.points))
        print(path.points)

    def test_distance_to_ends_function(self):
        path = self.create_path_from_JSON()
        
        #start: {"y": 1.0, "x": 2.0, "z": 2.0}
        #end: {"y": 9.0, "x": 9.0, "z": 9.0}
        
        dist = path.distance_to_ends(np.zeros(3))
        
        self.assertIsNotNone(dist)
        self.assertEqual(type(dist), tuple)
        self.assertEqual(2, len(dist))
        
        self.assertEqual(3.0, dist[0])
        self.assertEqual(np.around(np.sqrt(3)*9.0,decimals=7), np.around(dist[1],decimals=7))
        
    #test invalid creation inputs
    #test accessors
    #test invalid accessors
    #test line segment intersection with plane


class TestCubeoid(unittest.TestCase):
    #test create Cubeoid
    def construct_cuboid_from_JSON(self):
        json_dict = {"centroid_x": 1.5,
                     "centroid_y": 1.5,
                     "centroid_z": 1.5,
                     "length_x": 1,
                     "length_y": 1,
                     "length_z": 3,
                     "rot_z": 0.0
                     }
                     
        return Cuboid(JSON=json.dumps(json_dict))
    
    def test_create_cuboid_from_JSON(self):
        cuboid = self.construct_cuboid_from_JSON()
        
        self.assertIsNotNone(cuboid)
        self.assertIsNotNone(cuboid.bounds)
        self.assertEqual(8, len(cuboid.bounds))
        
        assert_array_equal(np.array([1.0,1.0,3.0]), cuboid.bounds[0])
        assert_array_equal(np.array([2.0,1.0,3.0]), cuboid.bounds[1])
        assert_array_equal(np.array([1.0,2.0,3.0]), cuboid.bounds[2])
        assert_array_equal(np.array([2.0,2.0,3.0]), cuboid.bounds[3])
        assert_array_equal(np.array([1.0,1.0,0.0]), cuboid.bounds[4])
        assert_array_equal(np.array([2.0,2.0,0.0]), cuboid.bounds[7])

    
    # def test_create_cuboid_from_points(self):
    #     min = np.zeros(3)
    #     max = np.ones(3)
    #     unitCube = Cuboid([min, max, [min[0], min[1], max[2]], [min[0], max[1], min[2]],
    #                         [max[0], min[1], mmin[3]], [min)
    #     self.assertIsNotNone(unitCube)
    #     self.assertIsNotNone(unitCube.bounds)
    #     assert_array_equal(unitCube.offset, np.zeros(3))
        
    def test_create_cuboid_from_another_cuboid(self):
        cuboid1 = self.construct_cuboid_from_JSON()
        cuboid2 = Cuboid(cuboid=cuboid1)
        
        self.assertIsNotNone(cuboid1)
        self.assertIsNotNone(cuboid2)
        assert_array_equal(cuboid1.bounds, cuboid2.bounds)
        
    #test get Cubeoid position

    #test Cubeoid bounds
    
    #test Cubeoid buffer zone

    #test change Cubeoid position

    #test change Cubeoid position changes Cubeoid bounds
    
# class TestPlane(unittest.TestCase):
    #test create plane
    # def test_create_plane(self):
        # p1 = Location()
        # p2 = Location(1., 1., 1.)
        # p3 = Location(2., 2., 2.)
        # plane = Plane(p1, p2, p3)
        # 
        # self.assertIsNotNone(plane)
        
    #test invalid creation inputs
    #test accessors
    #test invalid accessors
    #test line segment intersection with plane
