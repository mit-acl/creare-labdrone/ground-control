import logging
import sys
import re
import enum
import json
import math

import numpy as np
import sqlalchemy as sqla
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.hybrid import hybrid_method
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.mysql import DOUBLE
from sqlalchemy.dialects.mysql import ENUM
from sqlalchemy.types import TypeDecorator
from sqlalchemy.types import Integer
from sqlalchemy.types import Float

import tf

DATABASE = 'labdrone'

log = logging.getLogger(__name__)
log.setLevel(30)

if not len(log.handlers):
#    print("Loading logging basic config...")
    logging.basicConfig(stream=sys.stdout, level=30)


class NumpyFloat(TypeDecorator):
    """Adds numpy float support."""

    impl = Float

    def __init__(self, *args, **kwargs):
        super(NumpyFloat, self).__init__(*args, **kwargs)

    def load_dialect_impl(self, dialect):
        if dialect.name == 'mysql':
            return dialect.type_descriptor(DOUBLE())
        else:
            return Float()

    def process_bind_param(self, value, dialect):
        if isinstance(value, np.float64) or isinstance(value, np.float32):
            value = float(value)
        return value
        
    def process_result_value(self, value, dialect):
        return np.float64(value)
        
class IntEnumSQL(TypeDecorator):
    """
    Enables passing in a Python enum and storing the enum's *value* in the db.
    The default stores the enum's *name* (ie the string).
    Modified (fixed) from: https://www.michaelcho.me/article/using-python-enums-in-sqlalchemy-models on 2020-02-26
    """
    impl = Integer

    def __init__(self, enumtype, *args, **kwargs):
        super(IntEnumSQL, self).__init__(*args, **kwargs)
        self._enumtype = enumtype

    def process_bind_param(self, value, dialect):
#        print("Processing IntEnumSQL input: {}".format(value))
        if isinstance(value, enum.IntEnum):
            return value.value
        else:
            return value

    def process_result_value(self, value, dialect):
#        print("Returning IntEnumSQL value{}: {}".format(value, self._enumtype(value)))
        return self._enumtype(value)

#Set up database connection
engine = sqla.create_engine('mysql+mysqlconnector://root:{database}@localhost'.format(database=DATABASE))
engine.execute("CREATE DATABASE IF NOT EXISTS {database}".format(database=DATABASE))
engine.execute("USE {database}".format(database=DATABASE))
engine.echo = True

class Base(object):
    @declared_attr
    def __tablename__(cls):
        #camelCase to snake_case, and removing the SQL at the end of class names
        return re.sub(r'(?<!^)(?=[A-Z])', '_', cls.__name__.split('SQL')[0]).lower()

    # __table_args__ = {'mysql_engine': 'InnoDB'} #seems InnoDB is the default?
    metadata=sqla.MetaData(bind=engine)

    id =  sqla.Column(sqla.Integer, primary_key = True, autoincrement = True)

    @hybrid_property
    def dict(self):
        """Default dict gives values of all columns."""
        self_dict = {}
        for k in self.__table__.columns.keys():
            self_dict[k] = getattr(self, k)
            
        return self_dict

    @hybrid_property
    def json(self):
        return json.dumps(self.dict)

    @hybrid_method
    def to_json_pretty(self, indent=2):
        return json.dumps(self.dict, indent=indent)
        
#Project.__table__.columns.keys() --- for getting the column names to include in a base dict property
# SQLA base class for this database; used in all tables below
Base = declarative_base(cls=Base)

#### SQL Class wrappers

class LabModelSQL(Base):
    """
    Highest logical level of organization. A lab model owns a set of exclusions, poses of interest, paths, etc... 
    """
    fk_bounds = sqla.Column(sqla.Integer, sqla.ForeignKey('cuboids.id'))
    
    name = sqla.Column(sqla.String(64))

    cuboid = sqla.orm.relationship("CuboidsSQL", uselist=False, back_populates="lab_model")
    exclusions = sqla.orm.relationship("ExclusionSQL", lazy="joined")
    paths = sqla.orm.relationship("PathSQL", lazy="joined")
    endpoints = sqla.orm.relationship("EndpointSQL", lazy="joined")
    
    #Explicitly declaring this as python code
    @hybrid_property
    def bounds(self):
        return self.cuboid
    
    @hybrid_property
    def dict(self):
        return {"id": self.id, "name": self.name, 
                "bounds": self.bounds.dict, 
                "exclusions":[ex.dict for ex in self.exclusions],
                "paths": [p.dict for p in self.paths],
                "endpoints": [end.dict for end in self.endpoints]}
                
    @classmethod
    def from_dict(cls, lab_model_dict):
        """ Create LabModelSQL object from LabModel dict. """
        if lab_model_dict is None or not isinstance(lab_model_dict, dict):
            raise Exception("Input lab_model_dict not valid type: ".format(type(lab_model_dict)))
            
        if("name" not in lab_model_dict or "bounds" not in lab_model_dict or 
            "exclusions" not in lab_model_dict or "paths" not in lab_model_dict or 
            "endpoints" not in lab_model_dict):
            
            raise Exception("Missing valid inputs in LabModel dictionary input: {}".format(lab_model_dict.keys()))
            
        bounds_dict = lab_model_dict['bounds']
        bounds = CuboidsSQL.from_dict(bounds_dict)
        lab_model = cls(name = lab_model_dict['name'], cuboid = bounds)
        
        for ex in lab_model_dict["exclusions"]:
            lab_model.exclusions.append(ExclusionSQL(cuboid = CuboidsSQL.from_dict(ex)))
        
        for path in lab_model_dict["paths"]:
            lab_model.paths.append(PathSQL.from_dict(path))
        
        for endpoint in lab_model_dict["endpoints"]:
            lab_model.endpoints.append(EndpointSQL.from_dict(endpoint))
            
        return lab_model
            
    @classmethod
    def from_json(cls, lab_model_json):
        try:
            log.debug("Creating LabModelSQL from JSON string: {}".format(lab_model_json))
            return cls.from_dict(json.loads(lab_model_json))
        except Exception as e:
            log.error("Error while trying to create LabModelSQL from JSON: {}".format(e))
            raise e
        
         
class CuboidsSQL(Base):
    """
    Primitive descriptor for objects in lab model.
    """
    centroid_x = sqla.Column(NumpyFloat)
    centroid_y = sqla.Column(NumpyFloat)
    centroid_z = sqla.Column(NumpyFloat)
    theta_z = sqla.Column(NumpyFloat)
    length_x = sqla.Column(NumpyFloat)
    length_y = sqla.Column(NumpyFloat)
    length_z = sqla.Column(NumpyFloat)
    
    lab_model = sqla.orm.relationship("LabModelSQL", back_populates="cuboid")
    exclusions = sqla.orm.relationship("ExclusionSQL", back_populates="cuboid")
    
    @hybrid_property
    def dict(self):
        return {"id": self.id, 
                "centroid":{
                    "x": self.centroid_x, 
                    "y": self.centroid_y, 
                    "z": self.centroid_z},
                "theta_z": self.theta_z,
                "length":{
                    "x": self.length_x, 
                    "y": self.length_y, 
                    "z": self.length_z}
                }
                
    @classmethod
    def from_dict(cls, cuboid_dict):
        if not isinstance(cuboid_dict, dict):
            raise Exception("Input cuboid_dict not valid type: ".format(type(cuboid_dict)))
        
        cuboid = cls(centroid_x = cuboid_dict['centroid']['x'],
                     centroid_y = cuboid_dict['centroid']['y'],
                     centroid_z = cuboid_dict['centroid']['z'],
                     theta_z = cuboid_dict['theta_z'],
                     length_x = cuboid_dict['length']['x'],
                     length_y = cuboid_dict['length']['y'],
                     length_z = cuboid_dict['length']['z'])
                        
        return cuboid
        
    @classmethod
    def from_json(cls, cuboid_json):
        try:
            log.debug("Creating CuboidsSQL from JSON string: {}".format(cuboid_json))
            return cls.from_dict(json.loads(cuboid_json))
        except Exception as e:
            log.error("Error while trying to create CuboidsSQL from JSON: {}".format(e))
            raise e
    

class ExclusionSQL(Base):
    fk_lab_model = sqla.Column(sqla.Integer, sqla.ForeignKey('lab_model.id'))
    fk_cuboids = sqla.Column(sqla.Integer, sqla.ForeignKey('cuboids.id'))
    # fk_exclusion_type = sqla.Column(sqla.Integer, sqla.ForeignKey('exclusion_type.id'))
    
    
    #TODO look up 1-1 syntax.
    cuboid = sqla.orm.relationship("CuboidsSQL", uselist=False, back_populates="exclusions", lazy="joined", innerjoin=True)
    lab_model = sqla.orm.relationship("LabModelSQL", uselist=False, back_populates="exclusions", lazy="joined")

    @hybrid_property
    def dict(self):
    	output = self.cuboid.dict
    	output["lab_id"] = self.fk_lab_model
    	output["exclusion_id"] = self.id
    	return output

class PathSQL(Base):
    """
    Defined paths.
    """
    fk_lab_model = sqla.Column(sqla.Integer, sqla.ForeignKey('lab_model.id'))
    
    num_pts = sqla.Column(sqla.Integer)
    name = sqla.Column(sqla.String(64))
    
    pts = sqla.orm.relationship("PathPoseSQL", lazy="joined", innerjoin=True)
    
    @hybrid_property
    def pose_array(self):
        sorted_pts = sorted(self.pts, key=lambda x: x.path_index, reverse=False)
        return [pt.pose for pt in sorted_pts]
        
    @hybrid_property
    def pose_ndarray(self):
        return [pos.ndarray for pos in self.pose_array]

    @hybrid_property
    def start_id(self):
        sorted_pts = sorted(self.pts, key=lambda x: x.path_index, reverse=False)
        return sorted_pts[0].pose.id

    @hybrid_property
    def end_id(self):
        sorted_pts = sorted(self.pts, key=lambda x: x.path_index, reverse=True)
        return sorted_pts[0].pose.id

    
    @hybrid_property
    def dict(self):
        return {"id": self.id, "lab_id": self.fk_lab_model, "name": self.name, "points" :[p.dict for p in self.pose_array], "start_id" : self.start_id, "end_id" : self.end_id}

    @classmethod
    def from_dict(cls, path_dict):
        """ Create PathSQL object from Path dict. """
        if path_dict is None or not isinstance(path_dict, dict):
            raise Exception("Input path_dict not valid type: ".format(type(path_dict)))

        points = []
        index = 0
        for pose in path_dict['points']:
            points.append(PathPoseSQL(path_index = index, pose= PoseSQL.from_dict(pose)))
            index = index + 1
            
        path = cls(name = path_dict['name'],
                   num_pts = len(path_dict['points']),
                   pts = points)
        
        return path
        
    @classmethod
    def from_json(cls, path_json):
        try:
            log.debug("Creating PathSQL from JSON string: {}".format(path_json))
            return cls.from_dict(json.loads(path_json))
        except Exception as e:
            log.error("Error while trying to create PathSQL from JSON: {}".format(e))
            raise e

class PathPoseSQL(Base):
    """
    Components of path. Path index specifies order in path definition.
    """
    fk_path = sqla.Column(sqla.Integer, sqla.ForeignKey('path.id'))
    #Not sure what the value-add for storing null quaternion data is unless we plan to record full pose when defining paths.
    fk_pose = sqla.Column(sqla.Integer, sqla.ForeignKey('pose.id'))
    
    path_index = sqla.Column(sqla.Integer)
#    pose = sqla.orm.relationship("PoseSQL", uselist=False, back_populates="path_pose", lazy="joined", innerjoin=True)
    pose = sqla.orm.relationship("PoseSQL", foreign_keys=[fk_pose])

class PoseSQL(Base):
    x = sqla.Column(NumpyFloat)
    y = sqla.Column(NumpyFloat)
    z = sqla.Column(NumpyFloat)
    q1 = sqla.Column(NumpyFloat)
    q2 = sqla.Column(NumpyFloat)
    q3 = sqla.Column(NumpyFloat)
    q4 = sqla.Column(NumpyFloat)
    
    #Not every pose <--> path pose. Does this make sense?
#    path_pose = sqla.orm.relationship("PathPoseSQL", back_populates="pose")
#    endpoint = sqla.orm.relationship("EndpointSQL", back_populates="pose")
#    wait_at_pose = sqla.orm.relationship("EndpointSQL", back_populates="wait_at")

    @hybrid_property
    def ndarray(self):
        return np.array([self.x, self.y, self.z, self.q1, self.q2, self.q3, self.q4])

    @hybrid_property
    def dict(self):
        pose_dict = {"id": self.id, "x":self.x, "y": self.y, "z": self.z}
        if self.q1 is not None and self.q2 is not None and self.q3 is not None and self.q4 is not None:
            if np.isfinite(self.q1) and np.isfinite(self.q2) and np.isfinite(self.q3) and np.isfinite(self.q4):
                pose_dict["q1"] = self.q1
                pose_dict["q2"] = self.q2
                pose_dict["q3"] = self.q3
                pose_dict["q4"] = self.q4
            else:
                #verbose logging
                log.log(1,"Non-finite quaternion in Pose. Skipping in JSON representation.")
        
        return pose_dict
    
    @classmethod
    def from_dict(cls, pose_dict):
        """ Create PoseSQL object from Pose dict. """
        if pose_dict is None or not isinstance(pose_dict, dict):
            raise Exception("Input pose_dict not valid type: ".format(type(pose_dict)))

        try:
            pose = cls(x = pose_dict['x'],
                       y = pose_dict['y'],
                       z = pose_dict['z'])
                       
            if "q1" in pose_dict and "q2" in pose_dict and "q3" in pose_dict and "q4" in pose_dict:
                if np.isfinite(pose_dict["q1"]) and np.isfinite(pose_dict["q2"]) and np.isfinite(pose_dict["q3"]) and np.isfinite(pose_dict["q4"]):
                    pose.q1 = pose_dict['q1']
                    pose.q2 = pose_dict['q2']
                    pose.q3 = pose_dict['q3']
                    pose.q4 = pose_dict['q4']

            return pose
        except Exception as e:
            log.error("Error creating PoseSQL from dict: {}.".format(e))
            
        return None
        
    @classmethod
    def from_json(cls, pose_json):
        try:
            log.debug("Creating PoseSQL from JSON string: {}".format(pose_json))
            return cls.from_dict(json.loads(pose_json))
        except Exception as e:
            log.error("Error while trying to create PoseSQL from JSON: {}".format(e))
            raise e

class EndpointTypes(enum.IntEnum):
    """
    Used in Environment specification to specify an endpoint type.
    """
    docking_station = 0 #home sweet home
    tray_slot = 1 # in a vertical rack
    tray_bed = 2 #on top of a machine
    
    def to_json(self):
        return json.dumps(self.to_dict())
    
    def to_dict(self):
#        print("JKP: EndpointTypes to_dict.")
        return {"name": self.name, "value": self.value}
    
    @classmethod
    def from_dict(cls, endpoint_type_dict):
        """ Create EndpointTypeSQL object from EndpointType dict. """
        if endpoint_type_dict is None or not isinstance(endpoint_type_dict, dict):
            raise Exception("Input endpoint_type_dict not valid type: ".format(type(endpoint_type_dict)))
    
        try:
            endpoint_type = cls(value = endpoint_type_dict['value'])
    
            return endpoint_type
        except Exception as e:
            log.error("Error creating EndpointTypeSQL from dict: {}.".format(e))
    
        return None
    
    @classmethod
    def from_json(cls, endpoint_type_json):
        try:
            log.debug("Creating EndpointTypeSQL from JSON string: {}".format(endpoint_type_json))
            return cls.from_dict(json.loads(endoint_type_json))
        except Exception as e:
            log.error("Error while trying to create EndpointTypeSQL from JSON: {}".format(e))
            raise e


#association_table = Table('association', Base.metadata,
#    Column('endpoint_id', Integer, ForeignKey('endpoint.id')),
#    Column('pose_id', Integer, ForeignKey('pose.id'))
#)

class EndpointSQL(Base):
    """
    Pose of endpoint, e.g. slots
    """
    fk_lab_model = sqla.Column(sqla.Integer, sqla.ForeignKey('lab_model.id'))
    fk_pose = sqla.Column(sqla.Integer, sqla.ForeignKey('pose.id'))
    fk_wait_at_pose = sqla.Column(sqla.Integer, sqla.ForeignKey('pose.id'))
    endpoint_type = sqla.Column(IntEnumSQL(EndpointTypes), 
                                default=EndpointTypes.docking_station)
    # endpoint_type = sqla.Column('endpoint_type', ENUM(*[name for name, member in EndpointType.__members__.items()]))

    name = sqla.Column(sqla.String(64))
#    pose = sqla.orm.relationship("PoseSQL", uselist=False, 
#                                back_populates="endpoint", lazy="joined", 
#                                innerjoin=True)
#    wait_at = sqla.orm.relationship("PoseSQL",uselist=False, 
#                                back_populates="wait_at_pose", lazy="joined", 
#                                innerjoin=True)
    pose = sqla.orm.relationship("PoseSQL",foreign_keys=[fk_pose])
    wait_at = sqla.orm.relationship("PoseSQL",foreign_keys=[fk_wait_at_pose])
    # endpoint_type = sqla.orm.relationship("EndpointType", uselist=False, back_populates="endpoint")

    @hybrid_property
    def dict(self):
        return {"id": self.id, "name": self.name, 
                "pose": self.pose.dict, 
                "wait_at": self.wait_at.dict,
                "type": json.loads(self.endpoint_type.to_json()),
                "lab_id": self.fk_lab_model}

    @classmethod
    def from_dict(cls, endpoint_dict):
        """ Create EndpointSQL object from Endpoint dict. """
        if endpoint_dict is None or not isinstance(endpoint_dict, dict):
            raise Exception("Input endpoint_dict not valid type: ".format(type(endpoint_dict)))

        endpoint = cls(name = endpoint_dict['name'],
                       endpoint_type = EndpointTypes.from_dict(endpoint_dict['type']),
                       pose = PoseSQL.from_dict(endpoint_dict['pose']),
                       wait_at = PoseSQL.from_dict(endpoint_dict['wait_at']))

        return endpoint
        
    @classmethod
    def from_json(cls, endpoint_json):
        try:
            log.debug("Creating EndpointSQL from JSON string: {}".format(endpoint_json))
            return cls.from_dict(json.loads(endoint_json))
        except Exception as e:
            log.error("Error while trying to create EndpointSQL from JSON: {}".format(e))
            raise e

class Database(object):
    """Database connection."""
        
    def __init__(self):
        log.debug("Creating Database object.")
        self.__session = None
    
    def close(self):
        if self.__session is not None:
            log.debug("Closing database session.")
            self.__session.close()
        self.__session = None
        
    def open(self):
        log.debug("Open database session.")
        if engine is not None:
            log.debug("Engine: {}".format(engine) )
        Session = sessionmaker(bind=engine)
        self.__session = Session()
        if self.__session is None:
            log.error("SESSION NONE in open")
        
    def commit(self):
        self.__session.commit()
        
    def __enter__(self):
        log.debug("Opening database sesssion with __enter__")
        if self.__session is not None:
            log.warn("Trying to create database session when one already exists!")
        else:
            log.debug("Attempting to open database connection...")
            self.open()
            if self.__session is None:
                log.error("SESSION NONE in constructor")
        return self
        
    def __exit__(self, t, value, traceback):
        log.debug("Closing database session with __exit__")
        if t:
            log.error("Error in database connection: {} \n {} \n {}".format(t,value,traceback))
        self.close()
        
    def __getattr__(self, key):
        if key == 'session':
            return self.__session
        
    def coerce_path_ends(self,lab_model_id):
        if isinstance(lab_model_id,int):
            s = self.__session

#            lab_model_id = s.query(LabModelSQL).filter_by(name=lab_model_name).first().id
#            print(lab_model_id)
            endpoint_wait_at_ids = []
            for ep in s.query(EndpointSQL).filter_by(fk_lab_model=lab_model_id):
                endpoint_wait_at_ids.append(ep.fk_wait_at_pose)

            for path in s.query(PathSQL).filter_by(fk_lab_model=lab_model_id):
                num_pts = path.num_pts
                path_id = path.id
                path_start = s.query(PathPoseSQL).filter_by(path_index=0,fk_path=path_id).first()
                path_end = s.query(PathPoseSQL).filter_by(path_index=num_pts-1,fk_path=path_id).first()

                if not path_start.fk_pose in endpoint_wait_at_ids:
                    wait_at_id = self._find_close_wait_at(lab_model_id,path_start.fk_pose)
                    if wait_at_id is not None:
                        path_start.fk_pose = wait_at_id
#                        print("updating start pose with {}".format(wait_at_id))
                else:
                    pass
#                    print("path start pose already wait at")

                if not path_end.fk_pose in endpoint_wait_at_ids:
                    wait_at_id = self._find_close_wait_at(lab_model_id,path_end.fk_pose)
                    if wait_at_id is not None:
                        path_end.fk_pose = wait_at_id
#                        print("updating end pose")
                else:
                    pass
#                    print("path end pose already wait at")

            s.commit()
        else:
            pass
#            print("Couldn't resolve lab_model identifier",lab_model_name)


    def _find_close_wait_at(self,lab_model_id,pose_id, tolerance=0.1):
#        print("finding close wait at poses to reference pose id={} on lab model {}".format(pose_id,lab_model_id))
        dists = []
        wait_at_ids = []
        ref_pose = self.__session.query(PoseSQL).filter_by(id=pose_id).first()
        for endpoint in self.__session.query(EndpointSQL).filter_by(fk_lab_model=lab_model_id):
            ep_pose = self.__session.query(PoseSQL).filter_by(id=endpoint.fk_wait_at_pose).first()
#            print("inspecting endpoint id={} with wait_at id={}".format(endpoint.id,ep_pose.id))
            dist = math.sqrt((ep_pose.x-ref_pose.x)**2 + (ep_pose.y - ref_pose.y)**2 + (ep_pose.z - ref_pose.z)**2 )
            dists.append(dist)
            wait_at_ids.append(endpoint.fk_wait_at_pose)

        wait_at_index = None
        min_dist = min(dists)
        if min_dist <= tolerance:
            #Why yes this could be more concise with a lambda expression or tuple sorting or whatever.
            wait_at_index = wait_at_ids[dists.index(min_dist)]
        else:
            pass
#            print("min distance larger than tolerance: {} > {}".format(min_dist,tolerance))
#        print("close wait at logic returns {}".format(wait_at_index))
        return wait_at_index

    def condense_wait_at(self,lab_model_id,tolerance=0.5):

        close_eps = []
        for i, endpoint_1 in enumerate(self.__session.query(EndpointSQL).filter_by(fk_lab_model=lab_model_id)):
            ep_pose_1 = self.__session.query(PoseSQL).filter_by(id=endpoint_1.fk_wait_at_pose).first()

            for j, endpoint_2 in enumerate(self.__session.query(EndpointSQL).filter_by(fk_lab_model=lab_model_id)):

                if j > i:
                    ep_pose_2 = self.__session.query(PoseSQL).filter_by(id=endpoint_2.fk_wait_at_pose).first()
                    dist = math.sqrt((ep_pose_1.x-ep_pose_2.x)**2 + (ep_pose_1.y - ep_pose_2.y)**2 + (ep_pose_1.z - ep_pose_2.z)**2 )

                    if dist <= tolerance:
                        close_eps.append([ep_pose_1,ep_pose_2])
                    else:
                        pass
                        #print("NOT CLOSE")

        condensed_close_eps = []
        for close_ep_pair in close_eps:
            existing_indices = []
            for i,condensed_set in enumerate(condensed_close_eps):
                if (close_ep_pair[0] in condensed_set) or (close_ep_pair[1] in condensed_set):
                    existing_indices.append(i)
            existing_indices.sort(reverse=True)
            temp = close_ep_pair
            for index in existing_indices:
                temp = temp + condensed_close_eps.pop(index)
            condensed_close_eps.append(list(set(temp)))
        for condensed_set in condensed_close_eps:
#            print("new set")
            x_vals = []
            y_vals = []
            z_vals = []
            yaws = []
            for item in condensed_set:
                #print(item.id)
                x_vals.append(item.x)
                y_vals.append(item.y)
                z_vals.append(item.z)
                q = (item.q1,item.q2,item.q3,item.q4)
                yaws.append(tf.transformations.euler_from_quaternion(q)[2])
            condensed_set[0].x = sum(x_vals)/len(x_vals)
            condensed_set[0].y = sum(y_vals)/len(y_vals)
            condensed_set[0].z = sum(z_vals)/len(z_vals)
            avg_yaw = sum(yaws)/len(yaws)
            avg_quat = tf.transformations.quaternion_from_euler(0,0,avg_yaw)
            condensed_set[0].q1 = avg_quat[0]
            condensed_set[0].q2 = avg_quat[1]
            condensed_set[0].q3 = avg_quat[2]
            condensed_set[0].q4 = avg_quat[3]

            replacement_index = condensed_set.pop(0).id
            discarded_indices = [item.id for item in condensed_set]
            #Search through endpoint and path pose. Update indices to point to condensed_set[0].id
            for endpoint in self.__session.query(EndpointSQL).filter_by(fk_lab_model=lab_model_id):
                if endpoint.fk_wait_at_pose in discarded_indices:
                    endpoint.fk_wait_at_pose = replacement_index
#                    print("REPLACED IN ENDPOINT")
            for path_pose in self.__session.query(PathPoseSQL):
                if path_pose.fk_pose in discarded_indices:
                    path_pose.fk_pose = replacement_index
#                    print("REPLACED IN PATHPOSE")
        self.__session.commit()







def create_tables():
    #Create tables
    log.info("Creating tables...")
    Base.metadata.create_all(bind=engine)

def _delete_tables():
    #Probably shouldn't be using this a lot...mostly for testing
    log.warn("Dropping all tables!")
    Base.metadata.drop_all(bind=engine)


