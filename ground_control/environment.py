# -*- coding: utf-8 -*-


"""
Created on Fri, 06 Dec, 2019

@author: @jkp

Environment module for advanced plate transfer ground controller.
"""
import logging
import json

import numpy as np

from geometry import Cuboid
from geometry import Pose
from geometry import Path
from geometry import NUMERICAL_PRECISION
import database as db
from database import EndpointTypes

class Environment(object):

    def __init__(self, xmin, ymin, zmin, xmax, ymax, zmax, name="Labdrone_Environment"):
        """Initialize environment by defining corners."""
        self.bounds = self.define_room_boundary(xmin, xmax, ymin, ymax, zmin, zmax)
        self.paths = [] #TODO: this probably should be a dictionary of named paths with lists of path segments
        self.paths_np = []
        self.path_indices = []
        self.obstacles = []
        self.endpoints = {}
        self.droneClearance = 0.0
        self.name=name
        
        self._paths_by_endpoint_locations = None #kdtree; to be set up once paths are "finalized", or perhaps on atc instatiation?
        self._endpoints_by_location = None #kdtree; to be set up once environment is "finalized", or perhaps on atc instatiation?
    
    def drone_clearance(self, radius):
        """Define safety volume around drone."""
        self.droneClearance = radius
    
    def define_room_boundary(self, xmin, xmax, ymin, ymax, zmin, zmax):
        """Create cuboid, which uses centroid, rotation, and size."""
        lengths = [xmax-xmin, ymax-ymin, zmax-zmin]
        centroid = [xmin + lengths[0]/2., ymin + lengths[1]/2., zmin + lengths[2]/2.]
        rotation = 0.0 #defaulting to an unrotated environment
        return Cuboid(JSON = json.dumps({"centroid":{'x': centroid[0], 
                                                     "y": centroid[1],
                                                     "z": centroid[2]},
                                         "theta_z": rotation,
                                         "length": {'x': lengths[0],
                                                    "y": lengths[1],
                                                    "z": lengths[2]}
                                        })
                     )
                                 
    def create_obstacle(self, points):
        """Create an obstacle.
        Ensure points can be interpreted as reasonable Cuboid.
        """
        log = logging.getLogger(__name__)
        obstacle = None
        try:
            obstacle = Cuboid(points=points)
        except Exception as e:
            log.error("Error creating Cuboid from points: {}".format(e))
            raise e
        
        return obstacle
        
    def add_obstacle(self, obstacle):
        #TODO: log intersecting obstacles
        #TODO: check for intersection with paths; issue warning? fail?
        if not self.check_obstacle(obstacle):
            raise Exception("Obstacle error.") #TODO: give reason
        self.obstacles.append(obstacle)
        
    def check_obstacle(self, obstacle):
        #TODO: check that obstacle position is "OK". - does not intersect other Obstacles
        if not isinstance(obstacle, Cuboid):
            return False
        #TODO: more logic for checking intersections and collisions
        
        return True
        
    def remove_obstacle(self, obstacle):
        if obstacle is None:
            return -1
        i = 0
        while i < len(self.obstacles):
            if self.obstacles[i] == obstacle:
                break
            else:
                i += 1
            
        del self.obstacles[i]
        #TODO: remove contained endpoints
        
        return i
    
    def create_path(self, points):
        """
        Create a path from a list of points.
        Checks for validity, and may adjust endpoints to be near waypoints.
        """
        log = logging.getLogger(__name__)
        path = None
        try:
            path = Path(points=points)
        except Exception as e:
            print("Error creating Path from points: {}".format(e))
            log.error("Error creating Path from points: {}".format(e))
            raise e
        
        return path
    
    def add_path(self, path):
        """Add a path. If path intersects any boundaries, return -1."""
        if not self._check_path(path):
            raise Exception("Path invalid.") #TODO give reason; intersection?
        else:
            self.paths.append(path)
            
        return True

    def add_path_np(self,path):
        temp = []
        for point_dict in path["points"]:
            temp.append(np.array([point_dict["x"],point_dict["y"],point_dict["z"]]))
        self.paths_np.append(temp)

    def add_path_indices(self,path):
        self.path_indices.append((path["start_id"],path["end_id"],path["id"]))

    
    def remove_path(self, path):
        """Remove a path."""
        if path is None:
            return -2
        else:
            idx = 0
            for p in self.paths:
                if p == path:
                    del path[idx]
                    return idx
                else:
                    idx += 1
            return -1
    
    def add_endpoint(self, endpoint=None, JSON=None, pose=None, type=None):
        """Adds an endpoint with pose. Returns Endpoint."""
        log = logging.getLogger(__name__)
        ep = None
        if isinstance(endpoint, Endpoint):
            ep = endpoint
        elif isinstance(JSON, str) or isinstance(JSON, dict):
            ep = Endpoint(JSON = JSON)
        elif isinstance(pose, Pose) and isinstance(type, EndpointTypes):
            ep = Endpoint(pose = pose, type = type)
        else:
            raise TypeError("Invalid inputs. Endpoint not added.")
        
        #TODO: think about this and implement it; too complicated for initial skeleton framework
        #endpoints must be within existing obstacles
        # obstacle = self._check_location(pose.location)
        # 
        # if not obstacle:
        #     err = "Endpoint must be within an existing obstacle."
        #     log.error(err)
        #     raise Exception(err)
        # if len(obstacle) > 1:
        #     err = "Endpoint contained in overlapping obstacles. Endpoint must be within a single obstacle."
        #     log.debug(err + " Obstacles: {}".format(obstacle))
        #     raise Exception(err)
            
        #TODO: should the endpoint "belong" to an obstacle?
        #TODO: should we check for proximity to other endpoints, depending on type?
        # in case endpoint name exists (editing existing environment for example)
        while ep.name in self.endpoints.keys():
            endpoint.rename()
        
        self.endpoints[ep.name] = ep
        
        return ep
    
    def remove_endpoint(self, endpopint):
        """Removes endpoint with matching name."""
        assert(isinstance(endpoint, Endpoint))

        if endpoint.name not in self.endpoints.keys():
            log.warn("No endpoint named {}.".format(endpoint.name))
            return False
        del self.endpoints[endpoint.name]
        return True            
    
    def get_endpoints_by_location(self, location, endpoint_type = None):
        """Get a list of Endpoints near location."""
        #TODO: get a list of Endpoints within some radius instead
        #to test skeleton connectivity, just select the closest endpoint
        closest = None
        closest_approach = 99999999999.9
        for name, ep in self.endpoints.items():
            if endpoint_type is not None:
                if type(endpoint_type) != EndpointTypes:
                    raise Exception("Erroneous input endpoint_type.")
                if ep.type != endpoint_type:
                    continue;
            dist = ep.distance_to(location)
            if(dist < closest_approach):
                closest_approach = dist
                closest = ep
        return [closest]
        
    def get_endpoint_by_name(self, name):
        """
        Get an endpoint by its name.
        
        Inputs:
        name: str.
            Name of endpoint.
        
        Returns:
        Endpoint with input name, or None.
        """
        if name is None:
            return None
        
        return self.endpoints[name]
    
    def get_paths_by_location(self, start, end):
        """Get a list of paths endpoints near start and end."""
        log = logging.getLogger(__name__)
        #TODO: look up paths by location and get some within a given radius!
        #to test skeleton connectivity, just select the closest path
        if isinstance(start, Pose):
            start_loc = start.location
        else:
            start_loc = start
        if isinstance(end, Pose):
            end_loc = end.location
        else:
            end_loc = end
        start_dists = np.min(Path.get_distance_to_end(self.paths, start_loc), axis=1)
        end_dists = np.min(Path.get_distance_to_end(self.paths, end_loc), axis=1)
        score = np.square(start_dists)  + np.square(end_dists)
        log.debug("Path selection. Path scores: {}".format(score))
        min_index = np.argmin(score)
        log.debug("Path selection. Min score path: {}".format(self.paths[min_index]))
        return self.paths[min_index]
        # closest = None
        # closest_approach = 99999999999.9
        # for path in self.paths:
        #     dists = path.distance_to_ends(location)
        #     if(dists[0] < closest_approach):
        #         closest_approach = dists[0]
        #         closest = path
        #     if(dists[1] < closest_approach):
        #         closest_approach = dists[1]
        #         closest = path
        # return [closest] 
        
        #TODO: add path intersection checking!
        #TODO: fuzzy position checking for finding routes that start or end at a location -- could be facilitated by kdtree
        #TODO: may want to store two kdtrees of end and starting points for path lookup?
        #TODO: allow read-in of some text file to define an environment, or part of environment
        #TODO: All paths will specify orientation, but perhaps only precision maneuvering will use Orientation
        #TODO: need to be able to define locations of trays in a tower
        #TODO: need to be able to determine drone location for insertion maneuver, and docking    

    def get_paths_by_endpoint_names(self,endpoint_names):
        if len(endpoint_names) != 2:
            return []
        start_endpoint = self.get_endpoint_by_name(endpoint_names[0])
        end_endpoint = self.get_endpoint_by_name(endpoint_names[1])

        if not isinstance(start_endpoint, Endpoint) or not isinstance(end_endpoint, Endpoint):
            return []

        start_pose_index = start_endpoint.wait_at_id
        end_pose_index = end_endpoint.wait_at_id

#        print("start_pose_index: {}, end_pose_index: {} for names {}".format(start_pose_index,end_pose_index,endpoint_names))
        return self.get_paths_by_pose_index(start_pose_index,end_pose_index)

    def get_paths_by_pose_index(self,start_pose_index,end_pose_index):
        return_paths = []
        return_path_db_indices = []
        #This method extends naturally to running as a DB query, which would be super duper optimized
        for i, pose_index in enumerate(self.path_indices):
            if pose_index[0] == start_pose_index and pose_index[1] == end_pose_index:
                path = self.paths_np[i]
                return_paths.append(path)
                return_path_db_indices.append(pose_index[2])
            elif pose_index[1] == start_pose_index and pose_index[0] == end_pose_index:
                reversed_path = self.paths_np[i][:]
                reversed_path.reverse()
                return_paths.append(reversed_path)
                return_path_db_indices.append(pose_index[2])
        return return_paths, return_path_db_indices


    def to_dict(self):
        """Export a dict representation of the laboratory environment."""
        lab_dict = { 
                      "name": self.name,
                      "bounds": self.bounds.to_dict(),
                      "exclusions":[obs.to_dict() for obs in self.obstacles],
                      "paths": [path.to_dict() for path in self.paths],
                      "endpoints": [ep.to_dict() for ep in self.endpoints.values()]
                     }
                     
        return lab_dict

    def to_json(self, pretty=False):
        """Export a JSON representation of the laboratory environment."""
        json_dict = self.lab_dict()
        
        if pretty:
            json_output = json.dumps(json_dict, indent=2)
        else:
            json_output = json.dumps(json_dict)

        return json_output
    

    def _check_location(self, location):
        log = logging.getLogger(__name__)
        
        if not self.bounds.check_if_contains_point(location, -self.droneClearance):
            log.info("Location {} outside of environmental bounds.".format(location))
            return False
        
        o = self._check_if_location_in_obstacle(location)
        if o:
            log.info("Location {} obscured by obstacle.".format(location))
            log.debug("Obstacle(s) obscuring: {}".o)
            return False
        
        return True

    def _check_if_location_in_obstacle(self, location, buffer=None):
        """Returns a list of obstacles if location is within buffer of 
        obstacles, else None."""
        log = logging.getLogger(__name__)
        if buffer is None:
            buffer = self.droneClearance
        output = []
        for o in self.obstacles:
            if o.check_if_contains_point(location, buffer):
                output.add(o)
    
        if output:
            if len(output) > 1:
                log.warn("Location within {} of multiple obstacles: {}".format(buffer, output))
            return output
    
        return None

    def _check_path(self, path):
        log = logging.getLogger(__name__)
        #TODO: fix print statements, make into properly-functioning log.info() calls
        if not isinstance(path, Path):
            print("Path checking:  input is not a path: {}".format(path))
            return False
        if not self.bounds.check_if_contains_point(path.start, -self.droneClearance):
            print("Starting point {} outside of environmental bounds.".format(path.start))
            return False
        if not self.bounds.check_if_contains_point(path.end, -self.droneClearance):
            print("End point {} outside of environmental bounds.".format(path.end))
            return False

        for o in self.obstacles:
            if o.check_if_contains_point(path.start):
                print("Path {} starts within obstacle.".format(path))
                print("Obstacle containing start point {}.".format(path, o))
            if o.check_if_contains_point(path.end):
                print("Path {} ends within obstacle.".format(path))
                print("Obstacle containing end point {}.".format(path, o))
            if o.check_if_intersects(path, self.droneClearance):
                print("Path {} obscured by obstacle.".format(path))
                print("Obstacle obscuring: {}".o)
                return False
            
        return True

    def _setup_pathing_lookups(self):
        #TODO: Set up self._paths_by_endpoint_locations; create kdtree
        self._pathing_kdtree = None

    def _setup_endpoint_lookups(self):
        #TODO: set up self._endpoints_by_location; create kdtree
        self._endpoint_kdtree = None

    @staticmethod
    def from_dict(json_dict):
        """ Creates and returns an Environment object from input dict. """
        log = logging.getLogger(__name__)
        try:
            log.debug("Constructing Environment from json dict: {}".format(json_dict))
            name = json_dict['name']

            # Create Cuboid from JSON to help check validity of JSON definition
            room_bounds = Cuboid(JSON=json_dict['bounds'])
            room_bounds_dict = room_bounds.to_dict()
            if np.abs(room_bounds_dict['theta_z'])  > NUMERICAL_PRECISION:
                log.warn("Constructing room from rotated cuboid (theta = {}); bounds will be inaccurate.".format(room_bounds_dict['theta_z']))
            
            centroid = np.array([room_bounds_dict['centroid']['x'], 
                                 room_bounds_dict['centroid']['y'], 
                                 room_bounds_dict['centroid']['z']])
            
            size = np.array([room_bounds_dict['length']['x'],
                             room_bounds_dict['length']['y'],
                             room_bounds_dict['length']['z']])
                             
            mins = centroid - size/2.
            maxes = centroid + size/2.
        
            env = Environment(*(np.append(mins, maxes).tolist()))
            env.name = json_dict['name']
            for obs in json_dict["exclusions"]:
                log.debug("Adding obstacle to environment: {}".format(obs))
                env.add_obstacle(Cuboid(JSON = obs))
            for path in json_dict['paths']:
                log.debug("Adding path to environment: {}".format(path))
                env.add_path_indices(path)
                env.add_path(Path(JSON = path))
                env.add_path_np(path)
            for ep in json_dict["endpoints"]:
                log.debug("Adding endpoint to environment: {}".format(ep))
                env.add_endpoint(Endpoint(JSON = ep))

            return env
        except ValueError as e:
            log.error("Error loading Environment from dict")
            log.error(e)
            raise e
    
    @staticmethod
    def from_json(JSON):
        """ Creates and returns an Environment object from input JSON. """
        log = logging.getLogger(__name__)
        try:
            environment = json.loads(JSON)
            assert('name' in environment and 'paths' in environment and
                    'exclusions' in environment and 'bounds' in environment and
                    'endpoints' in environment)
            
            return Environment.from_dict(environment)
        except Exception as e:
            log.error("Error loading Environment from JSON.")
            log.error(e)
            raise e
    
    
class Endpoint(object):
    """
    Endpoint contains pose, type, and id for an endpoint.
    """
    counter = 0
    def __init__(self, JSON=None, pose=None, type=None):
        if isinstance(JSON, str):
            self.from_json(JSON)
        elif isinstance(JSON, dict):
            self.from_dict(JSON)
        elif isinstance(pose, Pose) and isinstance(type, EndpointTypes):
            self.pose = pose
            self.type = type
            self.wait_at = None # Pose of drone for wait_at before interacting with endpoint
            self.counter = self.counter + 1
            self._id = self.counter
            self.rename()
    
    def from_dict(self, json_dict):
        log = logging.getLogger(__name__)
        try:
            assert(isinstance(json_dict, dict))
            assert('pose' in json_dict and 'type' in json_dict and 'name' in json_dict)
            log.debug("Creating Endpoint from json dict: {}".format(json_dict))

            self.pose = Pose(JSON=json_dict['pose'])
            self.type = EndpointTypes(json_dict['type']['value'])
            self.wait_at = Pose(JSON=json_dict['wait_at'])
            self.name = json_dict['name']
            self.counter = self.counter + 1
            self._id = self.counter
            self.wait_at_id = json_dict['wait_at']['id']
        except Exception as e:
            log.error("Error while trying to create Endpoint from json_dict.")
            log.error(e)
            raise e
    
    def from_json(self, JSON):
        log = logging.getLogger(__name__)
        try:
            endpoint = json.loads(JSON)
            log.debug("Creating Endpoint from JSON string: {}".format(JSON))
            self.from_dict(endpoint)
        except Exception as e:
            log.error("Error while trying to create Endpoint from JSON.")
            log.error(e)
            raise e
            
            
    def to_dict(self):
        """Export dict representation of Endpoint."""
#Merge conflict between develop and feature/environment-spec.
#Assuming feature/environment-spec is more up to date, but not 100% sure.
#                        "type": self.type.to_json(), 
#                        "pose": json.loads(self.pose.json),
#                        "wait_at": json.loads(self.wait_at.json)
        endpoint_dict = {
                        "name": self.name, 
                        "type": self.type.to_dict(), 
                        "pose": self.pose.to_dict(),
                        "wait_at": self.wait_at.to_dict()
                     }
        return endpoint_dict
        
    def to_json(self, pretty=False):
        """Export JSON string representation of Endpoint."""
        if pretty:
            json_out = json.dumps(self.to_dict(), indent=2)
        else:
            json_out = json.dumps(self.to_dict())
        return json_out
        
    def add_wait_at_pose(self, wait_at_pose):
        """Add wait-at pose for endpoint."""
        if isinstance(wait_at_pose, Pose):
            self.wait_at = wait_at_pose
        else:
            raise TypeError("Invalid input type. Expecting Pose.")
            
    def distance_to(self, location):
        """Get distance to location."""
        dist = np.nan
        if isinstance(location, Pose):
            dist = np.linalg.norm(location.ndarray[:3]-self.pose.ndarray[:3])
        elif isinstance(location, np.ndarray):
            dist = np.linalg.norm(location[:3]-self.pose.ndarray[:3])
        return dist
        
    def rename(self, new_name = None):
        if new_name is not None:
            self.name = new_name
            return
            
        typeName = ""
        if self.type == EndpointTypes.docking_station:
            typeName = "Dock"
        else:
            typeName = "Nest"
        
        self.counter += 1
        self._id = self.counter
        self.name = typeName + str(self._id)
    
    

