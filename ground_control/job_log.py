import threading

from gui_log import  glog, glog_debug, glog_info, glog_warn, glog_error

from defs import Job, JobSpec

import rospy
from labdrone_msgs.srv import TransactionNotification
from labdrone_msgs.msg import TransactionInfo


class JobLogger(object):
    __instance = None
    @staticmethod
    def getInstance():
        """ Static access method. """
        if JobLogger.__instance == None:
            JobLogger()
        return JobLogger.__instance
    def __init__(self):
        """ Virtually private constructor. """
        if JobLogger.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            JobLogger.__instance = self
            self.lock = threading.Lock()
            self.notification_proxy = rospy.ServiceProxy("TransactionNotification",TransactionNotification)
    def log(self,msg):
        #In principle any other non-thread safe operations would go here, like writing to a log file.
        #for now, just wrap glog
        glog_info(msg=msg,source="JobBlog")


def job_assigned(job):
    jl = JobLogger.getInstance()
    with jl.lock:
        msg = "Job {}: Assigned to drone {} to transport tray {} from {} to {}".format(job.job_id, job.drone_id, job.payload_id, job.pickup, job.drop_off)
        jl.log(msg)

def job_completed(job):
    jl = JobLogger.getInstance()
    with jl.lock:
        msg = "Job {}: Completed".format(job.job_id)
        jl.log(msg)

def pickup_completed(job):
    jl = JobLogger.getInstance()
    with jl.lock:
        msg = "Job {}: Tray {} picked up at {} by drone {}".format(job.job_id, job.payload_id, job.pickup, job.drone_id)
        jl.log(msg)
    request = TransactionInfo(transaction_type="PICKUP", drone_id=job.drone_id, nest_id=job.pickup, tray_id=job.payload_id)
    response = jl.notification_proxy(request)

def drop_off_completed(job):
    jl = JobLogger.getInstance()
    with jl.lock:
        msg = "Job {}: Tray {} dropped off at {} by drone {}".format(job.job_id, job.payload_id, job.drop_off, job.drone_id)
        jl.log(msg)
    request = TransactionInfo(transaction_type="DROP_OFF", drone_id=job.drone_id, nest_id=job.drop_off, tray_id=job.payload_id)
    response = jl.notification_proxy(request)

def job_incomplete(job):
    jl = JobLogger.getInstance()
    with jl.lock:
        msg = "Job {}: Failed prior to completion.".format(job.job_id)
        jl.log(msg)
