import threading
from gui.src.gui_server import MainHandler
import datetime


class GUILogger(object):
    __instance = None
    @staticmethod
    def getInstance():
        """ Static access method. """
        if GUILogger.__instance == None:
            GUILogger()
        return GUILogger.__instance
    def __init__(self):
        """ Virtually private constructor. """
        if GUILogger.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            GUILogger.__instance = self
            self.lock = threading.Lock()
            self.gui_server_main_handler = MainHandler.getInstance()

    def log(self,severity,msg,source):
        log_dict = {"msg":msg,"source":source}

        #Enforce values?
        log_dict.update({"severity":severity})

        datetime_str = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        log_dict.update({"datetime":datetime_str})

        self.gui_server_main_handler.set_log(log_dict)


# I was going to dynamically grab the source module name, but it looked like messing around with
# a reference to the call stack could cause issues if not handled correctly, and it doesn't seem
# worth the risk of doing it wrong at this point.
def glog(severity,msg,source=""):

    gl = GUILogger.getInstance()
    with gl.lock:
        gl.log(severity,msg,source)

def glog_error(msg,source=""):
    glog("ERROR",msg,source="")

def glog_warn(msg,source=""):
    glog("WARN",msg,source)

def glog_info(msg,source=""):
    glog("INFO",msg,source)

def glog_debug(msg,source=""):
    glog("DEBUG",msg,source)

if __name__ == '__main__':
    pass
