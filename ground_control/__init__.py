# -*- coding: utf-8 -*-

import os
import logging
import logging.config
import yaml

#things that should be externally visible at module level.
#from air_traffic_controller import AirTrafficController
from defs import JobSpec
from defs import DroneStatus
from defs import BatteryStatus
from defs import DroneState
from defs import JobStatus
from defs import FleetManagerDroneStatus
from defs import DroneManagerCmdType
from defs import CmdQueueElement
#from ground_control import GroundControl
import environment
import geometry

# from ground_control.geometry import Cuboid
# from geometry import Location
# from geometry import Orientation
# from geometry import Position
# from geometry import LineSegment
# from environment import Environment
# from drone_sim import DroneSim
default_log_file = os.path.join(os.path.dirname(os.path.abspath(__file__)),'default_logging.yml')

def setup_logging(log_file = default_log_file,
                  log_level = logging.DEBUG,
                  handlers = ['console']):
    """Set up logging configuration."""
    print("Setting up logging.")

    if log_file is None or not os.path.exists(log_file):
        print("Loading basic logging configuration.")
        logging.basicConfig(level=log_level)
    else:
        print("Using log file: {}".format(log_file))
        with open(log_file, 'rt') as infile:
            config = yaml.safe_load(infile.read())
            logging.config.dictConfig(config)

    logging.info("Logging setup finished.")

setup_logging()
#TODO make this easier to configure; perhaps don't load logging by default?
