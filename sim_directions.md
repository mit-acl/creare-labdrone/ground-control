Directions for building drone component of simulation here:
https://gitlab.com/mit-acl/creare-labdrone/labdrone/-/blob/6fc0c6f318fd85d255e0b68f1d7c8496d361c616/README.md

The following assumes that the ^ MIT directions have been followed for building as simulation.



Other assumptions:
* There is a bash function 'gosim' that sources <workspace>/devel/setup.bash.
The specific version I'm currently using is defined in .bashrc as:

function gosim { goros; source /home/labdrone/Creare_Data/LabDrone/workspaces/20200805sim/devel/setup.bash; cd ~/workspaces/20200805sim/src/ground_control/ground_control/;}
export -f gosim

If this is not defined for your workspace, simply sourcing the right setup.bash file should suffice. I'm also cd'ing into the ground_control directory for convience so I don't have to worry about mucking about with the python PATH var.



To simulate flight:

New terminal:
goros && roscore

New terminal:
gosim
rosrun labdrone remote_start.sh -s

New terminal:
gosim
goconda && conda activate labdrone
python ground_controller.py


To issue an external transport request: (with example fields)

New terminal:
gosim
rosservice call TransportRequest "{job: {pickup: 'pickup1',drop_off: 'dropo ff1',pickup_attempts: 2,drop_off_attempts: 5,payload_id: 'asdf',count: 1,period_s: 0}}"


At present, ground_controller will start a simulated job once launched. This behavior can be easily edited in ground_control.py
