#!/usr/bin/env python

#Created on Fri, 10 Jan, 2020
#
#@author: @jkp, @WSowerwine
#
#Environment setup module for advanced plate transfer ground controller.
#
#Allows user to define an environment using the VICON system.
#


import logging

import numpy as np

#I'm assuming this will be wiped out at some point by more elegant handling
import sys
import os
# cur_path = os.path.abspath(os.path.dirname(__file__))
# sys.path.append(os.path.join(cur_path, "../ground_control/"))

print("Environment_Setup.py starting up!")

import tf
import rospy
from geometry_msgs.msg import PoseStamped, Pose #use vicon `world` topic
# from snapstack_msgs.msg import State # or use snap `state` topic from drone?
from sensor_msgs.msg import Joy

from ground_control.environment import Environment
from ground_control.environment import Endpoint

from ground_control.geometry import Pose
from ground_control.geometry import Cuboid
from ground_control.geometry import Path

import ground_control.database as db
from ground_control.database import EndpointTypes

from gui.src.gui_server import MainHandler
from gui.src.gui_server import TornadoDaemon


rospy.logwarn("Environment setup application starting...")

time_sync_threshold = 0.2
min_num_pts = 3


# TODO: convert to enum
A = 0
B = 1
X = 2
Y = 3
LB = 4
RB = 5
BACK = 6
START = 7
PWR = 8

BUTTON_ADD_PT = A
BUTTON_REMOVE_PT = B
BUTTON_SUBMIT_PATH = X
BUTTON_SUBMIT_OBSTACLE = Y
BUTTON_RESET_POINTS = BACK
BUTTON_ADD_TRAY = RB
BUTTON_ADD_DOCK = LB
BUTTON_SAVE_CONFIGURATION_AND_EXIT = START

ACCEPT = A
REJECT = B

class EnvironmentSetup(object):
    
    def __init__(self, environment):
        log = logging.getLogger(__name__)
        
        if not isinstance(environment, Environment):
            rospy.logfatal("Error. Startup with invalid environment specified.")
            
        # For ROS World Pose updates
        self.wand_pose = None 
        self.wand_pose_time = None 
        self.dock_pose = None 
        self.dock_pose_time = None 
        self.tray_pose = None 
        self.tray_pose_time = None 
        
        self.points = [] # temporary points buffer
        self.environment = environment
        
        #Get webserver handle
        rospy.loginfo("Starting webserver.")
        self.webserver = MainHandler.getInstance()
        
        self._LAST_CALL_TIMEOUT = 0.3
        self._last_buttons = None
        self._await_confirmation = None
        
        
    def update_wand_pose(self, data):
        """ Pose update callback for ROS Wand Node updates."""
        # print("update wand pose!")
        self.wand_pose = data.pose
        self.wand_pose_time = rospy.get_time()
        
    def update_dock_pose(self, data):
        """ Pose update callback for ROS Dock Node updates."""
        self.dock_pose = data.pose
        self.dock_pose_time = rospy.get_time()
        
    def update_tray_pose(self, data):
        """ Pose update callback for ROS Tray Node updates."""
        self.tray_pose = data.pose
        self.tray_pose_time = rospy.get_time()
    
    def joystick_ros_update(self, data):
        """Joystick input function."""
        if self._last_buttons is None:
            self._last_buttons = np.zeros(len(data.buttons))
            
        #debounce with timeout of self._LAST_CALL_TIMEOUT seconds
        bounce = False
        if np.any(np.array(data.buttons)) or np.any((self._last_buttons != 0.) & np.array(data.buttons, dtype=np.bool)):
            curr_time = rospy.get_time()

            j = 0
            for button in data.buttons:
                if button:
                    if curr_time - self._last_buttons[j] < self._LAST_CALL_TIMEOUT:
                        #reset timer:
                        bounce = True
                    self._last_buttons[j] = curr_time
                j += 1

            if bounce:            
                return
                
        if self._await_confirmation is not None:
            if(data.buttons[ACCEPT]):
                rospy.loginfo("Accept current object creation.")
                self.accept_object()
            elif(data.buttons[REJECT]):
                rospy.loginfo("Reject current object creation.")
                self.reject_object()
            return
        
        if data.buttons[BUTTON_ADD_PT]:
            now = rospy.get_time()
            if self.wand_pose is not None:
                if self.wand_pose_time - now < time_sync_threshold:
                    self.add_point()
                else:
                    rospy.loginfo("time sync issue adding object point.")
            else:
                rospy.logerr("Add Point Error. No wand pose!")
            
        elif data.buttons[BUTTON_REMOVE_PT]:
            rospy.loginfo("Removing last point.")
            if self.points is not None and len(self.points) > 0:
                self.points.pop()
                
        elif data.buttons[BUTTON_SUBMIT_PATH]:
            rospy.loginfo("Submit path.")
            self.construct_path()
            
        elif data.buttons[BUTTON_SUBMIT_OBSTACLE]:
            rospy.loginfo("Submitting obstacle.")
            self.construct_obstacle()
        
        elif data.buttons[BUTTON_RESET_POINTS]:
            rospy.loginfo("Reset points buffer.")
            self.reset()
            
        elif data.buttons[BUTTON_ADD_TRAY]:
            rospy.loginfo("Adding tray.")
            self.add_tray_pose()
        
        elif data.buttons[BUTTON_ADD_DOCK]:
            rospy.loginfo("Adding dock.")
            self.add_charging_dock_pose()
            
        elif data.buttons[BUTTON_SAVE_CONFIGURATION_AND_EXIT]:
            rospy.loginfo("Save and exit.")
            self.save_and_exit()
            
            
    def add_point(self):
        """Add the current wand location to the current point buffer."""
        position = self.get_wand_pose()
        rospy.logdebug("Adding point: {}".format(position))
        self.points.append(position)
        
    def reset_points(self):
        self.points = []
        
    def construct_obstacle(self):
        obstacle = None
        try:
            rospy.loginfo("Creating obstacle: {}.".format(obstacle))
            obstacle = environment.create_obstacle(self.points)
            print("Press A to accept, or B to reject obstacle: {}".format(obstacle.to_json(pretty=True)))
            self._await_confirmation = obstacle
            # display obstacle
            self.webserver.set_candidate_exclusion(obstacle.to_json())
            # self.environment.add_obstacle(obstacle)
        except Exception as e:
            rospy.logerr("Failed to create obstacle. Try again.")
            rospy.logerr("Error: {}".format(e))

    def construct_path(self):
        path = None
        try:
            rospy.loginfo("Creating path from points: {}".format(self.points))
            path = environment.create_path(self.points)
            print("Press A to accept, or B to reject path: {}".format(path.to_json(pretty=True)))
            self._await_confirmation = path
            # self.environment.add_path(path)
        except Exception as e:
            rospy.logerr("Failed to create path. Try again.")
            rospy.logerr("Error: {}".format(e))
                
    def add_tray_pose(self):
        try:
            num_points = len(self.points)
            if num_points > 1 or num_points < 1:
                print("Error: require one, and only one wait-at location to create tray pose.")
                self.reset()
                return
            rospy.loginfo("Creating tray pose with wait-at location: {}".format(self.points[0].to_json()))
            
            rospy.loginfo("New tray pose: {}".format(self.get_tray_pose()))
            tray = Endpoint(pose = self.get_tray_pose(), type = EndpointTypes.tray_slot)




            #Flatten pose. Not really necessary for now.
            wait_at_pose = self.points[0]
            print("What is going wrong here: {}".format(wait_at_pose))
            flattened_pose = flatten_pose(wait_at_pose)
            tray.add_wait_at_pose(flattened_pose)

            print("What is going wrong here: {} -> {}".format(wait_at_pose,flattened_pose))

            self._await_confirmation = tray
            print("Press A to accept, or B to reject tray pose: {}".format(tray.to_json(pretty=True)))

        except Exception as e:
            #At present, get_tray_location() doesn't exist
            #rospy.logerr("Failed to create tray at {}".format(self.get_tray_location()))
            rospy.logerr("Failed to create tray: " + str(e))
            
    def add_charging_dock_pose(self):
        try:
            num_points = len(self.points)
            if num_points > 1 or num_points < 1:
                print("Error: require one, and only one wait-at location to create dock pose.")
                self.reset()
                return
            rospy.loginfo("Creating dock pose with wait-at location: {}".format(self.points))
            
            rospy.loginfo("New dock pose: {}".format(self.get_dock_pose()))
            dock = Endpoint(pose = self.get_dock_pose(), type = EndpointTypes.docking_station)

            #Flatten pose. Not really necessary for now.
            wait_at_pose = self.points[0]
            flattened_pose = flatten_pose(wait_at_pose)
            dock.add_wait_at_pose(flattened_pose)

            self._await_confirmation = dock
            
            
            print("Press A to accept, or B to reject dock pose: {}".format(dock.to_json(pretty=True)))

        except Exception as e:
            rospy.logerr("Failed to create a charging dock at {}.".format(self.get_dock_pose()))
                
    def accept_object(self):
        if isinstance(self._await_confirmation, Path):
            path = self._await_confirmation
            rospy.loginfo("Adding Path: {}".format(path))
            self.environment.add_path(path)
        elif isinstance(self._await_confirmation, Cuboid):
            obstacle = self._await_confirmation
            rospy.loginfo("Adding obstacle: {}".format(obstacle))
            self.environment.add_obstacle(obstacle)
        elif isinstance(self._await_confirmation, Endpoint):
            endpoint = self._await_confirmation
            rospy.loginfo("Adding endponit: {}".format(endpoint))
            self.environment.add_endpoint(endpoint)
        
        self.reset()
    
    def reject_object(self):
        rospy.loginfo("Rejecting object.")
        self.reset()
            
    def reset(self):
        rospy.loginfo("Resetting after confirmation of object.")
        self.reset_points()
        self._await_confirmation = None
    
    @staticmethod
    def get_pose_from_vicon_pose(pose):
        pose_arr = np.zeros(7)
        pose_arr[0] = pose.position.x
        pose_arr[1] = pose.position.y
        pose_arr[2] = pose.position.z
        pose_arr[3] = pose.orientation.x
        pose_arr[4] = pose.orientation.y
        pose_arr[5] = pose.orientation.z
        pose_arr[6] = pose.orientation.w
        return Pose(array = pose_arr)
        
    def get_wand_pose(self):
        return self.get_pose_from_vicon_pose(self.wand_pose)

    def get_dock_pose(self):
        return self.get_pose_from_vicon_pose(self.dock_pose)

    def get_tray_pose(self):
        return self.get_pose_from_vicon_pose(self.tray_pose)

    def save_and_exit(self)    :
        #TODO: fix this up; should this have different behavior if no environment was created?
        # wsowerwine 20200807 Yes - if this process is strictly additive, we probably want to iterate over adding new elements to the
        # existing lab model. Thankfully, it looks like the default save-update behavior will save a lot of pain and we can just add
        # the highest level DB entries. I'm also not totally sure that adding everything at the end is preferable to sequentially adding
        # lab model elements as they become available.

        rospy.loginfo("Saving configuration and exiting.")
        with db.Database() as dbase:
            sess = dbase.session
            #make lab model sql object
            lab_model_dict = self.environment.to_dict()
            print("Saving configuration from dict: {}".format(lab_model_dict))
            lab_model = db.LabModelSQL.from_dict(lab_model_dict)
            sess.add(lab_model)
            sess.commit()

            lab_model_id = sess.query(db.LabModelSQL).filter_by(name=str(lab_model.name)).order_by(db.LabModelSQL.id.desc()).first().id
            dbase.coerce_path_ends(lab_model_id)
            dbase.condense_wait_at(lab_model_id)
        rospy.signal_shutdown("Environment saved. Exiting."); 
    
# def start_node(environment):

def flatten_pose(pose):
    pose_quat = (pose.orientation[0],pose.orientation[1],pose.orientation[2],pose.orientation[3])
    flattened_quat = flatten_quaternion(pose_quat)
    print("FLAT {}".format(flattened_quat))
    pose.orientation[0] = flattened_quat[0]
    pose.orientation[1] = flattened_quat[1]
    pose.orientation[2] = flattened_quat[2]
    pose.orientation[3] = flattened_quat[3]

    return pose


def flatten_quaternion(quaternion):
    yaw = tf.transformations.euler_from_quaternion(quaternion)[2]
    return tf.transformations.quaternion_from_euler(0,0,yaw)
        
        
def run_node(environment):
    # start_node(environment)

    mainHandler = MainHandler.getInstance()

    if not rospy.is_shutdown():
        TornadoDaemon.kill()
        
        setup = EnvironmentSetup(environment)

        # Subscribe to joystick inputs
        rospy.Subscriber("joy", Joy, setup.joystick_ros_update)
        # Subscribe to VICON rebroadcaster 'world' topic
        rospy.Subscriber("wand/world", PoseStamped, setup.update_wand_pose)
        rospy.Subscriber("dock/world", PoseStamped, setup.update_dock_pose)
        rospy.Subscriber("tray/world", PoseStamped, setup.update_tray_pose)
        
	#what is the purpose of this?
        #mainHandler.set_drone_info()
        TornadoDaemon.getInstance().start()
        print("tornaedo daemon staertaed")
        rospy.spin()
    
    rospy.logwarn("Stopping Tornado server.")
    TornadoDaemon.getInstance().stop()
        
if __name__ == '__main__':
    log = logging.getLogger(__name__)

    try:
        rospy.init_node('environment', log_level=rospy.DEBUG)
        rospy.logdebug("Opened environment spec app with params: {}.".format(rospy.get_param_names()))


        drop_db = rospy.get_param('/env_def/drop_db', False)

        # room bounds
        bounds = {}
        bounds['xmax'] = rospy.get_param('/env_def/x_max',  5.0)
        bounds['xmin'] = rospy.get_param('/env_def/x_min', -5.0)
        bounds['ymax'] = rospy.get_param('/env_def/y_max',  5.0)
        bounds['ymin'] = rospy.get_param('/env_def/y_min', -5.0)
        bounds['zmax'] = rospy.get_param('/env_def/z_max',  3.0)
        bounds['zmin'] = rospy.get_param('/env_def/z_min',  0.0)
        
        #Check for environment name:
        environment_name = rospy.get_param('/env_def/env_name', None)
        if environment_name is not None:
            rospy.logdebug("Checking for environment with name {}".format(environment_name))
            print("Checking for environment with name {}".format(environment_name))

            if drop_db:
                db._delete_tables()
                db.create_tables()

            with db.Database() as dbase:
                sess = dbase.session
                env_query = sess.query(db.LabModelSQL).filter(db.LabModelSQL.name == environment_name)
                if env_query.count() == 1:
                    rospy.logdebug("Loading environment {} from database.".format(environment_name))
                    sql_env = env_query.first()
                    environment = Environment.from_json(sql_env.json)
                else:
                    if env_query.count() > 1:
                        rospy.logwarn("Multiple environment specifications match name {}.".format(environment_name))
                        sql_env = env_query.first()
                        environment = Environment.from_json(sql_env.json)
                    else:
                        environment = None
        else:
            rospy.logwarn("Missing environment name. Using default.")
            environment = None
            environment_name = "default"
            
        if environment is None:
            rospy.logdebug("Creating new environment with name: {} and bounds {}.".format(environment_name, bounds))
            environment = Environment(bounds['xmin'],bounds['ymin'],bounds['zmin'],bounds['xmax'],bounds['ymax'],bounds['zmax'], name=environment_name)

        run_node(environment)
    except rospy.ROSInterruptException:
        rospy.logerr("Error on startup.")
        pass
