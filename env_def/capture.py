#!/usr/bin/env python
import roslib
import rospy
import math
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose
from acl_msgs.msg import ViconState


time_sync_threshold = 0.2
min_num_pts = 3

A = 0
B = 1
X = 2
Y = 3
RB = 5
BACK = 6
START = 7

BUTTON_ADD_PT     = A
BUTTON_REMOVE_PT  = B
BUTTON_SUBMIT     = START



class EnvDef:

    def __init__(self):
        self.pts = []
        self.pose = None
        self.last_pose = None

    def poseCB(self, data):
        self.pose = data.pose
        self.pose_time = rospy.get_time()


    def joyCB(self, data):

        if data.buttons[BUTTON_ADD_PT]:
            
            self.now_time = rospy.get_time()
            if self.last_pose is not None:
                if self.pose_time - self.now_time < time_sync_threshold:
                    self.pts.append(self.pose)
                else:
                    rospy.loginfo("time sync issue adding object point.")
        
        elif data.buttons[BUTTON_REMOVE_PT]:
            if self.pts:
                self.pts.pop()
                
        elif data.buttons[BUTTON_SUBMIT]:
            if len(self.pts) >= min_num_pts:
                self.sendPoints()
                
    def sendPoints(self):
        #TODO format message with points, send to env def class
        print(self.pts)
        pass

def startNode():
    env_def = EnvDef()
    rospy.Subscriber("joy", Joy, env_def.joyCB)
    #TODO want world not state
    rospy.Subscriber("vicon", ViconState, env_def.poseCB)
    rospy.spin()

if __name__ == '__main__':

    #TODO define name for wand object, handle here
    ns = rospy.get_namespace()
    try:
        rospy.init_node('joy')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is tyipcally accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=mQ01 $ rosrun quad_control joy.py")
        else:
            startNode()
    except rospy.ROSInterruptException:
        pass
